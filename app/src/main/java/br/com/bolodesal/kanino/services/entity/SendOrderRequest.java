package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Thiago Ferreira on 22/11/2015.
 */
public class SendOrderRequest {
    @SerializedName("order")
    @Expose
    private Order order;

    @SerializedName("orderItens")
    @Expose
    private List<OrderItem> orderItens;

    public void setOrder(Order order) {
        this.order = order;
    }

    public void setOrderItems(List<OrderItem> orderItens) {
        this.orderItens = orderItens;
    }

    public Order getOrder() {
        return order;
    }

    public List<OrderItem> getOrderItens() {
        return orderItens;
    }
}
