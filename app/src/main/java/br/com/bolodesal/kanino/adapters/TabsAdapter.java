package br.com.bolodesal.kanino.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.bolodesal.kanino.fragments.CategoryFragment;
import br.com.bolodesal.kanino.fragments.ProductFragment;

/**
 * Created by Thiago Ferreira on 09/10/2015.
 */
public class TabsAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private String[] titles = {"CATEGORIAS", "PRODUTOS"};

    public TabsAdapter(FragmentManager fm, Context c) {
        super(fm);

        mContext = c;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment frag = null;

        if(position == 0){
            frag = new CategoryFragment();
        }
        else if(position == 1){
            frag = new ProductFragment();
        }

        Bundle b = new Bundle();
        b.putInt("position", position);
        frag.setArguments(b);

        return frag;
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return ( titles[position] );
    }
}
