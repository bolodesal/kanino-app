package br.com.bolodesal.kanino.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.bolodesal.kanino.services.entity.ItemCart;

/**
 * Created by Thiago Ferreira on 02/11/2015.
 */
public class Cart {
    private SQLiteDatabase bd;

    public Cart(Context context){
        DBKanino auxBd = new DBKanino(context);
        bd = auxBd.getWritableDatabase();
    }

    public void insert(ItemCart item){
        ContentValues valores = new ContentValues();
        valores.put("id", item.getId());
        valores.put("name", item.getName());
        valores.put("amount", item.getAmount());
        valores.put("price", item.getPrice());
        valores.put("image", item.getStringImage());

        bd.insert("itemCart", null, valores);
    }


    public void update(ItemCart item){
        ContentValues valores = new ContentValues();
        valores.put("id", item.getId());
        valores.put("name", item.getName());
        valores.put("amount", item.getAmount());

        bd.update("itemCart", valores, "id = ?", new String[]{"" + item.getId()});
    }


    public void delete(ItemCart item){
        bd.delete("itemCart", "id = " + item.getId(), null);
    }

    public ItemCart getItem(int id){
        ItemCart item = new ItemCart();
        String[] colunas = new String[]{"id"};
        /*String[] colunas = new String[]{String.valueOf(id)};*/
        Cursor cursor = bd.query("itemCart", colunas, String.valueOf("id = " + id), null, null, null, null);

        if(cursor.moveToFirst()){

            item.setId(cursor.getInt(0));
            /*item.setName(cursor.getString(1));
            item.setAmount(cursor.getInt(2));*/
        }

        return item;
    }

    public List<ItemCart> getAllItems(){
        List<ItemCart> list = new ArrayList<ItemCart>();
        String[] colunas = new String[]{"id", "name", "amount", "price", "image"};

        Cursor cursor = bd.query("itemCart", colunas, null, null, null, null, "name ASC");

        if(cursor.moveToFirst()){

            do{

                ItemCart item = new ItemCart();
                item.setId(cursor.getInt(0));
                item.setName(cursor.getString(1));
                item.setAmount(cursor.getInt(2));
                item.setPrice(cursor.getDouble(3));
                item.setImage(cursor.getBlob(4));
                list.add(item);

            }while(cursor.moveToNext());
        }

        return(list);
    }

    public int getCount(){
        int count = 0;

        String[] colunas = new String[]{"id", "name", "amount"};

        Cursor cursor = bd.query("itemCart", colunas, null, null, null, null, "name ASC");

        if(cursor.getCount() > 0){
            count = cursor.getCount();
        }
        return count;
    }
}
