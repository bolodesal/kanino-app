package br.com.bolodesal.kanino.provider;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by Thiago Ferreira on 15/11/2015.
 */
public class SearchableProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = "br.com.bolodesal.kanino.provider.SearchableProvider";
    public static final int MODE = DATABASE_MODE_QUERIES;

    public SearchableProvider(){
        setupSuggestions( AUTHORITY, MODE );
    }
}
