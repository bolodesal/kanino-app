package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import br.com.bolodesal.kanino.domain.Category;

public class CategoryRequest {

    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories;

    public ArrayList<Category> getCategories() {
        return categories;
    }
}
