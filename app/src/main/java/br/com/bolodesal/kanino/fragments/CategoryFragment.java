package br.com.bolodesal.kanino.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.bolodesal.kanino.MainActivity;
import br.com.bolodesal.kanino.ProductsByCategoryActivity;
import br.com.bolodesal.kanino.R;
import br.com.bolodesal.kanino.adapters.CategoryAdapter;
import br.com.bolodesal.kanino.services.entity.Category;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.CustomerResponse;
import br.com.bolodesal.kanino.util.UtilKanino;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class CategoryFragment extends Fragment implements RecyclerViewOnClickListenerHack {

    private RecyclerView mRecyclerView;
    private List<Category> mList;
    private CategoryAdapter adapter;
    private FrameLayout fLProgress;
    private KaninoServices service;
    private View view;
    private UtilKanino utilKanino = new UtilKanino();
    FrameLayout fLWithoutConnection;
    private FrameLayout fLSlowConnection;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_category, container, false);

        fLSlowConnection = (FrameLayout) view.findViewById(R.id.fLSlowConnection);

        Button btnReloadS = (Button) view.findViewById(R.id.btnReloadS);
        btnReloadS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fLProgress.setVisibility(View.VISIBLE);
                fLSlowConnection.setVisibility(View.GONE);
                getCategories();
            }
        });

        fLWithoutConnection = (FrameLayout) view.findViewById(R.id.fLWithoutConnection);
        fLProgress = (FrameLayout) view.findViewById(R.id.fLProgress);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rvCategoryList);
        mRecyclerView.setHasFixedSize(true);

        final LinearLayoutManager Llm = new LinearLayoutManager(getActivity());
        Llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(Llm);

        service = RestService.getInstance().getService();

        if(utilKanino.hasConnected(getActivity())){
            getCategories();
        }
        else{
            loadViewError();
        }

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (mList.size() == Llm.findLastCompletelyVisibleItemPosition() + 1) {

                }
            }
        });

        return view;
    }

    private void loadViewError(){
        fLWithoutConnection.setVisibility(View.VISIBLE);
        fLProgress.setVisibility(View.GONE);

        Button btnReload = (Button) view.findViewById(R.id.btnReload);
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!utilKanino.hasConnected(getActivity())) {
                    loadViewError();
                } else {
                    fLWithoutConnection.setVisibility(View.GONE);
                    fLProgress.setVisibility(View.VISIBLE);
                    getCategories();
                }
            }
        });
    }

    public void getCategories(){
        service.getCategories().enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Response<List<Category>> response, Retrofit retrofit) {

                if(response.code() == 200){
                    mList = response.body();
                    adapter = new CategoryAdapter(getActivity(), mList);
                    adapter.setRecyclerViewOnClickListenerHack(CategoryFragment.this);

                    mRecyclerView.setAdapter(adapter);
                }
                else{
                    fLSlowConnection.setVisibility(View.VISIBLE);
                }

                fLProgress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Throwable t) {

                fLProgress.setVisibility(View.GONE);
                fLSlowConnection.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClickListener(View view, int position) {
        /*Toast.makeText(getActivity(), "Nome da categoria: " + mList.get(position).getNomeCategoria()
                + " ID: " + mList.get(position).getIdCategoria(), Toast.LENGTH_SHORT).show();*/

        Intent intent = new Intent(getActivity(), ProductsByCategoryActivity.class);
        intent.putExtra("idCategory", mList.get(position).getIdCategoria());
        intent.putExtra("nameCategory", mList.get(position).getNomeCategoria());
        startActivity(intent);


//        Customer costumer = new Customer();
//        costumer.seteMail("ouvidoria.bruno@gmail.com");
//        costumer.setSenhaCliente("abc123");
//        CustomerRequest request = new CustomerRequest();
//        request.getCustomer();

        /*KaninoServices service = RestService.getInstance().getService();
        service.getCustomer(3).enqueue(new Callback<CustomerResponse>() {
            @Override
            public void onResponse(Response<CustomerResponse> response, Retrofit retrofit) {
                if (response.body()!=null)
                    Log.d("retrofit", response.body().getNomeCompleto());

                Log.d("retrofit", "Não retornou nada no body!");
            }

            @Override
            public void onFailure(Throwable t){
                Log.d("retrofit", t.getMessage());
            }
        });*/

    }
}