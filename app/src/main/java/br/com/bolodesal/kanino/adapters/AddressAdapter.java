package br.com.bolodesal.kanino.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import br.com.bolodesal.kanino.R;
import br.com.bolodesal.kanino.UpdateAccountActivity;
import br.com.bolodesal.kanino.UpdateAddressActivity;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.AddressResponse;
import br.com.bolodesal.kanino.services.models.*;
import br.com.bolodesal.kanino.services.models.Error;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by bruno.pregis on 26/11/2015.
 */
public class AddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater mLayoutInflater;
    private List<AddressResponse> mList;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private Context mContext;
    private KaninoServices service;

    public AddressAdapter(Context c, List<AddressResponse> l){
        mContext = c;
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder mvh;
        if(viewType == 0) {
            View v = mLayoutInflater.inflate(R.layout.item_address_card, viewGroup, false);
            mvh = new MyViewHolder(v);
        }else {
            View v = mLayoutInflater.inflate(R.layout.progress_item, viewGroup, false);
            mvh = new ProgressViewHolder(v);
        }
        return mvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ProgressViewHolder){
            ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
        } else {
            if (holder instanceof MyViewHolder && mList.size() > 0 && position < mList.size()) {
                ((MyViewHolder) holder).txtAddressAlias.setText(mList.get(position).getNomeEndereco());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(AddressResponse addressRequest, int position){
        mList.add(addressRequest);
        notifyItemInserted(position);
    }

    public int progressBarPosition = -1;

    public void addProgressBar(){
        mList.add(null);
        progressBarPosition = mList.size()-1;
        notifyItemInserted(mList.size());
    }

    public void removeProgressBar(){
        mList.remove(mList.size() - 1);
        progressBarPosition = -1;
        notifyItemRemoved(mList.size() - 1);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == progressBarPosition) {
            return 1; //progress bar
        } else
            return 0; //item normal
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar)v.findViewById(R.id.progressBar1);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txtAddressAlias;

        public ImageView ivDelete;

        public MyViewHolder(View itemView) {
            super(itemView);

            service = RestService.getInstance().getService();
            txtAddressAlias = (TextView) itemView.findViewById(R.id.txtAddressAlias);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Deseja continuar?")
                            .setContentText("Confirme se deseja remover o endereço.")
                            .setCancelText("Não, cancelar!")
                            .setConfirmText("Sim, remover!")
                            .showCancelButton(true)
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.cancel();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    final SweetAlertDialog sAlertDialog = sweetAlertDialog;
                                    final UpdateAddressActivity activity = (UpdateAddressActivity) mContext;

                                    service.deleteAddress(mList.get(getPosition()).getIdEndereco()).enqueue(new Callback<GlobalResponse>() {
                                        @Override
                                        public void onResponse(Response<GlobalResponse> response, Retrofit retrofit) {
                                            if (response.code() == 200) {
                                                mList.remove(getPosition());
                                                notifyItemRemoved(getPosition());

                                                sAlertDialog
                                                        .setTitleText("Pronto!")
                                                        .setContentText("Endereço removido com sucesso!")
                                                        .setConfirmText("OK")
                                                        .showCancelButton(false)
                                                        .setConfirmClickListener(null)
                                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                                if(mList.size() == 0)
                                                {
                                                    activity.resetActitivyIsEmpytAddress();
                                                }
                                            }
                                            else
                                            {
                                                if (response != null && !response.isSuccess() && response.errorBody() != null)
                                                {
                                                    Converter<ResponseBody, GlobalResponse<Error<ArrayList<String>>>> errorConverter = retrofit.responseConverter(new TypeToken<GlobalResponse<Error<ArrayList<String>>>>() {}.getType(), new Annotation[0]);
                                                    try {
                                                        GlobalResponse<Error<ArrayList<String>>> r = errorConverter.convert(response.errorBody());

                                                        String messages = "";

                                                        for(int i = 0; i < r.getResponse().getErrors().size(); i++){
                                                            messages += String.format("%s\n", r.getResponse().getErrors().get(i));
                                                        }

                                                        if(mList.size() == 0)
                                                        {
                                                            activity.resetActitivyIsEmpytAddress();
                                                        }

                                                        sAlertDialog.dismiss();

                                                        new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                                                .setTitleText("Oops!")
                                                                .setContentText(messages).show();

                                                    } catch (IOException e) {

                                                        new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                                                .setTitleText("Oops!")
                                                                .setContentText("Ops! Houve um problema, estamos trabalhando na solução.").show();
                                                    }
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Throwable t) {
                                            new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                                    .setTitleText("Oops!")
                                                    .setContentText("Ops! Houve um problema, estamos trabalhando na solução.").show();
                                        }
                                    });

                                }
                            }).show();
                }
            });

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mRecyclerViewOnClickListenerHack != null) {
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }
}
