package br.com.bolodesal.kanino.validation;

import com.mobsandgeeks.saripaar.AnnotationRule;

import java.io.IOException;

import br.com.bolodesal.kanino.interfaces.validation.HasCustomerWithCPF;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.models.GlobalResponse;
import retrofit.Response;

/**
 * Created by Edgar Maia on 20/11/2015.
 */
public class CustomerWithCpfRule extends AnnotationRule<HasCustomerWithCPF, String> {

    protected CustomerWithCpfRule(HasCustomerWithCPF cpf) { super(cpf); }

    @Override
    public boolean isValid(String cpf) {
        boolean isValid = false;
        if (cpf == null || cpf.isEmpty()) { cpf = "00000000000"; }

        KaninoServices service = RestService.getInstance().getService();
        try {
            Response<GlobalResponse> r = service.hasCustomerWithCPF(cpf).execute();

            if (r.code() != 409) {
                isValid = true;
            }
        } catch (IOException e) {
            return false;
        }catch (Exception e){
            return false;
        }
        return isValid;
    }
}
