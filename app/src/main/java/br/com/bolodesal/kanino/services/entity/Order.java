package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Thiago Ferreira on 22/11/2015.
 */
public class Order {
    @SerializedName("dataPedido")
    @Expose
    private String dataPedido;

    @SerializedName("idTipoPagto")
    @Expose
    private int idTipoPagto;

    @SerializedName("idAplicacao")
    @Expose
    private int idAplicacao;

    @SerializedName("idCliente")
    @Expose
    private int idCliente;

    @SerializedName("idEndereco")
    @Expose
    private int idEndereco;

    @SerializedName("idStatus")
    @Expose
    private int idStatus;

    public void setDataPedido(String dataPedido) {
        this.dataPedido = dataPedido;
    }

    public void setIdTipoPagto(int idTipoPagto) {
        this.idTipoPagto = idTipoPagto;
    }

    public void setIdAplicacao(int idAplicacao) {
        this.idAplicacao = idAplicacao;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public String getDataPedido() {
        return dataPedido;
    }

    public int getIdTipoPagto() {
        return idTipoPagto;
    }

    public int getIdAplicacao() {
        return idAplicacao;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public int getIdEndereco() {
        return idEndereco;
    }

    public int getIdStatus() {
        return idStatus;
    }
}
