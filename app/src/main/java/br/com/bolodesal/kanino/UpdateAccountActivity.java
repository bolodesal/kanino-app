package br.com.bolodesal.kanino;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Optional;
import com.mobsandgeeks.saripaar.annotation.Or;
import com.mobsandgeeks.saripaar.annotation.Order;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Pattern;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import br.com.bolodesal.kanino.interfaces.validation.CheckPasswordCustomer;
import br.com.bolodesal.kanino.interfaces.validation.HasCostumerWithEmail;
import br.com.bolodesal.kanino.interfaces.validation.isDate;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.CustomerRequest;
import br.com.bolodesal.kanino.services.entity.CustomerResponse;
import br.com.bolodesal.kanino.services.models.*;
import br.com.bolodesal.kanino.services.models.Error;
import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.Response;
import retrofit.Retrofit;


public class UpdateAccountActivity extends AppCompatActivity implements Validator.ValidationListener {

    private KaninoServices service;
    private SharedPreferences prefs;
    private CustomerResponse customer;
    private CustomerRequest customerUpdateRequest = new CustomerRequest();
    private EditText txtNome;
    private Toolbar mToolBar;

    @Order(1)
    @Email(sequence = 1, message = "E-mail inválido")
    @HasCostumerWithEmail(sequence = 2)
    private EditText txtEmail;

    @Order(4)
    @CheckPasswordCustomer(sequence = 3)
    private EditText txtSenhaAtual;

    @Order(5)
    @Password(sequence = 1, min = 8, message = "A senha deve conter no mínimo 8 caracteres")
    @Length(sequence = 2, max = 64, message = "A senha deve conter no máximo 64 caracteres")
    private EditText txtSenha;

    @Order(6)
    @ConfirmPassword(message = "As senhas não conferem")
    private EditText txtConfirmaSenha;

    private EditText txtCPF;
    private EditText txtCelular;

    @Order(4)
    @Pattern(sequence = 1, regex = "^(?:(?:31(\\/)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$", message = "Data de nascimento inválida (dd/mm/aaaa)")
    @isDate(sequence = 2, message = "Verifique sua data de nascimento")
    private EditText txtDtNasc;

    private Validator validator;
    private String oldEmailValue;
    private String oldSenhaAtualValue;
    private FrameLayout flProgressEmail;
    private Button btnAlterar;
    private Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;
    private ProgressDialog progressDialog;
    private ComponentName componentName;
    private LinearLayout container;
    private FrameLayout fLProgress;
    private FrameLayout fLProgressSenhaAtual;
    private int idUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_account);

        final Animation animScale = AnimationUtils.loadAnimation(UpdateAccountActivity.this, R.anim.anim_scale);

        componentName = getCallingActivity();
        service = RestService.getInstance().getService();
        prefs = PreferenceManager.getDefaultSharedPreferences(UpdateAccountActivity.this);
        idUser = prefs.getInt(SharedPreferencesSettings.KEY_PREFS_ID_USER, 0);
        mToolBar = (Toolbar)findViewById(R.id.tb_main);
        mToolBar.setTitle("Atualizar Dados");
        mToolBar.setTitleTextColor(getResources().getColor(R.color.textWhite));
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtNome = (EditText)findViewById(R.id.txtCadNome);
        txtEmail = (EditText)findViewById(R.id.txtCadEmail);
        txtCPF = (EditText)findViewById(R.id.txtCadCPF);
        txtCelular = (EditText)findViewById(R.id.txtNumCelular);
        txtDtNasc = (EditText)findViewById(R.id.txtDataNasc);
        btnAlterar = (Button)findViewById(R.id.btnAlterar);
        flProgressEmail = (FrameLayout)findViewById(R.id.fLProgressEmail);
        fLProgressSenhaAtual = (FrameLayout)findViewById(R.id.fLProgressSenhaAtual);
        txtSenhaAtual = (EditText)findViewById(R.id.txtUpdateSenhaAtual);
        txtSenha = (EditText)findViewById(R.id.txtUpdateSenha);
        txtConfirmaSenha = (EditText)findViewById(R.id.txtUpdateConfirmaSenha);

        container = (LinearLayout) findViewById(R.id.container);
        fLProgress = (FrameLayout) findViewById(R.id.fLProgress);

        service.getCustomer(idUser).enqueue(new Callback<CustomerResponse>() {
            @Override
            public void onResponse(Response<CustomerResponse> response, Retrofit retrofit) {
                fLProgress.setVisibility(View.GONE);

                if (response.code() == 200) {
                    customer = response.body();
                    txtNome.setText(customer.getNomeCompletoCliente());
                    txtEmail.setText(customer.getEmailCliente());
                    oldEmailValue = txtEmail.getText().toString();
                    oldSenhaAtualValue = txtSenhaAtual.getText().toString();
                    txtCPF.setText(customer.getCpfCliente());
                    txtCelular.setText(customer.getCelularCliente());
                    txtDtNasc.setText(customer.getDtNascCliente().toString().substring(0, 2) + "/" + customer.getDtNascCliente().toString().substring(2, 4) + "/" + customer.getDtNascCliente().toString().substring(4, 8));
                    container.setVisibility(View.VISIBLE);
                }
                else
                {
                    if (response != null && !response.isSuccess() && response.errorBody() != null) {
                        Converter<ResponseBody, GlobalResponse<Error<ArrayList<String>>>> errorConverter = retrofit.responseConverter(new TypeToken<GlobalResponse<Error<ArrayList<String>>>>() {}.getType(), new Annotation[0]);
                        try {
                            GlobalResponse<Error<ArrayList<String>>> r = errorConverter.convert(response.errorBody());

                            String messages = "";

                            for(int i = 0; i < r.getResponse().getErrors().size(); i++){
                                messages += String.format("%s\n", r.getResponse().getErrors().get(i));
                            }

                            progressDialog.dismiss();
                            new SweetAlertDialog(UpdateAccountActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText(messages).show();

                        } catch (IOException e) {
                            progressDialog.dismiss();
                            new SweetAlertDialog(UpdateAccountActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText("Ops! Houve um problema, estamos trabalhando na solução.").show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(UpdateAccountActivity.this, "Não foi possível estabelecer comunicação com nossos serviços.", Toast.LENGTH_LONG).show();
            }
        });

        btnAlterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                animScale.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (btnAlterar.getText().toString().equals(getResources().getString(R.string.txtBtnAlterar))) {
                            txtNome.setEnabled(true);
                            txtEmail.setEnabled(true);
                            txtSenhaAtual.setEnabled(true);
                            txtSenha.setEnabled(true);
                            txtConfirmaSenha.setEnabled(true);
                            txtDtNasc.setEnabled(true);
                            txtCelular.setEnabled(true);
                            btnAlterar.setText(getResources().getString(R.string.txtBtnSalvar));
                        } else {
                            if(txtSenhaAtual.length() == 0)
                            {
                                validator.removeRules(txtSenha);
                                validator.removeRules(txtConfirmaSenha);
                            }
                            validator.validate(true);
                            progressDialog.setMessage("Validando dados...");
                            progressDialog.show();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });

        txtCPF.setFocusable(false);

        txtCPF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UpdateAccountActivity.this, "Não é permitido alterar seu CPF", Toast.LENGTH_LONG).show();
            }
        });

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        txtDtNasc.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    new DatePickerDialog(UpdateAccountActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });

        progressDialog = new ProgressDialog(UpdateAccountActivity.this, R.style.MaterialDrawerBaseTheme_Dialog);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);

        validator = new Validator(this);
        validator.registerAnnotation(HasCostumerWithEmail.class);
        validator.registerAnnotation(CheckPasswordCustomer.class);
        validator.setValidationListener(this);

        txtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && txtEmail.getText().toString().length() > 0 && !oldEmailValue.equals(txtEmail.getText().toString())) {
                    flProgressEmail.setVisibility(View.VISIBLE);
                    validator.validateTill(v, true);
                }
            }
        });

        txtSenhaAtual.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && txtSenhaAtual.getText().toString().length() > 0 && !oldSenhaAtualValue.equals(txtSenhaAtual.getText().toString())) {
                    fLProgressSenhaAtual.setVisibility(View.VISIBLE);
                    validator.validateTill(v, true);
                }
            }
        });


    }

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

        txtDtNasc.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_update_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onValidationSucceeded() {
        updateCustomer();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        flProgressEmail.setVisibility(View.GONE);
        fLProgressSenhaAtual.setVisibility(View.GONE);
        progressDialog.dismiss();

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

        //Procura todos os EditText e coloca o foco no que estiver com erro
        LinearLayout mainLayoutUpdate = (LinearLayout)findViewById(R.id.mainLayoutUpdate);
        View view = traverseEditTexts(mainLayoutUpdate);
        if(view != null)
        {
            view.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private EditText traverseEditTexts(ViewGroup v){
        EditText invalid = null;
        for (int i = 0; i < v.getChildCount(); i++)
        {
            Object child = v.getChildAt(i);
            if (child instanceof EditText)
            {
                EditText e = (EditText)child;
                if(e.getError() != null)
                {
                    return e;
                }
            }
            else if(child instanceof ViewGroup)
            {
                invalid = traverseEditTexts((ViewGroup)child);  // Recursive call.
                if(invalid != null)
                {
                    break;
                }
            }
        }
        return invalid;
    }

    public void updateCustomer(){
        progressDialog.setMessage("Alterando dados do usuário...");

        //Customer
        customerUpdateRequest.setIdCliente(idUser);
        customerUpdateRequest.setNomeCompleto(txtNome.getText().toString());
        customerUpdateRequest.setCpf(null);
        customerUpdateRequest.setDtNasc(txtDtNasc.getText().toString().replace("/", ""));
        customerUpdateRequest.seteMail(txtEmail.getText().toString());
        customerUpdateRequest.setSenhaCliente(txtSenha.getText().toString());
        customerUpdateRequest.setCelular(txtCelular.getText().toString());
        customerUpdateRequest.setRecebeNewsletter(false);
        customerUpdateRequest.setTelComercial(null);
        customerUpdateRequest.setTelResidencial(null);

        service.updateCustomer(customerUpdateRequest).enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Response<GlobalResponse> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    progressDialog.dismiss();

                    txtNome.setEnabled(false);
                    txtNome.clearFocus();
                    txtEmail.setEnabled(false);
                    txtEmail.clearFocus();
                    txtSenhaAtual.setEnabled(false);
                    txtSenhaAtual.clearFocus();
                    txtSenha.setEnabled(false);
                    txtSenha.clearFocus();
                    txtConfirmaSenha.setEnabled(false);
                    txtConfirmaSenha.clearFocus();
                    txtDtNasc.setEnabled(false);
                    txtDtNasc.clearFocus();
                    txtCelular.setEnabled(false);
                    txtCelular.clearFocus();
                    btnAlterar.setText(getResources().getString(R.string.txtBtnAlterar));

                    new SweetAlertDialog(UpdateAccountActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Pronto!")
                            .setContentText("Dados atualizados com sucesso!")
                            .setConfirmText("OK")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    finish();
                                }
                            })
                            .showCancelButton(false).show();
                }
                else {
                    if (response != null && !response.isSuccess() && response.errorBody() != null) {
                        Converter<ResponseBody, GlobalResponse<Error<ArrayList<String>>>> errorConverter = retrofit.responseConverter(new TypeToken<GlobalResponse<Error<ArrayList<String>>>>() {}.getType(), new Annotation[0]);
                        try {
                            GlobalResponse<Error<ArrayList<String>>> r = errorConverter.convert(response.errorBody());

                            String messages = "";

                            for(int i = 0; i < r.getResponse().getErrors().size(); i++){
                                messages += String.format("%s\n", r.getResponse().getErrors().get(i));
                            }

                            progressDialog.dismiss();
                            new SweetAlertDialog(UpdateAccountActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText(messages).show();

                        } catch (IOException e) {
                            progressDialog.dismiss();
                            new SweetAlertDialog(UpdateAccountActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText("Ops! Houve um problema, estamos trabalhando na solução.").show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
                new SweetAlertDialog(UpdateAccountActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText("Não foi possível estabelecer comunicação com nossos serviços.").show();
            }
        });
    }
}
