package br.com.bolodesal.kanino.interfaces.validation;

import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.com.bolodesal.kanino.validation.CustomerWithEmailRule;

/**
 * Created by Edgar Maia on 20/11/2015.
 */
@ValidateUsing(CustomerWithEmailRule.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface HasCostumerWithEmail {
    int messageResId()   default -1;
    String message()     default "E-mail já cadastrado";
    int sequence()       default -1;
}
