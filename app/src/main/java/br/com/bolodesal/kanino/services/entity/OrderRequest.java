package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yoopye on 25/11/2015.
 */
public class OrderRequest {
    @SerializedName("dataPedido")
    @Expose
    private String dataPedido;

    @SerializedName("idTipoPagto")
    @Expose
    private int idTipoPagto;

    @SerializedName("idAplicacao")
    @Expose
    private int idAplicacao;

    @SerializedName("idCliente")
    @Expose
    private int idCliente;

    @SerializedName("idEndereco")
    @Expose
    private int idEndereco;

    @SerializedName("idPedido")
    @Expose
    private int idPedido;

    @SerializedName("statusPedido")
    @Expose
    private String statusPedido;

    @SerializedName("totalItens")
    @Expose
    private int totalItens;

    @SerializedName("valorTotal")
    @Expose
    private double valorTotal;

    public void setDataPedido(String dataPedido) {
        this.dataPedido = dataPedido;
    }

    public void setIdTipoPagto(int idTipoPagto) {
        this.idTipoPagto = idTipoPagto;
    }

    public void setIdAplicacao(int idAplicacao) {
        this.idAplicacao = idAplicacao;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public void setStatusPedido(String statusPedido) {
        this.statusPedido = statusPedido;
    }

    public void setTotalItens(int totalItens) {
        this.totalItens = totalItens;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getDataPedido() {
        return dataPedido;
    }

    public int getIdTipoPagto() {
        return idTipoPagto;
    }

    public int getIdAplicacao() {
        return idAplicacao;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public int getIdEndereco() {
        return idEndereco;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public String getStatusPedido() {
        return statusPedido;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public int getTotalItens() {
        return totalItens;
    }
}
