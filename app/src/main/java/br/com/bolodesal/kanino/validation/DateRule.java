package br.com.bolodesal.kanino.validation;

import android.util.Log;

import com.mobsandgeeks.saripaar.AnnotationRule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.bolodesal.kanino.interfaces.validation.isDate;

/**
 * Created by Edgar Maia on 22/11/2015.
 */
public class DateRule extends AnnotationRule<isDate, String> {

    protected DateRule(isDate date) {
        super(date);
    }

    @Override
    public boolean isValid(String date) {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat(mRuleAnnotation.format());
            String min = mRuleAnnotation.minDate();
            String max = mRuleAnnotation.maxDate();
            if(min.isEmpty())
            {
                min = sdf.format(new Date());
            }

            if(max.isEmpty())
            {
                max = sdf.format(new Date());
            }
            Date minDate = sdf.parse(min);
            Date maxDate = sdf.parse(max);
            Date convertedDate = sdf.parse(date);

            Calendar cal = Calendar.getInstance();
            if(date.matches("^([0-9]{2}(/)[0-9]{4})$"))
            {
                //min
                cal.setTime(minDate);
                cal.add(Calendar.MONTH, -1);
                minDate = cal.getTime();

                //max
                cal.setTime(maxDate);
                cal.add(Calendar.MONTH, +1);
                maxDate = cal.getTime();
            }

            if(convertedDate.after(minDate) && convertedDate.before(maxDate))
            {
                return true;
            }
        } catch(ParseException ex) {
            return false;
        }
        return false;
    }
}
