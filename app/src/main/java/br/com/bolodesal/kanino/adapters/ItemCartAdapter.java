package br.com.bolodesal.kanino.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toolbar;

import java.text.DecimalFormat;
import java.util.List;

import br.com.bolodesal.kanino.CartActivity;
import br.com.bolodesal.kanino.MainActivity;
import br.com.bolodesal.kanino.R;
import br.com.bolodesal.kanino.domain.Product;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.Cart;
import br.com.bolodesal.kanino.services.entity.ItemCart;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Thiago Ferreira on 02/11/2015.
 */
public class ItemCartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater mLayoutInflater;
    private List<Product> mList;
    private List<ItemCart> items;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private Context mContext;
    private float scale;
    private int width, height, roundPixels;

    public ItemCartAdapter(Context c, List<Product> l, List<ItemCart> it){
        mContext = c;
        mList = l;
        items = it;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        scale = mContext.getResources().getDisplayMetrics().density;
        //width = (mContext.getResources().getDisplayMetrics().widthPixels - (int)(14 * scale + 0.5f)) / 2;
        width = 20;
        height = width;
        roundPixels = (int)(2 * scale + 0.5f);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder mvh;
        if(viewType == 0) {
            View v = mLayoutInflater.inflate(R.layout.item_cart_card, viewGroup, false);
            mvh = new MyViewHolder(v);
        }else {
            View v = mLayoutInflater.inflate(R.layout.progress_item, viewGroup, false);
            mvh = new ProgressViewHolder(v);
        }
        return mvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ProgressViewHolder){
            ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
        } else if(holder instanceof MyViewHolder && mList.size() > 0 && position < mList.size()) {
            ((MyViewHolder)holder).tvProductName.setText(mList.get(position).getName());
            //((MyViewHolder)holder).tvProductPrice.setText(mList.get(position).getPrice());

            ((MyViewHolder)holder).ivProduct.setImageBitmap(mList.get(position).getPhoto());

            DecimalFormat currencyType = new DecimalFormat("0.00");
            String pWithDiscount = "R$ " +  currencyType.format(mList.get(position).getAmount() * mList.get(position).getPrice());

            ((MyViewHolder)holder).tvTotalProductPrice.setText(pWithDiscount);
            ((MyViewHolder)holder).txtAmount.setText(Integer.toString(mList.get(position).getAmount()));

            /*Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), mList.get(position).getPhoto());
            width = bitmap.getWidth();
            height = bitmap.getHeight();

            bitmap = ImageHelper.getRoundedCornerBitmap(mContext, bitmap, 10, width, height, true, true, true, true);
            ((MyViewHolder)holder).ivProduct.setImageBitmap(bitmap);*/

        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(Product product, int position){
        mList.add(product);
        notifyItemInserted(position);
    }

    public int progressBarPosition = -1;

    public void addProgressBar(){
        mList.add(null);
        progressBarPosition = mList.size()-1;
        notifyItemInserted(mList.size());
    }

    public void removeProgressBar(){
        mList.remove(mList.size() - 1);
        progressBarPosition = -1;
        notifyItemRemoved(mList.size() - 1);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == progressBarPosition) {
            return 1; //progress bar
        } else
            return 0; //item normal
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar)v.findViewById(R.id.progressBar1);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivProduct;
        public TextView tvProductName;
        public TextView tvProductPrice;
        public TextView tvTotalProductPrice;
        public TextView txtAmount;
        public int control = 1, controlAux = 0;
        public int amount;
        public ProgressBar progressPrice;
        public ImageView ivDelete;
        public TextView txtLess;

        public MyViewHolder(View itemView) {
            super(itemView);

            txtLess =  (TextView) itemView.findViewById(R.id.txtLess);
            ivProduct = (ImageView) itemView.findViewById(R.id.ivOrder);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductName);
            tvProductPrice = (TextView) itemView.findViewById(R.id.tvProductPrice);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
            tvTotalProductPrice = (TextView) itemView.findViewById(R.id.tvTotalProductPrice);
            progressPrice = (ProgressBar) itemView.findViewById(R.id.progressPrice);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);

            final Animation animScale = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale);

            final Cart cart = new Cart(mContext);

            txtLess.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(animScale);
                    if (control != controlAux) {

                        controlAux = control;
                        int amount = Integer.parseInt(txtAmount.getText().toString());

                        if (amount > 1) {
                            amount--;
                            mList.get(getAdapterPosition()).setAmount(amount);

                            progressPrice.setVisibility(View.VISIBLE);
                            tvTotalProductPrice.setVisibility(View.GONE);

                            ItemCart item = new ItemCart();
                            final Integer p = getPosition();
                            item.setId(mList.get(p).getId());
                            item.setName(mList.get(p).getName());
                            item.setAmount(mList.get(p).getAmount());

                            cart.update(item);

                            CartActivity activity = (CartActivity) mContext;

                            double price = activity.getTotalPrice() - mList.get(getPosition()).getPrice();
                            int am = activity.getTotalItems() - 1;

                            activity.setTotalPriceAndAmount(price, am);

                            final int finalAmount = amount;

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    SystemClock.sleep(300);
                                    ((CartActivity) mContext).runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            control++;

                                            mList.get(p).setTotalPrice(finalAmount * mList.get(p).getPrice());
                                            Log.d("Posição", String.valueOf(p));


                                            progressPrice.setVisibility(View.GONE);
                                            tvTotalProductPrice.setVisibility(View.VISIBLE);
                                            notifyDataSetChanged();
                                        }
                                    });
                                }
                            }).start();
                        }
                        else{
                            new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText("Para remover o produto clique no X!")
                                    .show();

                            control++;
                        }
                    }
                }
            });

            TextView txtMore = (TextView) itemView.findViewById(R.id.txtMore);
                                    txtMore.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            v.startAnimation(animScale);
                                            if (control != controlAux) {

                                                controlAux = control;
                                                int amount = Integer.parseInt(txtAmount.getText().toString());

                                                if (amount < 5) {
                                                    amount++;

                                                    mList.get(getPosition()).setAmount(amount);


                                                    progressPrice.setVisibility(View.VISIBLE);
                                                    tvTotalProductPrice.setVisibility(View.GONE);

                                                    ItemCart item = new ItemCart();
                                                    item.setId(mList.get(getPosition()).getId());
                                                    item.setName(mList.get(getPosition()).getName());
                                                    item.setAmount(mList.get(getPosition()).getAmount());

                                                    cart.update(item);

                                                    CartActivity activity = (CartActivity) mContext;

                                                    double price = activity.getTotalPrice() + mList.get(getPosition()).getPrice();
                                                    int am = activity.getTotalItems() + 1;

                                                    activity.setTotalPriceAndAmount(price, am);

                                                    final int finalAmount = amount;
                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            SystemClock.sleep(300);
                                                            ((CartActivity) mContext).runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    control++;

                                                                    mList.get(getPosition()).setTotalPrice(finalAmount * mList.get(getPosition()).getPrice());


                                                                    progressPrice.setVisibility(View.GONE);
                                                                    tvTotalProductPrice.setVisibility(View.VISIBLE);
                                                                    notifyDataSetChanged();
                                                                }
                                                            });
                                }
                            }).start();
                        }
                        else{
                            new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText("A quantidade máxima é 5!")
                                    .show();
                            control++;
                        }
                    }
                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(animScale);
                    new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Deseja continuar?")
                            .setContentText("Confirme se deseja remover o produto.")
                            .setCancelText("Não, cancelar!")
                            .setConfirmText("Sim, remover!")
                            .showCancelButton(true)
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.cancel();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    /*ItemCart item = new ItemCart();
                                    item.setId(mList.get(getPosition()).getId());

                                    cart.delete(item);

                                    CartActivity activity = (CartActivity) mContext;

                                    double price = activity.getTotalPrice() - mList.get(getPosition()).getPrice() * mList.get(getPosition()).getAmount();
                                    int am = activity.getTotalItems() - mList.get(getPosition()).getAmount();

                                    mList.remove(getPosition());
                                    notifyItemRemoved(getPosition());

                                    activity.setTotalPriceAndAmount(price, am);

                                    int size = cart.getCount();

                                    activity.setNewSize(size);*/

                                    sweetAlertDialog
                                            .setTitleText("Pronto!")
                                            .setContentText("Produto removido com sucesso.")
                                            .setConfirmText("OK")
                                            .showCancelButton(false)
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                                    ItemCart item = new ItemCart();
                                                    item.setId(mList.get(getPosition()).getId());

                                                    cart.delete(item);

                                                    CartActivity activity = (CartActivity) mContext;

                                                    double price = activity.getTotalPrice() - mList.get(getPosition()).getPrice() * mList.get(getPosition()).getAmount();
                                                    int am = activity.getTotalItems() - mList.get(getPosition()).getAmount();

                                                    mList.remove(getPosition());
                                                    notifyItemRemoved(getPosition());

                                                    activity.setTotalPriceAndAmount(price, am);

                                                    int size = cart.getCount();

                                                    activity.setNewSize(size);
                                                    if (mList.size() == 0) {
                                                        sweetAlertDialog.dismiss();
                                                        activity.resetActitivyIsEmpytCart();
                                                    }

                                                    sweetAlertDialog.dismiss();
                                                    /*ItemCart item = new ItemCart();
                                                    item.setId(mList.get(getPosition()).getId());

                                                    cart.delete(item);

                                                    CartActivity activity = (CartActivity) mContext;

                                                    double price = activity.getTotalPrice() - mList.get(getPosition()).getPrice() * mList.get(getPosition()).getAmount();
                                                    int am = activity.getTotalItems() - mList.get(getPosition()).getAmount();

                                                    mList.remove(getPosition());
                                                    notifyItemRemoved(getPosition());

                                                    activity.setTotalPriceAndAmount(price, am);

                                                    int size = cart.getCount();

                                                    activity.setNewSize(size);

                                                    if(mList.size() == 0)
                                                    {
                                                        sweetAlertDialog.dismiss();
                                                        activity.resetActitivyIsEmpytCart();
                                                    }*/
                                                }
                                            }).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                }
                            }).show();
                }
            });

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }
}
