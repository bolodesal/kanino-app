package br.com.bolodesal.kanino.interfaces.validation;

import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.com.bolodesal.kanino.validation.CustomerPasswordRule;

/**
 * Created by Edgar Maia on 29/11/2015.
 */
@ValidateUsing(CustomerPasswordRule.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CheckPasswordCustomer {
    int messageResId()   default -1;
    String message()     default "Senha inválida";
    int sequence()       default -1;
}
