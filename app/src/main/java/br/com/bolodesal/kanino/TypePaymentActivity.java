package br.com.bolodesal.kanino;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Pattern;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import br.com.bolodesal.kanino.enums.ApplicationsEnum;
import br.com.bolodesal.kanino.enums.OrderStatusEnum;
import br.com.bolodesal.kanino.interfaces.validation.isDate;
import br.com.bolodesal.kanino.services.Cart;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.ItemCart;
import br.com.bolodesal.kanino.services.entity.Order;
import br.com.bolodesal.kanino.services.entity.OrderItem;
import br.com.bolodesal.kanino.services.entity.SendOrderRequest;
import br.com.bolodesal.kanino.enums.PaymentMethodsEnum;
import br.com.bolodesal.kanino.services.models.*;
import br.com.bolodesal.kanino.services.models.Error;
import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;
import br.com.bolodesal.kanino.util.DatePicker;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.Response;
import retrofit.Retrofit;

public class TypePaymentActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private RadioButton rbBillet;
    private RadioButton rbCredit;
    private DatePickerDialog.OnDateSetListener date;
    private Calendar myCalendar;
    private EditText creditCard;
    private EditText validate;
    private EditText securiyCode;
    private Button finish;
    private CreditCardController creditCardController;
    private ProgressDialog progressDialog;
    private SharedPreferences prefs;
    private int addressId;

    private List<ItemCart> items;

    private Cart cart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_payment);

        final Animation animScale = AnimationUtils.loadAnimation(TypePaymentActivity.this, R.anim.anim_scale);

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle("Formas de pagamento");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.textWhite));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        addressId = intent.getIntExtra("idAddress", 1);

        final CardView cvBilletBank = (CardView) findViewById(R.id.cvBilletBank);
        final CardView cvCreditCard = (CardView) findViewById(R.id.cvCreditCard);

        final LinearLayout lLInforCreditCard = (LinearLayout) findViewById(R.id.lLInforCreditCard);

        rbBillet = (RadioButton) findViewById(R.id.rbBillet);
        rbCredit = (RadioButton) findViewById(R.id.rbCredit);

        myCalendar = Calendar.getInstance();
        creditCard = (EditText) findViewById(R.id.txtNumberCreditCard);
        validate = (EditText) findViewById(R.id.txtValidate);
        securiyCode = (EditText) findViewById(R.id.txtSecurityCode);
        finish = (Button) findViewById(R.id.btnFinish);

        prefs = PreferenceManager.getDefaultSharedPreferences(TypePaymentActivity.this);

        cart = new Cart(this);
        items = cart.getAllItems();

        cvBilletBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbBillet.performClick();
            }
        });

        cvCreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbCredit.performClick();
            }
        });

        rbBillet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lLInforCreditCard.setVisibility(View.GONE);
                rbCredit.setChecked(false);
            }
        });

        rbCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lLInforCreditCard.setVisibility(View.VISIBLE);
                rbBillet.setChecked(false);
            }
        });

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(android.widget.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                validate.setText(String.format("%02d/%d", monthOfYear + 1, year));
            }
        };

        validate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (validate.getText().toString().matches("^([0-9]{2}(/)[0-9]{4})$")) {
                        String[] validateData = validate.getText().toString().split("/");
                        new DatePickerDialog(TypePaymentActivity.this, date, Integer.parseInt(validateData[1]), Integer.parseInt(validateData[0]) - 1, myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    } else {
                        new DatePickerDialog(TypePaymentActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                    }
                }
            }
        });

        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate.getText().toString().matches("^([0-9]{2}(/)[0-9]{4})$"))
                {
                    String [] validateData = validate.getText().toString().split("/");
                    new DatePickerDialog(TypePaymentActivity.this, date, Integer.parseInt(validateData[1]), Integer.parseInt(validateData[0]) - 1, myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }else{
                    new DatePickerDialog(TypePaymentActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });

        creditCardController = new CreditCardController(creditCard, validate, securiyCode, new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                sendOrder();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                progressDialog.dismiss();
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(TypePaymentActivity.this);

                    // Display error messages ;)
                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
                    } else {
                        Toast.makeText(TypePaymentActivity.this, message, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                animScale.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        progressDialog = new ProgressDialog(TypePaymentActivity.this, R.style.MaterialDrawerBaseTheme_Dialog);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setMessage("Validando dados...");
                        progressDialog.setCancelable(false);
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.show();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                SystemClock.sleep(1000);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!rbBillet.isChecked() && !rbCredit.isChecked()) {
                                            progressDialog.dismiss();
                                            new SweetAlertDialog(TypePaymentActivity.this, SweetAlertDialog.ERROR_TYPE)
                                                    .setTitleText("Oops!")
                                                    .setContentText("Escolha uma forma de pagamento.").show();
                                        } else {
                                            creditCardController.validate();
                                        }
                                    }
                                });
                            }
                        }).start();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /*
    *
    * VALIDATION CLASSES
    *
    * */

    class CreditCardController {

        @com.mobsandgeeks.saripaar.annotation.Order(1)
        @Length(sequence = 1, min = 15, max = 16, message = "Número do cartão de crédito inválido")
        private EditText creditCard;

        @com.mobsandgeeks.saripaar.annotation.Order(2)
        @Pattern(sequence = 2, regex = "^([0-9]{2}(/)[0-9]{4})$", message = "Data inválida")
        @isDate(sequence = 3, format = "MM/yyyy", maxDate = "12/2030", message = "Data de vencimento inválida")
        private EditText validate;

        @com.mobsandgeeks.saripaar.annotation.Order(3)
        @Length(sequence = 1, min = 3, max = 3, message = "Código de segurança inválido")
        private EditText securityCode;

        private Validator validator;

        public CreditCardController(EditText creditCard, EditText validate, EditText securityCode, Validator.ValidationListener listener){

            this.creditCard = creditCard;
            this.validate = validate;
            this.securityCode = securityCode;
            validator = new Validator(this);
            validator.registerAnnotation(isDate.class);
            validator.setValidationListener(listener);

        }

        public void validate(){
            validator.validate();
        }
    }

    /*
    *
    * END VALIDATION CLASSES
    *
    * */

    private void sendOrder(){
        progressDialog.setMessage("Efetuando pagamento...");
        Order orderHeader = new Order();
        Calendar calendar = Calendar.getInstance();
        int second = calendar.get(Calendar.SECOND);
        int minute = calendar.get(Calendar.MINUTE);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        orderHeader.setDataPedido(String.format("%02d", day) + "" + String.format("%02d", month) + "" + year + " " + hour + ":" + minute + ":" + second);

        int paymentMethod;

        if(rbBillet.isChecked())
        {
            paymentMethod = PaymentMethodsEnum.BANK_SLIP.getPaymentMethod();
            orderHeader.setIdStatus(OrderStatusEnum.AGUARDANDO_PAGAMENTO.getOrderStatus());
        }else if (rbCredit.isChecked())
        {
            paymentMethod = PaymentMethodsEnum.CREDIT_CARD.getPaymentMethod();
            orderHeader.setIdStatus(OrderStatusEnum.ENVIADO_TRANSPORTADORA.getOrderStatus());
        }else {
            paymentMethod = PaymentMethodsEnum.CREDIT_CARD.getPaymentMethod();
            orderHeader.setIdStatus(OrderStatusEnum.ENVIADO_TRANSPORTADORA.getOrderStatus());
        }

        orderHeader.setIdTipoPagto(paymentMethod);
        orderHeader.setIdAplicacao(ApplicationsEnum.KANNO_MOBILE.getApplication());
        orderHeader.setIdCliente(prefs.getInt(SharedPreferencesSettings.KEY_PREFS_ID_USER, 0));
        orderHeader.setIdEndereco(addressId);


        List<OrderItem> orderItems = new ArrayList<OrderItem>();

        Cart cart = new Cart(TypePaymentActivity.this);
        List<ItemCart> items;

        items = cart.getAllItems();

        for(int i = 0; i < items.size(); i++){
            OrderItem oI = new OrderItem();
            oI.setIdProduto(items.get(i).getId());
            oI.setPrecoVendaItem(items.get(i).getPrice());
            oI.setQtdProduto(items.get(i).getAmount());
            orderItems.add(oI);
        }

        SendOrderRequest sendOrderRequest = new SendOrderRequest();
        sendOrderRequest.setOrder(orderHeader);
        sendOrderRequest.setOrderItems(orderItems);

        KaninoServices service = RestService.getInstance().getService();
        service.sendOrderRequest(sendOrderRequest).enqueue(new Callback<SendOrderRequest>() {
            @Override
            public void onResponse(Response<SendOrderRequest> response, Retrofit retrofit) {
                progressDialog.dismiss();
                if(response.code() == 200)
                {
                    clearCart();
                    new SweetAlertDialog(TypePaymentActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Pronto!")
                            .setContentText("Pedido realizado com sucesso!")
                            .setConfirmText("OK")
                            .setConfirmClickListener(null)
                            .showCancelButton(false).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    Intent i = new Intent(TypePaymentActivity.this, MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            }).show();
                }
                else
                {
                    if (response != null && !response.isSuccess() && response.errorBody() != null)
                    {
                        Converter<ResponseBody, GlobalResponse<Error<ArrayList<String>>>> errorConverter = retrofit.responseConverter(new TypeToken<GlobalResponse<Error<ArrayList<String>>>>() {}.getType(), new Annotation[0]);
                        try {
                            GlobalResponse<Error<ArrayList<String>>> r = errorConverter.convert(response.errorBody());

                            String messages = "";

                            for(int i = 0; i < r.getResponse().getErrors().size(); i++){
                                messages += String.format("%s\n", r.getResponse().getErrors().get(i));
                            }

                            new SweetAlertDialog(TypePaymentActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .showCancelButton(true)
                                    .setCancelText("Meu carrinho")
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            Intent i = new Intent(TypePaymentActivity.this, CartActivity.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    })
                                    .setContentText(messages).show();

                        } catch (IOException e) {

                            new SweetAlertDialog(TypePaymentActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText("Ops! Houve um problema, estamos trabalhando na solução.").show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
                new SweetAlertDialog(TypePaymentActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText("Estamos passando por problemas técnicos. Tente novamente mais tarde.").show();
            }
        });
    }

    private void clearCart(){
        for (int i = 0; i < items.size(); i++) {
            ItemCart itemCart = new ItemCart();
            itemCart.setId(items.get(i).getId());
            cart.delete(itemCart);
        }
    }
}
