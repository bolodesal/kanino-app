package br.com.bolodesal.kanino.adapters;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import br.com.bolodesal.kanino.MainActivity;
import br.com.bolodesal.kanino.ProductsByCategoryActivity;
import br.com.bolodesal.kanino.R;
import br.com.bolodesal.kanino.SearchableActivity;
import br.com.bolodesal.kanino.services.entity.ProductResponse;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.Cart;
import br.com.bolodesal.kanino.services.entity.ItemCart;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Thiago Ferreira on 12/10/2015.
 */
public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater mLayoutInflater;
    private List<ProductResponse> mList;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private Context mContext;
    private float scale;
    private int width, height, roundPixels;

    public ProductAdapter(Context c, List<ProductResponse> l){
        mContext = c;

        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        scale = mContext.getResources().getDisplayMetrics().density;
        /*width = (mContext.getResources().getDisplayMetrics().widthPixels - (int)(14 * scale + 0.5f)) / 2;
        height = width;*/
        roundPixels = (int)(2 * scale + 0.5f);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder mvh;
        if(viewType == 0) {
            View v = mLayoutInflater.inflate(R.layout.items_product_card, viewGroup, false);
            mvh = new MyViewHolder(v);
        }else {
            View v = mLayoutInflater.inflate(R.layout.progress_item, viewGroup, false);
            mvh = new ProgressViewHolder(v);
        }
        return mvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ProgressViewHolder){
            ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
        } else if(holder instanceof MyViewHolder/*mList.size() > 0 && position < mList.size()*/) {

            DecimalFormat currencyType = new DecimalFormat("0.00");
            String pWithDiscount = currencyType.format(mList.get(position).getPrecProduto() - mList.get(position).getDescontoPromocao());
            //mList.get(position).setId(position + 1);
            ((MyViewHolder)holder).tvProductName.setText(mList.get(position).getNomeProduto());
            ((MyViewHolder)holder).tvProductPrice.setText("R$ " + pWithDiscount);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                ((MyViewHolder)holder).ivProduct.setImageBitmap(mList.get(position).getImagem());
            }
            else{
                /*Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), mList.get(position).getPhoto());
                width = bitmap.getWidth();
                height = bitmap.getHeight();

                bitmap = ImageHelper.getRoundedCornerBitmap(mContext, bitmap, 10, width, height, false, false, true, true);
                ((MyViewHolder)holder).ivProduct.setImageBitmap(bitmap);*/
                ((MyViewHolder)holder).ivProduct.setImageBitmap(mList.get(position).getImagem());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(ProductResponse product, int position){
        mList.add(product);
        notifyItemInserted(position);
    }

    public int progressBarPosition = -1;

    public void addProgressBar(){
        mList.add(null);
        progressBarPosition = mList.size()-1;
        notifyItemInserted(mList.size());
    }

    public void removeProgressBar(){
        mList.remove(mList.size() - 1);
        progressBarPosition = -1;
        notifyItemRemoved(mList.size() - 1);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == progressBarPosition) {
            return 1; //progress bar
        } else
            return 0; //item normal
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar)v.findViewById(R.id.progressBar1);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView ivProduct;
        public TextView tvProductName;
        public TextView tvProductPrice;
        public Button btnAdicionar;

        public MyViewHolder(final View itemView) {
            super(itemView);
            ivProduct = (ImageView) itemView.findViewById(R.id.ivOrder);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductName);
            tvProductPrice = (TextView) itemView.findViewById(R.id.tvProductPrice);

            final Animation animScale = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale);

            btnAdicionar = (Button) itemView.findViewById(R.id.btnAdicionar);
            btnAdicionar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(animScale);
                    animScale.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            Cart cart = new Cart(mContext);

                            int idProduct = mList.get(getPosition()).getIdProduto();

                            int id = cart.getItem(idProduct).getId();

                            int asdf = id;

                            ItemCart item = new ItemCart();
                            item.setId(mList.get(getPosition()).getIdProduto());
                            item.setName(mList.get(getPosition()).getNomeProduto());
                            item.setPrice(mList.get(getPosition()).getPrecProduto() - mList.get(getPosition()).getDescontoPromocao());
                            item.setImage(mList.get(getPosition()).getStringImagem());
                            if (id != mList.get(getPosition()).getIdProduto()) {
                                item.setAmount(1);
                                cart.insert(item);

                                if (mContext instanceof MainActivity) {
                                    MainActivity activity = (MainActivity) mContext;
                                    int count = cart.getCount();
                                    activity.updateHotCount(count);
                                } else if (mContext instanceof ProductsByCategoryActivity) {
                                    ProductsByCategoryActivity activity = (ProductsByCategoryActivity) mContext;
                                    int count = cart.getCount();
                                    activity.updateHotCount(count);
                                } else if (mContext instanceof SearchableActivity) {
                                    SearchableActivity activity = (SearchableActivity) mContext;
                                    int count = cart.getCount();
                                    activity.updateHotCount(count);
                                }

                                new SweetAlertDialog(mContext, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Pronto!")
                                        .setContentText("Produto adicionado ao carrinho!")
                                        .show();
                            } else {
                                new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText("Este produto já está no carrinho!")
                                        .show();
                            }
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }
            });

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }
}