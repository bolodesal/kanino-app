package br.com.bolodesal.kanino.adapters;

import android.content.Context;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import br.com.bolodesal.kanino.CartActivity;
import br.com.bolodesal.kanino.R;
import br.com.bolodesal.kanino.domain.Product;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.Cart;
import br.com.bolodesal.kanino.services.entity.ItemCart;
import br.com.bolodesal.kanino.services.entity.OrderDetailsResponse;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Thiago Ferreira on 02/11/2015.
 */
public class OrderDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater mLayoutInflater;
    private List<OrderDetailsResponse> mList;
    private List<OrderDetailsResponse> items;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private Context mContext;
    private float scale;
    private int width, height, roundPixels;

    public OrderDetailsAdapter(Context c, List<OrderDetailsResponse> l, List<OrderDetailsResponse> it){
        mContext = c;
        mList = l;
        items = it;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        scale = mContext.getResources().getDisplayMetrics().density;
        //width = (mContext.getResources().getDisplayMetrics().widthPixels - (int)(14 * scale + 0.5f)) / 2;
        width = 20;
        height = width;
        roundPixels = (int)(2 * scale + 0.5f);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder mvh;
        if(viewType == 0) {
            View v = mLayoutInflater.inflate(R.layout.item_order_details_card, viewGroup, false);
            mvh = new MyViewHolder(v);
        }else {
            View v = mLayoutInflater.inflate(R.layout.progress_item, viewGroup, false);
            mvh = new ProgressViewHolder(v);
        }
        return mvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ProgressViewHolder){
            ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
        } else if(holder instanceof MyViewHolder && mList.size() > 0 && position < mList.size()) {
            ((MyViewHolder)holder).tvProductName.setText(mList.get(position).getNomeProduto());
            //((MyViewHolder)holder).tvProductPrice.setText(mList.get(position).getPrice());

            ((MyViewHolder)holder).ivProduct.setImageBitmap(mList.get(position).getImagem());

            DecimalFormat currencyType = new DecimalFormat("0.00");
            String pWithDiscount = "R$ " +  currencyType.format(mList.get(position).getQtdProduto() * mList.get(position).getPrecoVendaItem());
            String price = "R$ " + currencyType.format(mList.get(position).getPrecoVendaItem());

            ((MyViewHolder)holder).tvTotalProductPrice.setText(pWithDiscount);
            ((MyViewHolder)holder).txtPrice.setText(price);
            ((MyViewHolder)holder).txtAmount.setText(Integer.toString(mList.get(position).getQtdProduto()));

            /*Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), mList.get(position).getPhoto());
            width = bitmap.getWidth();
            height = bitmap.getHeight();

            bitmap = ImageHelper.getRoundedCornerBitmap(mContext, bitmap, 10, width, height, true, true, true, true);
            ((MyViewHolder)holder).ivProduct.setImageBitmap(bitmap);*/

        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(OrderDetailsResponse product, int position){
        mList.add(product);
        notifyItemInserted(position);
    }

    public int progressBarPosition = -1;

    public void addProgressBar(){
        mList.add(null);
        progressBarPosition = mList.size()-1;
        notifyItemInserted(mList.size());
    }

    public void removeProgressBar(){
        mList.remove(mList.size() - 1);
        progressBarPosition = -1;
        notifyItemRemoved(mList.size() - 1);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == progressBarPosition) {
            return 1; //progress bar
        } else
            return 0; //item normal
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar)v.findViewById(R.id.progressBar1);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivProduct;
        public TextView tvProductName;
        public TextView tvProductPrice;
        public TextView tvTotalProductPrice;
        public TextView txtAmount;
        public int control = 1, controlAux = 0;
        public int amount;
        public ProgressBar progressPrice;
        TextView txtPrice;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivProduct = (ImageView) itemView.findViewById(R.id.ivOrder);
            tvProductName = (TextView) itemView.findViewById(R.id.tvProductName);
            tvProductPrice = (TextView) itemView.findViewById(R.id.tvProductPrice);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
            tvTotalProductPrice = (TextView) itemView.findViewById(R.id.tvTotalProductPrice);
            progressPrice = (ProgressBar) itemView.findViewById(R.id.progressPrice);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }
}
