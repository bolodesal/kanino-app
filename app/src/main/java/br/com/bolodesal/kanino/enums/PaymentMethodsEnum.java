package br.com.bolodesal.kanino.enums;

/**
 * Created by Edgar Maia on 24/11/2015.
 */
public enum PaymentMethodsEnum {

    CREDIT_CARD(1),
    BANK_SLIP(2);

    private int paymentMethod;

    PaymentMethodsEnum(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public int getPaymentMethod(){
        return this.paymentMethod;
    }
}
