package br.com.bolodesal.kanino.services.entity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.sql.Blob;
import java.sql.SQLException;

/**
 * Created by Thiago Ferreira on 29/10/2015.
 */
public class Product {
    @SerializedName("idProduto")
    @Expose
    private int idProduto;

    @SerializedName("nomeProduto")
    @Expose
    private String nomeProduto;

    @SerializedName("descProduto")
    @Expose
    private String descProduto;

    @SerializedName("precProduto")
    @Expose
    private double precProduto;

    @SerializedName("descontoPromocao")
    @Expose
    private double descontoPromocao;

    @SerializedName("idCategoria")
    @Expose
    private int idCategoria;

    @SerializedName("ativoProduto")
    @Expose
    private char ativoProduto;

    @SerializedName("idUsuario")
    @Expose
    private int idUsuario;

    @SerializedName("qtdMinEstoque")
    @Expose
    private int qtdMinEstoque;

    @SerializedName("imagem")
    @Expose
    private Bitmap imagem;
    /*private Blob imagem;*/

    @SerializedName("nomeCategoria")
    @Expose
    private String nomeCategoria;

    public Product(int idProduto, String nomeProduto, String descProduto, double precProduto, double descontoPromocao, Blob imagem) throws SQLException {
        setIdProduto(idProduto);
        setNomeProduto(nomeProduto);
        setDescProduto(descProduto);
        setPrecProduto(precProduto);
        setDescontoPromocao(descontoPromocao);
        setImagem(imagem);
    }

    public int getIdProduto() {
        return idProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public String getDescProduto() {
        return descProduto;
    }

    public double getPrecProduto() {
        return precProduto;
    }

    public double getDescontoPromocao() {
        return descontoPromocao;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public char getAtivoProduto() {
        return ativoProduto;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public int getQtdMinEstoque() {
        return qtdMinEstoque;
    }

    public Bitmap getImagem(){
        return imagem;
    }

    public double getPriceWithDiscount(){
        return precProduto - descontoPromocao;
    }

    /*public Blob getImagem() {
        return imagem;
    }*/

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public void setDescProduto(String descProduto) {
        this.descProduto = descProduto;
    }

    public void setPrecProduto(double precProduto) {
        this.precProduto = precProduto;
    }

    public void setDescontoPromocao(double descontoPromocao) {
        this.descontoPromocao = descontoPromocao;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public void setAtivoProduto(char ativoProduto) {
        this.ativoProduto = ativoProduto;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setQtdMinEstoque(int qtdMinEstoque) {
        this.qtdMinEstoque = qtdMinEstoque;
    }

    public void setImagem(Blob imagem) throws SQLException {
        /*this.imagem = imagem;*/

        int blobLength = (int) imagem.length();
        byte[] arrayBytes = imagem.getBytes(1, blobLength);

        this.imagem = BitmapFactory.decodeByteArray(arrayBytes, 0, arrayBytes.length);
    }

    public String getNomeCategoria() {
        return nomeCategoria;
    }

    public void setNomeCategoria(String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }
}
