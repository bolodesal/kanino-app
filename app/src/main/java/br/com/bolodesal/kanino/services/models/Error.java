package br.com.bolodesal.kanino.services.models;

/**
 * Created by Edgar Maia on 21/11/2015.
 */
public class Error<T> {

    private T errors;

    public T getErrors() {
        return errors;
    }

    public void setErrors(T errors) {
        this.errors = errors;
    }

    public Error(){

    }

    public Error(T errors){
        this.errors = errors;
    }

}
