package br.com.bolodesal.kanino;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.text.DecimalFormat;

import br.com.bolodesal.kanino.services.Cart;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.ItemCart;
import br.com.bolodesal.kanino.services.entity.ProductResponse;
import br.com.bolodesal.kanino.util.UtilKanino;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class ProductDetailActivity extends AppCompatActivity {

    FrameLayout fLWithoutConnection;
    FrameLayout fLContainer;
    FrameLayout fLProgress;
    private FrameLayout fLEnganado;
    private FrameLayout fLNotFound;
    private FrameLayout fLSlowConnection;
    ProgressBar progressPrice;
    TextView txtAmount;
    String name;
    String category;
    int idProduct;
    double price;
    byte[] photo;
    int amount;
    public int control = 1, controlAux = 0;
    private Animation animScale;
    private Animation animScaleTxtMore;
    private Animation animScaleTxtLess;
    private int id;
    private boolean isKanino = false;

    UtilKanino utilKanino = new UtilKanino();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle("Detalhes do produto");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.textWhite));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fLWithoutConnection = (FrameLayout) findViewById(R.id.fLWithoutConnection);
        fLContainer = (FrameLayout) findViewById(R.id.fLContainer);
        fLProgress = (FrameLayout) findViewById(R.id.fLProgress);
        fLSlowConnection = (FrameLayout) findViewById(R.id.fLSlowConnection);
        progressPrice = (ProgressBar) findViewById(R.id.progressPrice);
        txtAmount = (TextView) findViewById(R.id.txtAmount);
        fLEnganado = (FrameLayout) findViewById(R.id.fLEnganado);
        fLNotFound = (FrameLayout) findViewById(R.id.fLNotFound);

        animScale = AnimationUtils.loadAnimation(ProductDetailActivity.this, R.anim.anim_scale);
        animScaleTxtMore = AnimationUtils.loadAnimation(ProductDetailActivity.this, R.anim.anim_scale);
        animScaleTxtLess = AnimationUtils.loadAnimation(ProductDetailActivity.this, R.anim.anim_scale);

        if(!utilKanino.hasConnected(this)){
            loadViewError();
        }
        else{
            loadView();
        }

        Button btnReloadS = (Button) findViewById(R.id.btnReloadS);
        btnReloadS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                fLProgress.setVisibility(View.VISIBLE);
                fLSlowConnection.setVisibility(View.GONE);
                loadView();
            }
        });
    }

    private void loadViewError(){
        fLWithoutConnection.setVisibility(View.VISIBLE);
        fLProgress.setVisibility(View.GONE);
        fLContainer.setVisibility(View.GONE);

        Button btnReload = (Button) findViewById(R.id.btnReload);
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                if (!utilKanino.hasConnected(ProductDetailActivity.this)) {
                    loadViewError();
                } else {
                    loadView();
                }
            }
        });

        Button btnReloadS = (Button) findViewById(R.id.btnReloadS);
        btnReloadS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                fLProgress.setVisibility(View.VISIBLE);
                fLSlowConnection.setVisibility(View.GONE);
                loadView();
            }
        });
    }

    private void loadView(){
        final ImageView productImage = (ImageView) findViewById(R.id.productImage);
        final TextView txtName = (TextView) findViewById(R.id.txtName);
        final TextView txtPrice = (TextView) findViewById(R.id.txtPrice);
        final TextView txtPriceWithDiscount = (TextView) findViewById(R.id.txtPriceWithDiscount);
        final TextView txtProductsTotalPrice = (TextView) findViewById(R.id.txtProductsTotalPrice);
        final TextView txtProductDescription = (TextView) findViewById(R.id.txtProductDescription);
        final TextView txtCategory = (TextView) findViewById(R.id.txtCategory);

        fLWithoutConnection.setVisibility(View.GONE);

        Intent intent = getIntent();

        String letra = intent.getStringExtra("id").substring(0, 1).toLowerCase();
        int length = intent.getStringExtra("id").length();

        String idString = "";

        //String idString = intent.getStringExtra("id").substring(1, 2);

        if(letra.equals("k")){
            isKanino = true;
        }

        for(int i = 2; i <= length; i++){
            String str = intent.getStringExtra("id").substring(i-1, i);

            Integer strInt;

            try{
                strInt = Integer.parseInt(str);
                idString += strInt;
            }
            catch (Exception e){
                isKanino = false;
            }

            /*if(strInt != null){
                idString += strInt;
            }
            else{
                isKanino = false;
            }*/
        }

        try{
            id = Integer.parseInt(idString);
        }
        catch (NumberFormatException e)
        {
            isKanino = false;
        }

        if(isKanino){

            KaninoServices service = RestService.getInstance().getService();
            service.getProductById(id).enqueue(new Callback<ProductResponse>() {
                @Override
                public void onResponse(Response<ProductResponse> response, Retrofit retrofit) {
                    fLProgress.setVisibility(View.GONE);

                    if(response.code() == 200){
                        fLContainer.setVisibility(View.VISIBLE);

                        name = response.body().getNomeProduto();
                        category = response.body().getNomeCategoria();
                        idProduct = response.body().getIdProduto();
                        price = response.body().getPrecProduto() - response.body().getDescontoPromocao();
                        photo = response.body().getStringImagem();


                        DecimalFormat currencyType = new DecimalFormat("0.00");
                        String price = currencyType.format(response.body().getPrecProduto());
                        String name = response.body().getNomeProduto();
                        String pWithDiscount = currencyType.format(response.body().getPrecProduto() - response.body().getDescontoPromocao());

                        Bitmap bmp = response.body().getImagem();

                        txtName.setText(name);
                        txtCategory.setText(category);
                        productImage.setImageBitmap(bmp);
                        txtPrice.setText("De R$ " + price);
                        txtPriceWithDiscount.setText("Por R$ " + pWithDiscount);
                        txtProductsTotalPrice.setText("R$ " + pWithDiscount);
                        txtProductDescription.setText(response.body().getDescProduto());

                        if(price.equals(pWithDiscount))
                        {
                            txtPrice.setVisibility(View.GONE);
                        }
                    }
                    else if(response.code() == 404){
                        fLNotFound.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    fLProgress.setVisibility(View.GONE);
                    fLSlowConnection.setVisibility(View.VISIBLE);
                }
            });
        }
        else{
            fLProgress.setVisibility(View.GONE);
            fLEnganado.setVisibility(View.VISIBLE);
        }

        TextView txtMore = (TextView) findViewById(R.id.txtMore);
        txtMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScaleTxtMore);
                animScaleTxtMore.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (control != controlAux) {

                            controlAux = control;
                            amount = Integer.parseInt(txtAmount.getText().toString());

                            if (amount < 5) {
                                amount++;

                                txtAmount.setText(Integer.toString(amount));

                                progressPrice.setVisibility(View.VISIBLE);
                                txtProductsTotalPrice.setVisibility(View.GONE);

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        SystemClock.sleep(300);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                control++;

                                                DecimalFormat currencyType = new DecimalFormat("0.00");
                                                String pWithDiscount = currencyType.format(amount * price);

                                                txtProductsTotalPrice.setText("R$ " + pWithDiscount);

                                                progressPrice.setVisibility(View.GONE);
                                                txtProductsTotalPrice.setVisibility(View.VISIBLE);
                                            }
                                        });
                                    }
                                }).start();
                            } else {
                                new SweetAlertDialog(ProductDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText("A quantidade máxima é 5.")
                                        .show();
                                control++;
                            }
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });

        TextView txtLess = (TextView) findViewById(R.id.txtLess);
        txtLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScaleTxtLess);
                animScaleTxtLess.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (control != controlAux) {

                            controlAux = control;
                            amount = Integer.parseInt(txtAmount.getText().toString());

                            if (amount > 1) {
                                amount--;

                                txtAmount.setText(Integer.toString(amount));

                                progressPrice.setVisibility(View.VISIBLE);
                                txtProductsTotalPrice.setVisibility(View.GONE);

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        SystemClock.sleep(300);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                control++;

                                                DecimalFormat currencyType = new DecimalFormat("0.00");
                                                String pWithDiscount = currencyType.format(amount * price);

                                                txtProductsTotalPrice.setText("R$ " + pWithDiscount);

                                                progressPrice.setVisibility(View.GONE);
                                                txtProductsTotalPrice.setVisibility(View.VISIBLE);
                                            }
                                        });
                                    }
                                }).start();
                            } else {
                                new SweetAlertDialog(ProductDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Oops...")
                                        .setContentText("A quantidade não pode ser 0.")
                                        .show();
                                control++;
                            }
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });

        Button btnAdicionar = (Button) findViewById(R.id.btnAdicionar);
        btnAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                animScale.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Cart cart = new Cart(ProductDetailActivity.this);

                        int id = cart.getItem(idProduct).getId();

                        amount = amount != 0 ? amount : Integer.parseInt(txtAmount.getText().toString());

                        ItemCart item = new ItemCart();
                        item.setId(idProduct);
                        item.setName(name);
                        item.setPrice(price);
                        item.setAmount(amount);
                        item.setImage(photo);
                        if (id != idProduct) {

                            cart.insert(item);

                            new SweetAlertDialog(ProductDetailActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Pronto!")
                                    .setContentText("Produto adicionado ao carrinho.")
                                    .showCancelButton(true)
                                    .setCancelText("Meu carrinho")
                                    .setConfirmText("Mais produtos")
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            Intent i = new Intent(ProductDetailActivity.this, CartActivity.class);
                                            startActivity(i);
                                        }
                                    })
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            Intent i = new Intent(ProductDetailActivity.this, MainActivity.class);
                                            startActivity(i);
                                        }
                                    })
                                    .show();
                        } else {
                            new SweetAlertDialog(ProductDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText("Este produto já está no carrinho.")
                                    .show();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
