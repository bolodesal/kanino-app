package br.com.bolodesal.kanino.services;

import java.util.ArrayList;
import java.util.List;

import br.com.bolodesal.kanino.domain.Product;
import br.com.bolodesal.kanino.services.entity.AddressRequest;
import br.com.bolodesal.kanino.services.entity.AddressResponse;
import br.com.bolodesal.kanino.services.entity.Category;
import br.com.bolodesal.kanino.services.entity.CustomerRequest;
import br.com.bolodesal.kanino.services.entity.CustomerResponse;
import br.com.bolodesal.kanino.services.entity.ItemCart;
import br.com.bolodesal.kanino.services.entity.LoginRequest;
import br.com.bolodesal.kanino.services.entity.OrderDetailsResponse;
import br.com.bolodesal.kanino.services.entity.OrderRequest;
import br.com.bolodesal.kanino.services.entity.ProductResponse;
import br.com.bolodesal.kanino.services.entity.SendOrderRequest;
import br.com.bolodesal.kanino.services.models.AuthResponse;
import br.com.bolodesal.kanino.services.models.CustomerAddressRequest;
import br.com.bolodesal.kanino.services.models.GlobalResponse;
import br.com.bolodesal.kanino.services.models.ProductsPaginated;
import br.com.bolodesal.kanino.services.models.Error;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by thiago.fsilva8 on 22/10/2015.
 */
public interface KaninoServices {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    //@POST("/api/customers/3")
    //Call<LoginResponse> login (@Body LoginRequest request);

    /*@GET("/kanino/api/customers/{idCliente}")
    Call<CustomerResponse> getCustomer (@Path("idCliente") Integer idCliente);

    @GET("/kanino/api/products/{idProduto}")
    Call<ProductResponse> getProductById (@Path("idProduto") int idProduto);

    @GET("/kanino/api/categories")
    Call<CategoryResponse> getCategories ();*/

    @GET("/kanino/api/customers/{idCliente}")
    Call<CustomerResponse> getCustomer (@Path("idCliente") Integer idCliente);

    @GET("/kanino/api/orders/customer/{id}")
    Call<List<OrderRequest>> getOrdersByCustomerID (@Path("id") int id);

    @GET("/kanino/api/orders/details/{id}")
    Call<List<OrderDetailsResponse>> getOrderDetails (@Path("id") int id);

    @GET("/kanino/api/customers/hasByCPF/{cpf}")
    Call<GlobalResponse> hasCustomerWithCPF(@Path("cpf") String cpf);

    @GET("/kanino/api/customers/hasByEmail/{email}/{idUser}")
    Call<GlobalResponse> hasCustomerWithEmail(@Path("email") String email, @Path("idUser") int idUser);

    @GET("/kanino/api/products/{idProduto}")
    Call<ProductResponse> getProductById (@Path("idProduto") int idProduto);

    @GET("/kanino/api/products/pagination/{page}/{amount}")
    Call<ProductsPaginated> getProducts (@Path("page") int page, @Path("amount") int amount);

    @GET("/kanino/api/products/category/{categoryId}/{pageNumber}/{itensPerPage}")
    Call<ProductsPaginated> getProductsByCategory (@Path("categoryId") int categoryId, @Path("pageNumber") int pageNumber, @Path("itensPerPage") int itensPerPage);

    @GET("/kanino/api/search/pagination/{pageNumber}/{qtdItens}/")
    Call<ProductsPaginated> getProductsBySearch (@Path("pageNumber") int pageNumber, @Path("qtdItens") int qtdItens, @Query("search") String search);

    @GET("/kanino/api/categories")
    Call<List<Category>> getCategories ();

    @GET("/kanino/api/addresses/customer/{customerId}")
    Call<List<AddressResponse>> getAddresses (@Path("customerId") int customerId);

    @GET("/kanino/api/addresses/{addressId}")
    Call<AddressResponse> getAddressDetail (@Path("addressId") int addressId);

    @POST("/kanino/api/customers/address")
    Call<GlobalResponse> newCustomerAddress (@Body CustomerAddressRequest customerAddressRequest);

    @POST("/kanino/api/customers/")
    Call<CustomerResponse> newCustomer (@Body CustomerRequest customerRequest);

    @POST("/kanino/api/login")
    Call<GlobalResponse<AuthResponse>> logon (@Body LoginRequest loginRequest);

    @POST("/kanino/api/orders")
    Call<SendOrderRequest> sendOrderRequest (@Body SendOrderRequest sendOrderRequest);

    @POST("/kanino/api/addresses/")
    Call<GlobalResponse> newAddress (@Body AddressRequest address);

    @PUT("/kanino/api/addresses/")
    Call<GlobalResponse> updateAddress (@Body AddressRequest address);

    @DELETE("/kanino/api/addresses/{id}")
    Call<GlobalResponse> deleteAddress (@Path("id") int id);

    @PUT("/kanino/api/customers/")
    Call<GlobalResponse> updateCustomer (@Body CustomerRequest customerRequest);
}
