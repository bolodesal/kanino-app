package br.com.bolodesal.kanino.enums;

/**
 * Created by Edgar Maia on 25/11/2015.
 */
public enum OrderStatusEnum {

    ABERTO(1),
    AGUARDANDO_PAGAMENTO(2),
    ENVIADO_TRANSPORTADORA(3),
    ENTREGUE(4),
    CANCELADO(5);

    private int orderStatus;

    OrderStatusEnum(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getOrderStatus(){
        return this.orderStatus;
    }

}
