package br.com.bolodesal.kanino.services.models;

import br.com.bolodesal.kanino.services.entity.Address;
import br.com.bolodesal.kanino.services.entity.CustomerResponse;

/**
 * Created by Edgar Maia on 21/11/2015.
 */
public class CustomerAddressRequest {

    public CustomerResponse cliente;

    public Address endereco;

}
