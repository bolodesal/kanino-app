package br.com.bolodesal.kanino;

import android.app.ProgressDialog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.CustomerResponse;
import br.com.bolodesal.kanino.services.entity.LoginRequest;
import br.com.bolodesal.kanino.services.models.AuthResponse;
import br.com.bolodesal.kanino.services.models.GlobalResponse;
import br.com.bolodesal.kanino.services.models.Error;
import br.com.bolodesal.kanino.settings.IntentSettings;
import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener {

    @Email(sequence = 1, message = "E-mail inválido")
    public EditText login;

    @Password(sequence = 1, min = 8, message = "A senha deve conter no mínimo 8 caracteres")
    @Length(sequence = 2, max = 64, message = "A senha deve conter no máximo 64 caracteres")
    public EditText senha;

    private TextView btnSignup;

    private Button btnLogin;

    private Validator validator;

    private KaninoServices service;
    private LoginRequest loginRequest = new LoginRequest();
    private CustomerResponse customerResponse;
    private int REQUEST_SIGNUP = 0;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Animation animScaleBtnLogin = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.anim_scale);

        /*
        *
        * INITIALIZE COMPONENTS
        *
         */

//        Intent data = getIntent();
//        login.setText(data.getStringExtra("emailCliente"));

        Toolbar mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle("Login");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.textWhite));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        login = (EditText)findViewById(R.id.txtLogin);
        senha = (EditText)findViewById(R.id.txtSenha);
        btnSignup = (TextView) findViewById(R.id.txtCreateAccount);
        btnLogin = (Button)findViewById(R.id.btnLogin);

        service = RestService.getInstance().getService();

        prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            login.setText(extras.getString("emailCliente"));
        }

        /*
        *
        * END INITIALIZE COMPONENTS
        *
         */

        // get our html content
        String htmlAsString = getString(R.string.txt_create_account);
        Spanned htmlAsSpanned = Html.fromHtml(htmlAsString);

        // set the html content on the TextView
        TextView textView = (TextView) findViewById(R.id.txtCreateAccount);
        textView.setText(htmlAsSpanned);

        /*
        *
        * CONFIG VALIDATION
        *
        * */

        validator = new Validator(this);
        validator.setValidationListener(this);

        /*
        *
        * END CONFIG VALIDATION
        *
        * */

        /*
        *
        * EVENTS
        *
        * */

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            intent.putExtra(IntentSettings.KEY_PREFS_USER_LOGGED, true);
            startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScaleBtnLogin);
                animScaleBtnLogin.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        validator.validate();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });

         /*
        *
        * END EVENTS
        *
        * */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                login.setText(data.getStringExtra("emailCliente"));
            }
        }
    }

    /*
    *
    * VALIDATION
    *
    * */

    @Override
    public void onValidationSucceeded() {
        login();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    /*
    *
    * END VALIDATION
    *
    * */

    /*
    *
    * LOGIN
    *
    * */

    private void login(){

        btnLogin.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this, R.style.MaterialDrawerBaseTheme_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Autenticando...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        loginRequest.setEmailCliente(login.getText().toString());
        loginRequest.setSenhaCliente(senha.getText().toString());

        service.logon(loginRequest).enqueue(new Callback<GlobalResponse<AuthResponse>>() {
            @Override
            public void onResponse(Response<GlobalResponse<AuthResponse>> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    GlobalResponse<AuthResponse> r = response.body();
                    CustomerResponse customer = r.getResponse().getCustomer();
                    onLoginSuccess(customer);
                } else {
                    if (response != null && !response.isSuccess() && response.errorBody() != null) {
                        Converter<ResponseBody, GlobalResponse<Error<ArrayList<String>>>> errorConverter = retrofit.responseConverter(new TypeToken<GlobalResponse<Error<ArrayList<String>>>>() {
                        }.getType(), new Annotation[0]);
                        try {
                            GlobalResponse<Error<ArrayList<String>>> r = errorConverter.convert(response.errorBody());

                            String messages = "";

                            for (int i = 0; i < r.getResponse().getErrors().size(); i++) {
                                messages += String.format("%s\n", r.getResponse().getErrors().get(i));
                            }

                            senha.setError(messages);
                            btnLogin.setEnabled(true);

                        } catch (IOException e) {
                            onLoginFailed("Ops! Houve um problema, estamos trabalhando na solução.");
                        }
                    } else {
                        onLoginFailed("Ops! Houve um problema, estamos trabalhando na solução.");
                    }
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                onLoginFailed("Não foi possível estabelecer comunicação com nossos serviços.");
                progressDialog.dismiss();
            }
        });

    }

    private void onLoginSuccess(CustomerResponse customer){
        //prefs = getSharedPreferences("login", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(SharedPreferencesSettings.KEY_PREFS_USER_LOGGED, true);
        editor.putInt(SharedPreferencesSettings.KEY_PREFS_ID_USER, customer.getIdCliente());
        editor.putString(SharedPreferencesSettings.KEY_PREFS_NAME_USER, customer.getNomeCompletoCliente());
        editor.putString(SharedPreferencesSettings.KEY_PREFS_EMAIL_USER, customer.getEmailCliente());
        editor.apply();
        btnLogin.setEnabled(true);
        Intent intent;
        Intent redirect = getIntent();
        if(redirect.getStringExtra("redirectTo") != null)
        {
            if(redirect.getStringExtra("redirectTo").equals("TypePaymentActivity"))
            {
                intent = new Intent(LoginActivity.this, TypePaymentActivity.class);
            }
            else if (redirect.getStringExtra("redirectTo").equals("AddressDeliveryActivity")) {
                intent = new Intent(LoginActivity.this, AddressDeliveryActivity.class);
            }
            else {
                intent = new Intent(LoginActivity.this, MainActivity.class);
            }
        }
        else
        {
            intent = new Intent(LoginActivity.this, MainActivity.class);
        }
        startActivity(intent);
        finish();
    }

    private void onLoginFailed(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        btnLogin.setEnabled(true);
    }

    /*
    *
    * END LOGIN
    *
    * */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
