package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Thiago Ferreira on 29/10/2015.
 */
public class Category {

    @SerializedName("idCategoria")
    @Expose
    private int idCategoria;

    @SerializedName("nomeCategoria")
    @Expose
    private String nomeCategoria;

    @SerializedName("descCategoria")
    @Expose
    private String descCategoria;

    public Category(int idCategoria, String nomeCategoria){
        setIdCategoria(idCategoria);
        setNomeCategoria(nomeCategoria);
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public String getNomeCategoria() {
        return nomeCategoria;
    }

    public String getDescCategoria() {
        return descCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public void setNomeCategoria(String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }

    public void setDescCategoria(String descCategoria) {
        this.descCategoria = descCategoria;
    }
}
