package br.com.bolodesal.kanino.services.models;

import br.com.bolodesal.kanino.services.entity.CustomerResponse;

/**
 * Created by bregis on 23/11/2015.
 */
public class AuthResponse {

    private boolean auth;

    private CustomerResponse customer;

    private String message;

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public CustomerResponse getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerResponse customer) {
        this.customer = customer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
