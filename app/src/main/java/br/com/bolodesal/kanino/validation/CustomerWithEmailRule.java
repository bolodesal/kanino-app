package br.com.bolodesal.kanino.validation;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mobsandgeeks.saripaar.AnnotationRule;

import java.io.IOException;
import java.lang.annotation.Annotation;

import br.com.bolodesal.kanino.app.KaninoApplication;
import br.com.bolodesal.kanino.interfaces.validation.HasCostumerWithEmail;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.models.GlobalResponse;
import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;
import retrofit.Call;
import retrofit.Response;

/**
 * Created by Edgar Maia on 20/11/2015.
 */
public class CustomerWithEmailRule extends AnnotationRule<HasCostumerWithEmail, String> {

    protected CustomerWithEmailRule(HasCostumerWithEmail email) {
        super(email);
    }

    @Override
    public boolean isValid(String email) {
        boolean isValid = false;
        if (email == null || email.isEmpty()) { return true; }

        KaninoServices service = RestService.getInstance().getService();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(KaninoApplication.getAppContext());

        try {
            Response<GlobalResponse> r = service.hasCustomerWithEmail(email, prefs.getInt(SharedPreferencesSettings.KEY_PREFS_ID_USER, 0)).execute();
            if (r.code() != 409) {
                isValid = true;
            }
        } catch (IOException e) {
            return false;
        } catch(Exception e){
            return false;
        }
        return isValid;
    }
}
