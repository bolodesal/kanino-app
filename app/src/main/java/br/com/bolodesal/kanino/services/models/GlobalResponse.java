package br.com.bolodesal.kanino.services.models;

/**
 * Created by Edgar Maia on 20/11/2015.
 */
public class GlobalResponse<T> {

    private int status;
    private T response;

    public GlobalResponse(){}

    public GlobalResponse(int status, T response){
        this.status = status;
        this.response = response;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public T getResponse() {
        return response;
    }

    public void setErrors(T response) {
        this.response = response;
    }

}
