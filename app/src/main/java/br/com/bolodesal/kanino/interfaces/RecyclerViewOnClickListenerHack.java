package br.com.bolodesal.kanino.interfaces;

import android.view.View;

/**
 * Created by Thiago Ferreira on 12/10/2015.
 */
public interface RecyclerViewOnClickListenerHack {
    public void onClickListener(View view, int position);
}
