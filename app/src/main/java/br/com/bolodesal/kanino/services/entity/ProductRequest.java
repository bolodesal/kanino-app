package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Thiago Ferreira on 29/10/2015.
 */
public class ProductRequest {

    @SerializedName("idProduto")
    @Expose
    private int idProduto;

    @SerializedName("idCategoria")
    @Expose
    private int idCategoria;

    public int getProduct() {
        return idProduto;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setProduct(int idProduto) {
        this.idProduto = idProduto;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

}
