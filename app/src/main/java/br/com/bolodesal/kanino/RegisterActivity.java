package br.com.bolodesal.kanino;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Optional;
import com.mobsandgeeks.saripaar.annotation.Order;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Pattern;
import com.mobsandgeeks.saripaar.annotation.Select;
import com.soundcloud.android.crop.Crop;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.bolodesal.kanino.interfaces.validation.HasCostumerWithEmail;
import br.com.bolodesal.kanino.interfaces.validation.HasCustomerWithCPF;
import br.com.bolodesal.kanino.interfaces.validation.isCpf;
import br.com.bolodesal.kanino.interfaces.validation.isDate;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.Address;
import br.com.bolodesal.kanino.services.entity.CustomerResponse;
import br.com.bolodesal.kanino.services.models.*;
import br.com.bolodesal.kanino.services.models.Error;
import br.com.bolodesal.kanino.util.UtilKanino;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.Response;
import retrofit.Retrofit;

public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener {

    private CustomerResponse customer = new CustomerResponse();
    private Address address = new Address();

    @Order(3)
    @Length(sequence = 1, min = 2, max = 100, message = "Nome deve conter entre 2 e 100 caracteres")
    private EditText nomeCompleto;

    @Order(1)
    @Email(sequence = 1, message = "E-mail inválido")
    @HasCostumerWithEmail(sequence = 2)
    private EditText email;

    @Order(5)
    @Password(sequence = 1, min = 8, message = "A senha deve conter no mínimo 8 caracteres")
    @Length(sequence = 2, max = 64, message = "A senha deve conter no máximo 64 caracteres")
    private EditText senha;

    @Order(6)
    @ConfirmPassword(message = "As senhas não conferem")
    private EditText confirmSenha;

    @Order(2)
    @isCpf(sequence = 1)
    @HasCustomerWithCPF(sequence = 2)
    private EditText CPF;

    @Order(4)
    @Pattern(sequence = 1, regex = "^(?:(?:31(\\/)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$", message = "Data de nascimento inválida (dd/mm/aaaa)")
    @isDate(sequence = 2, minDate = "31/12/1899", message = "Verifique sua data de nascimento")
    private EditText dataNasc;

    @Order(7)
    @Pattern(sequence = 1, regex = "^[1-9]{2}(| )[2-9][0-9]{7,8}$", message = "Digite um telefone válido (XX XXXXXXXXX)")
    private EditText celular;

    @Order(8)
    @Length(sequence = 1, min = 1,  max = 50, message = "Nome inválido")
    private EditText nomeEndereco;

    @Order(9)
    @Pattern(sequence = 1, regex = "^[0-9]{2}[0-9]{3}(-||\\s)[0-9]{3}$", message = "CEP inválido (xxxxx-xxx ou xxxxxxxx)")
    private EditText CEP;

    @Order(10)
    @Length(sequence = 1, min = 1, max = 100, message = "Logradouro inválido")
    private EditText logradouro;

    @Order(11)
    @Length(sequence = 1, min = 1, max = 10, message = "O número do endereço deve conter entre 1 e 10 caracteres")
    private EditText numeroLogradouro;

    @Order(12)
    @Optional
    @Length(sequence = 1, max = 10, message = "O complemento do endereço deve conter entre 1 e 10 caracteres")
    private EditText complemento;

    @Order(13)
    @Length(sequence = 1, min = 1, max = 50, message = "A cidade deve conter entre 1 e 50 caracteres")
    private EditText cidade;

    /*Order(14)
    @Pattern(sequence = 1, regex = "[A-za-z]{2}", message = "Estado inválido")
    private EditText estado;*/

    @Order(14)
    @Select(message = "Selecione um Estado")
    private Spinner spnEstados;

    private ArrayAdapter<String> arrayAdapterEstados;

    private Button cadastrar;

    private KaninoServices service = RestService.getInstance().getService();
    private Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;
    private Validator validator;
    private FrameLayout flProgress;
    private FrameLayout flProgressCpf;
    private FrameLayout flProgressEmail;
    private FrameLayout flProgressCep;
    private String oldCpfValue = "";
    private String oldEmailValue = "";
    private static int RESULT_LOAD_IMAGE = 1;
    private static int CROP_REQUEST_CODE = 2;
    private int i = 0;
    private String name;


    private ComponentName componentName;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        componentName = getCallingActivity();

        final Animation animScale = AnimationUtils.loadAnimation(this, R.anim.anim_scale);

        /*
        *
        * INITIALIZE COMPONENTS
        *
        * */

        Toolbar mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle("Cadastro");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.textWhite));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cadastrar = (Button) findViewById(R.id.btnCadastrar);
        nomeCompleto = (EditText)findViewById(R.id.txtCadNome);
        email = (EditText)findViewById(R.id.txtCadEmail);
        senha = (EditText)findViewById(R.id.txtCadSenha);
        confirmSenha = (EditText)findViewById(R.id.txtConfirSenha);
        CPF = (EditText)findViewById(R.id.txtCadCPF);
        nomeEndereco = (EditText)findViewById(R.id.txtEnderecoNome);
        CEP = (EditText)findViewById(R.id.txtCEP);
        logradouro = (EditText)findViewById(R.id.txtLogradouro);
        numeroLogradouro = (EditText)findViewById(R.id.txtNumeroLogradouro);
        complemento = (EditText)findViewById(R.id.txtComplementoEndereco);
        //estado = (EditText)findViewById(R.id.txtEstado);
        spnEstados = (Spinner) findViewById(R.id.spnEstados);
        cidade = (EditText)findViewById(R.id.txtCidade);
        dataNasc = (EditText)findViewById(R.id.txtDataNasc);
        celular = (EditText)findViewById(R.id.txtNumCelular);
        service = RestService.getInstance().getService();
        myCalendar = Calendar.getInstance();
        flProgress = (FrameLayout)findViewById(R.id.fLProgress);
        flProgressCpf = (FrameLayout)findViewById(R.id.fLProgressCpf);
        flProgressEmail = (FrameLayout)findViewById(R.id.fLProgressEmail);
        flProgressCep = (FrameLayout)findViewById(R.id.fLProgressCep);

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        dataNasc.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    new DatePickerDialog(RegisterActivity.this, date, myCalendar
                            .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                            myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            }
        });

        spnEstados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!((TextView) parent.getChildAt(0)).getText().toString().equals(getResources().getStringArray(R.array.arrayEstados)[0].toString())) {
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.primary_dark));
                    //UtilKanino.showKeyboard(getCurrentFocus(), getSystemService(Context.INPUT_METHOD_SERVICE));
                    if (numeroLogradouro.getText().toString().isEmpty() && !CEP.getText().toString().isEmpty() && !logradouro.getText().toString().isEmpty()) {
                        numeroLogradouro.requestFocus();
                    } else {
                        cadastrar.requestFocus();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String[] items = getResources().getStringArray(R.array.arrayEstados);
        arrayAdapterEstados = new ArrayAdapter<>(RegisterActivity.this, R.layout.spinner_item, items);
        arrayAdapterEstados.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnEstados.setAdapter(arrayAdapterEstados);

        cidade.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (cidade.hasFocus()) {
                        UtilKanino.hideKeyboard(cidade.getWindowToken(), getSystemService(Context.INPUT_METHOD_SERVICE));
                        v.clearFocus();
                        spnEstados.requestFocus();
                        spnEstados.performClick();
                    }
                }
                return true;
            }
        });

        //request focus first component form
        nomeCompleto.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(nomeCompleto, InputMethodManager.SHOW_IMPLICIT);

        progressDialog = new ProgressDialog(RegisterActivity.this, R.style.MaterialDrawerBaseTheme_Dialog);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);

        /*
        *
        * END INITIALIZE COMPONENTS
        *
        * */

        /*
        *
        * VALIDATION
        *
        * */

        validator = new Validator(this);
        validator.registerAnnotation(isCpf.class);
        validator.registerAnnotation(HasCustomerWithCPF.class);
        validator.registerAnnotation(HasCostumerWithEmail.class);
        validator.registerAnnotation(isDate.class);
        validator.setValidationListener(this);

        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && email.getText().toString().length() > 0 && !oldEmailValue.equals(email.getText().toString())) {
                    flProgressEmail.setVisibility(View.VISIBLE);
                    validator.validateTill(v, true);
                    oldEmailValue = email.getText().toString();
                }
            }
        });


        CPF.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && CPF.getText().toString().length() == 11 && !oldCpfValue.equals(CPF.getText().toString())) {
                    flProgressCpf.setVisibility(View.VISIBLE);
                    validator.validateTill(v, true);
                    oldCpfValue = CPF.getText().toString();
                }
            }
        });

        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                //flProgress.setVisibility(View.VISIBLE);
                validator.validate(true);
                progressDialog.setMessage("Validando dados...");
                progressDialog.show();
            }
        });

        /*
        *
        * END VALIDATION
        *
        * */

        /*
        *
        * GET CEP
        *
        * */

        CEP.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                TextView txtCEP = (TextView) findViewById(R.id.txtCEP);
                if (!hasFocus && txtCEP.getText().toString().length() > 0) {
                    flProgressCep.setVisibility(View.VISIBLE);
                    PostalCodeSearch buscaCEP = new PostalCodeSearch();
                    buscaCEP.execute(txtCEP.getText().toString());
                }
            }
        });

        /*
        *
        * END CEP
        *
        * */

    }

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

        dataNasc.setText(sdf.format(myCalendar.getTime()));
    }

    /*
    *
    * VALIDATION METHODS
    *
    * */

    @Override
    public void onValidationSucceeded() {
        saveCustomer();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        //flProgress.setVisibility(View.GONE);
        progressDialog.dismiss();
        flProgressCpf.setVisibility(View.GONE);
        flProgressEmail.setVisibility(View.GONE);

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

        //Procura todos os EditText e coloca o foco no que estiver com erro
        LinearLayout mainLayoutRegister = (LinearLayout)findViewById(R.id.mainLayoutRegister);
        View view = traverseEditTexts(mainLayoutRegister);
        if(view != null)
        {
            view.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private EditText traverseEditTexts(ViewGroup v){
        EditText invalid = null;
        for (int i = 0; i < v.getChildCount(); i++)
        {
            Object child = v.getChildAt(i);
            if (child instanceof EditText)
            {
                EditText e = (EditText)child;
                if(e.getError() != null)
                {
                    return e;
                }
            }
            else if(child instanceof ViewGroup)
            {
                invalid = traverseEditTexts((ViewGroup)child);  // Recursive call.
                if(invalid != null)
                {
                    break;
                }
            }
        }
        return invalid;
    }

    /*
    *
    * END VALIDATION METHODS
    *
    * */

    /*
    *
    * CEP METHODS
    *
    * */

    public class PostalCodeSearch extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                URL urlCEP = new URL("http://viacep.com.br/ws/" + params[0]  +"/json/");
                HttpURLConnection urlConn = (HttpURLConnection) urlCEP.openConnection();
                InputStream in = urlConn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                StringBuilder strBuilder = new StringBuilder();
                String response;

                while ((response = reader.readLine()) != null)
                    strBuilder.append(response);

                String result = strBuilder.toString();

                return result;

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                flProgressCep.setVisibility(View.GONE);
                TextView logradouro = (TextView)findViewById(R.id.txtLogradouro);
                //TextView estado = (TextView)findViewById(R.id.txtEstado);
                TextView cidade = (TextView)findViewById(R.id.txtCidade);
                String strLogradouro = "", strUf = "", strLocalidade = "";

                if(s != null)
                {
                    JSONObject json = new JSONObject(s);
                    if(!json.has("erro"))
                    {
                        strLogradouro = json.getString("logradouro") != null ? json.getString("logradouro") : "";
                        strUf = json.getString("uf") != null ? json.getString("uf") : "";
                        strLocalidade = json.getString("localidade") != null ? json.getString("localidade") : "";
                        numeroLogradouro.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(numeroLogradouro, InputMethodManager.SHOW_IMPLICIT);
                    }
                }

                logradouro.setText(strLogradouro);
                //estado.setText(strUf);
                spnEstados.setSelection(arrayAdapterEstados.getPosition(strUf));
                cidade.setText(strLocalidade);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
    *
    * END CEP METHODS
    *
    * */

    /*
    *
    * CUSTOMER METHODS
    *
    * */

    public void saveCustomer(){
        progressDialog.setMessage("Cadastrando usuário...");

        //Customer
        customer.setNomeCompletoCliente(nomeCompleto.getText().toString());
        customer.setCpfCliente(CPF.getText().toString());
        customer.setDtNascCliente(dataNasc.getText().toString().replace("/", ""));
        customer.setEmailCliente(email.getText().toString());
        customer.setSenhaCliente(senha.getText().toString());
        customer.setCelularCliente(celular.getText().toString());
        customer.setRecebeNewsletter(false);
        customer.setTelComercialCliente(null);
        customer.setTelResidencialCliente(null);

        //Address
        address.setNomeEndereco(nomeEndereco.getText().toString());
        address.setCepEndereco(CEP.getText().toString());
        address.setLogradouroEndereco(logradouro.getText().toString());
        address.setNumeroEndereco(numeroLogradouro.getText().toString());
        address.setComplementoEndereco(complemento.getText().toString());
        address.setCidadeEndereco(cidade.getText().toString());
        address.setUfEndereco(spnEstados.getSelectedItem().toString());
        address.setPaisEndereco("Brasil");

        CustomerAddressRequest customerAddressRequest = new CustomerAddressRequest();
        customerAddressRequest.cliente = customer;
        customerAddressRequest.endereco = address;

        service.newCustomerAddress(customerAddressRequest).enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Response<GlobalResponse> response, Retrofit retrofit) {
                if (response.code() != 200) {
                    if (response != null && !response.isSuccess() && response.errorBody() != null) {
                        Converter<ResponseBody, GlobalResponse<Error<ArrayList<String>>>> errorConverter = retrofit.responseConverter(new TypeToken<GlobalResponse<Error<ArrayList<String>>>>() {}.getType(), new Annotation[0]);
                        try {
                            GlobalResponse<Error<ArrayList<String>>> r = errorConverter.convert(response.errorBody());

                            String messages = "";

                            for(int i = 0; i < r.getResponse().getErrors().size(); i++){
                                messages += String.format("%s\n", r.getResponse().getErrors().get(i));
                            }

                            progressDialog.dismiss();
                            new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText(messages).show();

                        } catch (IOException e) {
                            progressDialog.dismiss();
                            new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText("Ops! Houve um problema, estamos trabalhando na solução.").show();
                        }
                    }
                } else {
                    //flProgress.setVisibility(View.GONE);
                    progressDialog.dismiss();
                    if(componentName.getShortClassName().equals(LoginActivity.class.getSimpleName())) {
                        Intent data = new Intent();
                        data.putExtra("emailCliente", email.getText().toString());
                        setResult(RESULT_OK, data);
                        finish();
                    }
                    else {
                        Intent data = new Intent(RegisterActivity.this, LoginActivity.class);
                        data.putExtra("emailCliente", email.getText().toString());
                        startActivity(data);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
                new SweetAlertDialog(RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText("Não foi possível estabelecer comunicação com nossos serviços.").show();
            }
        });

    }

    /*
    *
    * END CUSTOMER METHODS
    *
    * */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
