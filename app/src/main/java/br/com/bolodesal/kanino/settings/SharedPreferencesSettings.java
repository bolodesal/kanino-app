package br.com.bolodesal.kanino.settings;

/**
 * Created by Edgar Maia on 22/11/2015.
 */
public class SharedPreferencesSettings {

    public static final String KEY_PREFS_USER_LOGGED = "isAuth";
    public static final String KEY_PREFS_ID_USER = "idUser";
    public static final String KEY_PREFS_NAME_USER = "nameUser";
    public static final String KEY_PREFS_EMAIL_USER = "emailUser";
}
