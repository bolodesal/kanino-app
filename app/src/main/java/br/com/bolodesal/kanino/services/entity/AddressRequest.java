package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Edgar Maia on 27/11/2015.
 */
public class AddressRequest {

    @SerializedName("idEndereco")
    @Expose
    private int idEndereco;
    @SerializedName("idCliente")
    @Expose
    private int idCliente;
    @SerializedName("nomeEndereco")
    @Expose
    private String nomeEndereco;
    @SerializedName("logradouroEndereco")
    @Expose
    private String logradouroEndereco;
    @SerializedName("numeroEndereco")
    @Expose
    private String numeroEndereco;
    @SerializedName("cepEndereco")
    @Expose
    private String cepEndereco;
    @SerializedName("complementoEndereco")
    @Expose
    private String complementoEndereco;
    @SerializedName("cidadeEndereco")
    @Expose
    private String cidadeEndereco;
    @SerializedName("paisEndereco")
    @Expose
    private String paisEndereco;
    @SerializedName("ufEndereco")
    @Expose
    private String ufEndereco;
    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    public void setNomeEndereco(String nomeEndereco) {
        this.nomeEndereco = nomeEndereco;
    }
    public void setLogradouroEndereco(String logradouroEndereco) {
        this.logradouroEndereco = logradouroEndereco;
    }
    public void setNumeroEndereco(String numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }
    public void setCepEndereco(String cepEndereco) {
        this.cepEndereco = cepEndereco;
    }
    public void setComplementoEndereco(String complementoEndereco) {
        this.complementoEndereco = complementoEndereco;
    }
    public void setCidadeEndereco(String cidadeEndereco) {
        this.cidadeEndereco = cidadeEndereco;
    }
    public void setPaisEndereco(String paisEndereco) {
        this.paisEndereco = paisEndereco;
    }
    public void setUfEndereco(String ufEndereco) {
        this.ufEndereco = ufEndereco;
    }
}
