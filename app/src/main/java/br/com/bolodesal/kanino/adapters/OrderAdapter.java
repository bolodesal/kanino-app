package br.com.bolodesal.kanino.adapters;

import android.content.Context;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import br.com.bolodesal.kanino.R;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.entity.OrderRequest;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Thiago Ferreira on 02/11/2015.
 */
public class OrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater mLayoutInflater;
    private List<OrderRequest> mList;
    private List<OrderRequest> items;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private Context mContext;
    private float scale;
    private int width, height, roundPixels;

    public OrderAdapter(Context c, List<OrderRequest> l, List<OrderRequest> it){
        mContext = c;
        mList = l;
        items = it;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        scale = mContext.getResources().getDisplayMetrics().density;
        //width = (mContext.getResources().getDisplayMetrics().widthPixels - (int)(14 * scale + 0.5f)) / 2;
        width = 20;
        height = width;
        roundPixels = (int)(2 * scale + 0.5f);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder mvh;
        if(viewType == 0) {
            View v = mLayoutInflater.inflate(R.layout.item_orders_card, viewGroup, false);
            mvh = new MyViewHolder(v);
        }else {
            View v = mLayoutInflater.inflate(R.layout.progress_item, viewGroup, false);
            mvh = new ProgressViewHolder(v);
        }
        return mvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof ProgressViewHolder){
            ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
        } else {
            if (holder instanceof MyViewHolder && mList.size() > 0 && position < mList.size()) {
                ((MyViewHolder) holder).txtOrderNumber.setText("Pedido: " + Integer.toString(mList.get(position).getIdPedido()));
                ((MyViewHolder) holder).ivOrder.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_pedidos_fundo));
                ((MyViewHolder) holder).txtOrderStatus.setText(mList.get(position).getStatusPedido());
                ((MyViewHolder) holder).txtItemsAmount.setText("Quantidade de itens: " + Integer.toString(mList.get(position).getTotalItens()));

                DecimalFormat currencyType = new DecimalFormat("0.00");
                String pWithDiscount = "R$ " + currencyType.format(mList.get(position).getValorTotal());

                ((MyViewHolder) holder).txtOrderPrice.setText(pWithDiscount);



            }
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(OrderRequest orderRequest, int position){
        mList.add(orderRequest);
        notifyItemInserted(position);
    }

    public int progressBarPosition = -1;

    public void addProgressBar(){
        mList.add(null);
        progressBarPosition = mList.size()-1;
        notifyItemInserted(mList.size());
    }

    public void removeProgressBar(){
        mList.remove(mList.size() - 1);
        progressBarPosition = -1;
        notifyItemRemoved(mList.size() - 1);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == progressBarPosition) {
            return 1; //progress bar
        } else
            return 0; //item normal
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar)v.findViewById(R.id.progressBar1);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivOrder;
        public TextView txtOrderNumber;
        public TextView txtOrderStatus;
        public TextView txtItemsAmount;
        public TextView txtOrderPrice;
        public int amount;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivOrder = (ImageView) itemView.findViewById(R.id.ivOrder);
            txtOrderNumber = (TextView) itemView.findViewById(R.id.txtOrderNumber);
            txtOrderStatus = (TextView) itemView.findViewById(R.id.txtOrderStatus);
            txtItemsAmount = (TextView) itemView.findViewById(R.id.txtItemsAmount);
            txtOrderPrice = (TextView) itemView.findViewById(R.id.txtOrderPrice);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }
}
