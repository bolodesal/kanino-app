package br.com.bolodesal.kanino;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.List;

import br.com.bolodesal.kanino.adapters.OrderAdapter;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.OrderRequest;
import br.com.bolodesal.kanino.util.UtilKanino;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class OrdersActivity extends AppCompatActivity implements RecyclerViewOnClickListenerHack {

    private FrameLayout fLProgress;
    private FrameLayout fLSlowConnection;
    private FrameLayout fLWithoutConnection;
    private FrameLayout fLEmptyOrders;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager Llm;
    private OrderAdapter adapter;
    private List<OrderRequest> mList;
    private List<OrderRequest> items;
    private KaninoServices service;
    private Toolbar mToolbar;
    private UtilKanino utilKanino;
    private SharedPreferences prefs;
    public static final String KEY_PREFS_ID_USER = "idUser";
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        utilKanino = new UtilKanino();

        fLProgress = (FrameLayout) findViewById(R.id.fLProgress);
        mRecyclerView = (RecyclerView) findViewById(R.id.rvOrdersList);
        fLWithoutConnection = (FrameLayout) findViewById(R.id.fLWithoutConnection);
        fLEmptyOrders = (FrameLayout) findViewById(R.id.fLEmptyOrders);
        fLSlowConnection = (FrameLayout) findViewById(R.id.fLSlowConnection);

        Button btnReloadS = (Button) findViewById(R.id.btnReloadS);
        btnReloadS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fLProgress.setVisibility(View.VISIBLE);
                fLSlowConnection.setVisibility(View.GONE);
                getOrders();
            }
        });

        mRecyclerView.setHasFixedSize(true);

        Llm = new LinearLayoutManager(this);
        Llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(Llm);

        service = RestService.getInstance().getService();
        prefs = PreferenceManager.getDefaultSharedPreferences(OrdersActivity.this);
        id = prefs.getInt(KEY_PREFS_ID_USER, 0);

        if(utilKanino.hasConnected(this)){
            getOrders();
        }
        else{
            loadViewError();
        }


    }

    private void loadViewError(){
        fLWithoutConnection.setVisibility(View.VISIBLE);
        fLProgress.setVisibility(View.GONE);

        Button btnReload = (Button) findViewById(R.id.btnReload);
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!utilKanino.hasConnected(OrdersActivity.this)) {
                    loadViewError();
                } else {
                    fLWithoutConnection.setVisibility(View.GONE);
                    fLProgress.setVisibility(View.VISIBLE);
                    getOrders();
                }
            }
        });
    }


    public void getOrders(){
        service.getOrdersByCustomerID(id).enqueue(new Callback<List<OrderRequest>>() {
            @Override
            public void onResponse(Response<List<OrderRequest>> response, Retrofit retrofit) {

                if (response.code() == 200) {
                    items = response.body();
                    mList = items;

                    adapter = new OrderAdapter(OrdersActivity.this, mList, items);
                    adapter.setRecyclerViewOnClickListenerHack(OrdersActivity.this);
                    mRecyclerView.setAdapter(adapter);

                    if (items.size() == 0) {
                        fLEmptyOrders.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    } else {
                        fLEmptyOrders.setVisibility(View.GONE);
                        mRecyclerView.setVisibility(View.VISIBLE);
                    }

                }
                else{
                    fLSlowConnection.setVisibility(View.VISIBLE);
                }


                fLProgress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Throwable t) {
                fLProgress.setVisibility(View.GONE);
                fLSlowConnection.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClickListener(View view, int position) {
        int id = mList.get(position).getIdPedido();

        Intent intent = new Intent(OrdersActivity.this, OrderDetailsActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}