package br.com.bolodesal.kanino.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by Edgar Maia on 26/11/2015.
 */
public class KaninoApplication extends Application {

    private static Context context;

    public void onCreate(){

        super.onCreate();
        KaninoApplication.context = getApplicationContext();

    }

    public static Context getAppContext(){

        return KaninoApplication.context;

    }

}
