package br.com.bolodesal.kanino.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.bolodesal.kanino.R;
import br.com.bolodesal.kanino.services.entity.Category;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;

/**
 * Created by Thiago Ferreira on 12/10/2015.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private LayoutInflater mLayoutInflater;
    private List<Category> mList;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;

    public CategoryAdapter(Context c, List<Category> l){
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = mLayoutInflater.inflate(R.layout.item_category, viewGroup, false);
        MyViewHolder mvh = new MyViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        //myViewHolder.ivCategory.setImageResource( mList.get(position).getPhoto() );

        int id = mList.get(position).getIdCategoria();

        switch (id){
            case 1:
                myViewHolder.ivCategory.setImageResource(R.drawable.ic_category1);
                break;
            case 2:
                myViewHolder.ivCategory.setImageResource(R.drawable.ic_category2);
                break;
            case 3:
                myViewHolder.ivCategory.setImageResource(R.drawable.ic_category3);
                break;
            case 4:
                myViewHolder.ivCategory.setImageResource(R.drawable.ic_category4);
                break;
            case 5:
                myViewHolder.ivCategory.setImageResource(R.drawable.ic_category5);
                break;
            case 6:
                myViewHolder.ivCategory.setImageResource(R.drawable.ic_category6);
                break;
            default:
                myViewHolder.ivCategory.setImageResource(R.drawable.ic_osso_marrom);
        }

        myViewHolder.tvName.setText( mList.get(position).getNomeCategoria() );
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(Category c, int position){
        mList.add(c);
        notifyItemInserted(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView ivCategory;
        public TextView tvName;


        public MyViewHolder(View itemView) {
            super(itemView);
            ivCategory = (ImageView) itemView.findViewById(R.id.ivCategory);
            tvName = (TextView) itemView.findViewById(R.id.tvCategoryName);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }

}
