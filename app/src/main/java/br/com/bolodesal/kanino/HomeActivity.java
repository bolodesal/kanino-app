package br.com.bolodesal.kanino;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class HomeActivity extends AppCompatActivity {

    public static final String KEY_PREFS_FIRST_LAUNCH = "first_launch";
    public static final String KEY_PREFS_USER_LOGGED = "isAuth";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);
        final Animation animScale = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.anim_scale);

        if(sharedPref.getBoolean(KEY_PREFS_FIRST_LAUNCH, true)){
            setContentView(R.layout.activity_termos);

            final CheckBox checkBox = (CheckBox) findViewById(R.id.cbTermos);
            final Button btnOK = (Button) findViewById(R.id.btnOK);

            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkBox.isChecked()) {
                        btnOK.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        btnOK.setTextColor(getResources().getColor(R.color.textWhite));
                    }
                    else{
                        btnOK.setBackgroundColor(getResources().getColor(R.color.buttomUnselected));
                        btnOK.setTextColor(getResources().getColor(R.color.tabUnselected));
                    }
                }
            });

            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(animScale);
                    if(checkBox.isChecked()) {
                        SharedPreferences.Editor prefEditor = sharedPref.edit();
                        prefEditor.putBoolean(KEY_PREFS_FIRST_LAUNCH, false);
                        prefEditor.apply();

                        Intent intent = new Intent(HomeActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Oops...")
                                .setContentText("Aceite os termos para continuar.")
                                .show();
                    }
                }
            });
        }
        else{
            setContentView(R.layout.activity_home);

            LinearLayout lblBoloDeSal = (LinearLayout) findViewById(R.id.lblBoloDeSal);
            lblBoloDeSal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent it = it = new Intent(Intent.ACTION_VIEW);
                    it.setData(Uri.parse("http://bolodesal.com.br"));
                    startActivity(it);
                }
            });

            Button btnQR = (Button) findViewById(R.id.btnQRCode);
            btnQR.setOnClickListener(new View.OnClickListener() {
                @Override
               public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, LeitorQRActivity.class);
                    startActivity(intent);
                }
            });

            Button btnProdutos = (Button) findViewById(R.id.btnProdutos);
            btnProdutos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });

            Button btnCart = (Button) findViewById(R.id.btnCarrinho);
            btnCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, CartActivity.class);
                    startActivity(intent);
                }
            });

            Button btnAcesse = (Button) findViewById(R.id.btnAcesse);

            if(sharedPref.getInt(SharedPreferencesSettings.KEY_PREFS_ID_USER, 0) != 0)
            {
                btnAcesse.setText("Minha conta");
            }
            else
            {
                btnAcesse.setText("Login");
            }
            btnAcesse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent;

                    if (sharedPref.getBoolean(KEY_PREFS_USER_LOGGED, false))
                        intent = new Intent(HomeActivity.this, AccountActivity.class);
                    else
                        intent = new Intent(HomeActivity.this, LoginActivity.class);

                    startActivity(intent);
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}