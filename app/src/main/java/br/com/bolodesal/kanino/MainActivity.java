package br.com.bolodesal.kanino;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import br.com.bolodesal.kanino.adapters.TabsAdapter;
import br.com.bolodesal.kanino.extras.SlidingTabLayout;
import br.com.bolodesal.kanino.services.Cart;
import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;

public class MainActivity extends AppCompatActivity {

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private Toolbar mToolbar;

    private Drawer.Result navigationDrawerLeft;
    private AccountHeader.Result headerNavigationLeft;
    private int mPositionClicked;
    private PrimaryDrawerItem item;
    private IDrawerItem iDrawerItem;

    private int hot_number = 0;
    private TextView ui_hot = null;

    private Cart cart;

    private SharedPreferences prefs;

    private String nameUser = "Kanino";
    private String emailUser = "kanino@bolodesal.com.br";

    private Bundle savedInstace;

    private ProfileDrawerItem profile;

    public  MainActivity(){}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        savedInstace = savedInstanceState;

        cart = new Cart(getBaseContext());
        
        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle("Kanino");
        mToolbar.setTitleTextColor( getResources().getColor(R.color.textWhite) );
        //mToolbar.setSubtitle("just a subtitle");
        //mToolbar.setLogo(R.drawable.ic_launcher);
        setSupportActionBar(mToolbar);
        prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

        mViewPager = (ViewPager) findViewById(R.id.vp_tabs);
        mViewPager.setAdapter(new TabsAdapter(getSupportFragmentManager(), this));
        mViewPager.setCurrentItem(1);

        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.stl_tabs);
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.colorAccent));
        mSlidingTabLayout.setCustomTabView(R.layout.tab_view, R.id.tv_tab);
        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                navigationDrawerLeft.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        mSlidingTabLayout.setDistributeEvenly(true);
        mSlidingTabLayout.setViewPager(mViewPager);
        //mSlidingTabLayout.setHorizontalFadingEdgeEnabled(true);
        //mSlidingTabLayout.setHorizontalScrollBarEnabled(true);

        //NAVIGATION DRAWER
        createDrawerNavigation();

    }

    private void createDrawerNavigation(){
        profile = new ProfileDrawerItem().withName(nameUser).withEmail(emailUser)
                .withIcon(getResources().getDrawable(R.drawable.ic_logo));

        headerNavigationLeft = new AccountHeader()
                .withActivity(MainActivity.this)
                .withCompactStyle(false)
                .withSavedInstance(savedInstace)
                .withThreeSmallProfileImages(false)
                .withHeaderBackground(R.drawable.dog4)
                .addProfiles(profile)
                .build();

        navigationDrawerLeft = new Drawer()
                .withActivity(MainActivity.this)
                .withToolbar(mToolbar)
                .withAccountHeader(headerNavigationLeft)
                .withDisplayBelowToolbar(false)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.LEFT)
                .withSavedInstance(savedInstace)
                .withSelectedItem(1)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                        MainActivity.this.iDrawerItem = iDrawerItem;

                        String mensagem;
                        Intent intent;

                        //SINCRONIZAÇÃO DO DRAWER COM AS TABS
                        if (i >= 0 && i <= 1) {
                            mViewPager.setCurrentItem(i);
                        }

                        for (int count = 0, tam = navigationDrawerLeft.getDrawerItems().size(); count < tam; count++) {
                            if (count == mPositionClicked &&
                                    ((mPositionClicked >= 0 && mPositionClicked <= 4) || (mPositionClicked >= 6 && mPositionClicked <= 7))) {

                                PrimaryDrawerItem aux = (PrimaryDrawerItem) navigationDrawerLeft.getDrawerItems().get(count);
                                aux.setIcon(getResources().getDrawable(getCorrectDrawerIcon(count, false)));
                                break;
                            }
                        }

                        if ((i >= 0 && i <= 4) || (i >= 6 && i <= 7)) {
                            ((PrimaryDrawerItem) iDrawerItem).setIcon(getResources().getDrawable(getCorrectDrawerIcon(i, true)));
                        }
                        mPositionClicked = i;
                        navigationDrawerLeft.getAdapter().notifyDataSetChanged();

                        switch (i) {
                            case 2:
                                intent = new Intent(MainActivity.this, LeitorQRActivity.class);
                                startActivity(intent);
                                navigationDrawerLeft.setSelection(1);
                                mViewPager.setCurrentItem(1);
                                ((PrimaryDrawerItem) iDrawerItem).setIcon(getResources().getDrawable(getCorrectDrawerIcon(1, true)));

                                item = (PrimaryDrawerItem) navigationDrawerLeft.getDrawerItems().get(2);
                                item.setIcon(getResources().getDrawable(getCorrectDrawerIcon(2, false)));
                                break;
                            case 3:

                                intent = new Intent(MainActivity.this, CartActivity.class);
                                startActivity(intent);

                                navigationDrawerLeft.setSelection(1);
                                mViewPager.setCurrentItem(1);
                                ((PrimaryDrawerItem) iDrawerItem).setIcon(getResources().getDrawable(getCorrectDrawerIcon(1, true)));

                                item = (PrimaryDrawerItem) navigationDrawerLeft.getDrawerItems().get(3);
                                item.setIcon(getResources().getDrawable(getCorrectDrawerIcon(3, false)));
                                break;
                            case 4:
                                if (prefs.getBoolean(SharedPreferencesSettings.KEY_PREFS_USER_LOGGED, false)) {
                                    intent = new Intent(MainActivity.this, OrdersActivity.class);
                                } else {
                                    intent = new Intent(MainActivity.this, LoginActivity.class);
                                }
                                startActivity(intent);
                                navigationDrawerLeft.setSelection(1);
                                mViewPager.setCurrentItem(1);
                                ((PrimaryDrawerItem) iDrawerItem).setIcon(getResources().getDrawable(getCorrectDrawerIcon(1, true)));

                                item = (PrimaryDrawerItem) navigationDrawerLeft.getDrawerItems().get(4);
                                item.setIcon(getResources().getDrawable(getCorrectDrawerIcon(4, false)));

                                break;
                            case 6:
                                if (prefs.getBoolean(SharedPreferencesSettings.KEY_PREFS_USER_LOGGED, false))
                                    intent = new Intent(MainActivity.this, AccountActivity.class);
                                else
                                    intent = new Intent(MainActivity.this, LoginActivity.class);

                                startActivity(intent);
                                navigationDrawerLeft.setSelection(1);
                                mViewPager.setCurrentItem(1);
                                ((PrimaryDrawerItem) iDrawerItem).setIcon(getResources().getDrawable(getCorrectDrawerIcon(1, true)));

                                item = (PrimaryDrawerItem) navigationDrawerLeft.getDrawerItems().get(6);
                                item.setIcon(getResources().getDrawable(getCorrectDrawerIcon(6, false)));

                                break;
                            case 7:
                                /*mensagem = "A activity Sobre o APP ainda não foi criada!" ;
                                Toast.makeText(MainActivity.this, mensagem, Toast.LENGTH_LONG).show();*/

                                intent = new Intent(MainActivity.this, SobreActivity.class);
                                startActivity(intent);
                                navigationDrawerLeft.setSelection(1);
                                mViewPager.setCurrentItem(1);
                                ((PrimaryDrawerItem) iDrawerItem).setIcon(getResources().getDrawable(getCorrectDrawerIcon(1, true)));

                                item = (PrimaryDrawerItem) navigationDrawerLeft.getDrawerItems().get(7);
                                item.setIcon(getResources().getDrawable(getCorrectDrawerIcon(7, false)));
                                break;
                            case 8:
                                if (prefs.getBoolean(SharedPreferencesSettings.KEY_PREFS_USER_LOGGED, false)) {
                                    SharedPreferences.Editor editor = prefs.edit();
                                    editor.remove(SharedPreferencesSettings.KEY_PREFS_USER_LOGGED);
                                    editor.remove(SharedPreferencesSettings.KEY_PREFS_ID_USER);
                                    editor.remove(SharedPreferencesSettings.KEY_PREFS_NAME_USER);
                                    editor.remove(SharedPreferencesSettings.KEY_PREFS_EMAIL_USER);
                                    editor.apply();
                                    updateDrawerOnLogout();
                                    Toast.makeText(MainActivity.this, "Você saiu da sua conta", Toast.LENGTH_LONG).show();
                                }
                                break;

                        }
                        navigationDrawerLeft.getAdapter().notifyDataSetChanged();
                    }
                })
                .build();

        //navigationDrawerLeft.addItem(new SectionDrawerItem().withName("Menu"));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Categorias")
                .withIcon(getResources().getDrawable(R.drawable.ic_categorias_cinza)));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Produtos")
                .withIcon(getResources().getDrawable(R.drawable.ic_osso_marrom)));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("QR Code")
                .withIcon(getResources().getDrawable(R.drawable.ic_qrcode_cinza)));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Carrinho")
                .withIcon(getResources().getDrawable(R.drawable.ic_carrinhodrawer_cinza)));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Pedidos")
                .withIcon(getResources().getDrawable(R.drawable.ic_pedidos_cinza)));
        navigationDrawerLeft.addItem(new DividerDrawerItem());
        String accountItemName = "";
        if (prefs.getBoolean(SharedPreferencesSettings.KEY_PREFS_USER_LOGGED, false))
        {
            accountItemName = "Minha Conta";
        }
        else
        {
            accountItemName = "Entrar";
        }

        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName(accountItemName)
                .withIcon(getResources().getDrawable(R.drawable.ic_user_cinza)));
        navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Sobre")
                .withIcon(getResources().getDrawable(R.drawable.ic_sobre_cinza)));
        if (prefs.getBoolean(SharedPreferencesSettings.KEY_PREFS_USER_LOGGED, false))
        {
            navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Sair")
                    .withIcon(getResources().getDrawable(R.drawable.ic_logout_cinza)));
        }

        //FIM NAVIGATION DRAWER
    }

    private int getCorrectDrawerIcon(int position, boolean isSelected){
        switch (position){
            case 0:
                return (isSelected ? R.drawable.ic_categorias_marrom : R.drawable.ic_categorias_cinza);
            case 1:
                return (isSelected ? R.drawable.ic_osso_marrom : R.drawable.ic_osso_cinza);
            case 2:
                return (isSelected ? R.drawable.ic_qrcode_marrom : R.drawable.ic_qrcode_cinza);
            case 3:
                return (isSelected ? R.drawable.ic_carrinhodrawer_marrom : R.drawable.ic_carrinhodrawer_cinza);
            case 4:
                return (isSelected ? R.drawable.ic_pedidos_marrom : R.drawable.ic_pedidos_cinza);
            case 6:
                return (isSelected ? R.drawable.ic_user_marrom : R.drawable.ic_user_cinza);
            case 7:
                return (isSelected ? R.drawable.ic_sobre_marrom : R.drawable.ic_sobre_cinza);
            case 8:
                return (isSelected ? R.drawable.ic_logout_cinza : R.drawable.ic_logout_cinza);

        }
        return (0);
    }

    /*public List<Product> getProductList(int qtd){
        String[] productNames = new String[]{"Produto A", "Produto B", "Produto C", "Produto D", "Produto E", "Produto F", "Produto G", "Produto H", "Produto I", "Produto J"};
        double[] productPrices = new double[]{10.00, 20.00, 25.50, 30.00, 50.99, 60.00, 70.00, 8.00, 9.00, 10.00};
        int[] productPhotos = new int[]{R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago};
        List<Product> productList = new ArrayList<>();

        for(int i = 0; i < qtd; i++){
            Product product = new Product( productNames[i % productNames.length], productPrices[ i % productPrices.length ], productPhotos[i % productNames.length] );
            productList.add(product);
        }
        return(productList);
    }

    public List<Category> getCategoryList(int qtd){
        String[] categoryNames = new String[]{"Catfegoria A", "Categoria B", "Categoria C", "Categoria D", "Categoria E", "Categoria F", "Categoria G", "Categoria H", "Categoria I", "Categoria J"};
        int[] categoryPhotos = new int[]{R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago, R.drawable.thiago};
        List<Category> categoryList = new ArrayList<>();

        for(int i = 0; i < qtd; i++){
            Category category = new Category( categoryNames[i % categoryNames.length], categoryPhotos[i % categoryNames.length] );
            categoryList.add(category);
        }
        return(categoryList);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;*/

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView;
        MenuItem item = menu.findItem(R.id.action_searchable_activity);

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ){
            searchView = (SearchView) item.getActionView();
        }
        else{
            searchView = (SearchView) MenuItemCompat.getActionView(item);
        }

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getResources().getString(R.string.buscar_produtos));


        final View menu_hotlist = menu.findItem(R.id.action_cart).getActionView();
        ui_hot = (TextView) menu_hotlist.findViewById(R.id.hotlist_hot);
        hot_number = cart.getCount();
        updateHotCount(hot_number);
        new MyMenuItemStuffListener(menu_hotlist, "Carrinho") {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CartActivity.class);
                startActivity(intent);
            }
        };
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*public void addHotCount () {
        hot_number +=1;
        updateHotCount(hot_number);
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        hot_number = cart.getCount();
        updateHotCount(hot_number);
        profile.setName(prefs.getString(SharedPreferencesSettings.KEY_PREFS_NAME_USER, nameUser));
        profile.setEmail(prefs.getString(SharedPreferencesSettings.KEY_PREFS_EMAIL_USER, emailUser));
        headerNavigationLeft.updateProfileByIdentifier(profile);
        if(!prefs.getBoolean(SharedPreferencesSettings.KEY_PREFS_USER_LOGGED, false))
        {
            updateDrawerOnLogout();
        }else{
            navigationDrawerLeft.updateItem(
                    new PrimaryDrawerItem().withName("Minha Conta")
                            .withIcon(getResources().getDrawable(R.drawable.ic_user_cinza)), 6);
            navigationDrawerLeft.removeItem(8);
            navigationDrawerLeft.addItem(new PrimaryDrawerItem().withName("Sair")
                    .withIcon(getResources().getDrawable(R.drawable.ic_logout_cinza)));

        }
        //createDrawerNavigation();
    }

    private void updateDrawerOnLogout(){
        navigationDrawerLeft.removeItem(8);
        profile.setName(nameUser);
        profile.setEmail(emailUser);
        headerNavigationLeft.updateProfileByIdentifier(profile);
        navigationDrawerLeft.updateItem(
                new PrimaryDrawerItem().withName("Entrar")
                        .withIcon(getResources().getDrawable(R.drawable.ic_user_cinza)), 6);
    }

    public void updateHotCount(final int new_hot_number) {
        hot_number = new_hot_number;

        if (ui_hot == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (new_hot_number == 0)
                    ui_hot.setVisibility(View.INVISIBLE);
                else {
                    ui_hot.setVisibility(View.VISIBLE);
                    ui_hot.setText(Integer.toString(new_hot_number));
                }
            }
        });
    }

    abstract class MyMenuItemStuffListener implements View.OnClickListener, View.OnLongClickListener {
        private String hint;
        private View view;

        MyMenuItemStuffListener(View view, String hint) {
            this.view = view;
            this.hint = hint;
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override abstract public void onClick(View v);

        @Override public boolean onLongClick(View v) {
            final int[] screenPos = new int[2];
            final Rect displayFrame = new Rect();
            view.getLocationOnScreen(screenPos);
            view.getWindowVisibleDisplayFrame(displayFrame);
            final Context context = view.getContext();
            final int width = view.getWidth();
            final int height = view.getHeight();
            final int midy = screenPos[1] + height / 2;
            final int screenWidth = getResources().getDisplayMetrics().widthPixels;
            Toast cheatSheet = Toast.makeText(context, hint, Toast.LENGTH_SHORT);
            if (midy < displayFrame.height()) {
                cheatSheet.setGravity(Gravity.TOP | Gravity.RIGHT,
                        screenWidth - screenPos[0] - width / 2, height);
            } else {
                cheatSheet.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, height);
            }
            cheatSheet.show();
            return true;
        }
    }
}
