package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerRequest {

    @SerializedName("idCliente")
    @Expose
    private int idCliente;

    @SerializedName("nomeCompletoCliente")
    @Expose
    private String nomeCompletoCliente;

    @SerializedName("emailCliente")
    @Expose
    private String emailCliente;

    @SerializedName("senhaCliente")
    @Expose
    private String senhaCliente;

    @SerializedName("cpfCliente")
    @Expose
    private String cpfCliente;

    @SerializedName("celularCliente")
    @Expose
    private String celularCliente;

    @SerializedName("telComercialCliente")
    @Expose
    private String telComercialCliente;

    @SerializedName("telResidencialCliente")
    @Expose
    private String telResidencialCliente;

    @SerializedName("dtNascCliente")
    @Expose
    private String dtNascCliente;

    @SerializedName("recebeNewsletter")

    @Expose
    private boolean recebeNewsletter;

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public void setSenhaCliente(String senhaCliente) {
        this.senhaCliente = senhaCliente;
    }

    public void setNomeCompleto(String nomeCompletoCliente) {
        this.nomeCompletoCliente = nomeCompletoCliente;
    }

    public void seteMail(String emailCliente) {
        this.emailCliente = emailCliente;
    }

    public void setCpf(String cpfCliente) {
        this.cpfCliente = cpfCliente;
    }

    public void setCelular(String celularCliente) {
        this.celularCliente = celularCliente;
    }

    public void setTelComercial(String telComercialCliente) {
        this.telComercialCliente = telComercialCliente;
    }

    public void setTelResidencial(String telResidencialCliente) {
        this.telResidencialCliente = telResidencialCliente;
    }

    public void setDtNasc(String dtNascCliente) {
        this.dtNascCliente = dtNascCliente;
    }

    public void setRecebeNewsletter(boolean recebeNewsletter) {
        this.recebeNewsletter = recebeNewsletter;
    }

}
