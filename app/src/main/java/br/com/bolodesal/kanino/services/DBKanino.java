package br.com.bolodesal.kanino.services;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Thiago Ferreira on 02/11/2015.
 */
public class DBKanino extends SQLiteOpenHelper {
    private static final String NOME_BD = "kanino";
    private static final int VERSAO_BD = 1;


    public DBKanino(Context ctx){
        super(ctx, NOME_BD, null, VERSAO_BD);
    }


    @Override
    public void onCreate(SQLiteDatabase bd) {
        bd.execSQL("create table itemCart(id int primary key, name text not null, amount int not null, price real not null, image blob not null);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase bd, int arg1, int arg2) {
        bd.execSQL("drop table itemCart;");
        onCreate(bd);
    }
}
