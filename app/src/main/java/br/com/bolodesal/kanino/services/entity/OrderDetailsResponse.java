package br.com.bolodesal.kanino.services.entity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Thiago Ferreira on 26/11/2015.
 */
public class OrderDetailsResponse {

    @SerializedName("idPedido")
    @Expose
    private int idPedido;

    @SerializedName("idProduto")
    @Expose
    private int idProduto;

    @SerializedName("nomeProduto")
    @Expose
    private String nomeProduto;

    @SerializedName("precoVendaItem")
    @Expose
    private double precoVendaItem;

    @SerializedName("qtdProduto")
    @Expose
    private int qtdProduto;

    @SerializedName("imagem")
    @Expose
    private String imagem;

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public void setPrecoVendaItem(double precoVendaItem) {
        this.precoVendaItem = precoVendaItem;
    }

    public void setQtdProduto(int qtdProduto) {
        this.qtdProduto = qtdProduto;
    }

    public void setImagemProduto(String imagem) {
        this.imagem = imagem;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public int getIdProduto() {
        return idProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public double getPrecoVendaItem() {
        return precoVendaItem;
    }

    public int getQtdProduto() {
        return qtdProduto;
    }

    public Bitmap getImagem() {

        byte [] encodeByte = Base64.decode(imagem, Base64.DEFAULT);
        Bitmap bmp = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);

        return bmp;
    }
}
