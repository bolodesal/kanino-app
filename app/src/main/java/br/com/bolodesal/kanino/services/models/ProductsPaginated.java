package br.com.bolodesal.kanino.services.models;

import java.util.List;

import br.com.bolodesal.kanino.services.entity.ProductResponse;

/**
 * Created by Thiago Ferreira on 06/11/2015.
 */
public class ProductsPaginated {

    private int totalProdutos;
    private int paginaAtual;
    private int ultimaPagina;
    private List<ProductResponse> produtos;

    public void setTotalProdutos(int totalProdutos) {
        this.totalProdutos = totalProdutos;
    }

    public void setPaginaAtual(int paginaAtual) {
        this.paginaAtual = paginaAtual;
    }

    public void setUltimaPagina(int ultimaPagina) {
        this.ultimaPagina = ultimaPagina;
    }

    public void setProdutos(List<ProductResponse> produtos) {
        this.produtos = produtos;
    }

    public int getTotalProdutos() {
        return totalProdutos;
    }

    public int getPaginaAtual() {
        return paginaAtual;
    }

    public int getUltimaPagina() {
        return ultimaPagina;
    }

    public List<ProductResponse> getProdutos() {
        return produtos;
    }
}
