package br.com.bolodesal.kanino;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import br.com.bolodesal.kanino.adapters.ItemCartAdapter;
import br.com.bolodesal.kanino.adapters.OrderDetailsAdapter;
import br.com.bolodesal.kanino.domain.Product;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.ItemCart;
import br.com.bolodesal.kanino.services.entity.OrderDetailsResponse;
import br.com.bolodesal.kanino.util.UtilKanino;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class OrderDetailsActivity extends AppCompatActivity implements RecyclerViewOnClickListenerHack {

    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private List<OrderDetailsResponse> mList;
    private List<OrderDetailsResponse> items;
    private int pLeft, pRight, pTop, pBottom;
    private int controle = 1, aux = 0;
    private int control, controlAux;
    private Toolbar mToolbar;
    private TextView txtTotalPrice;
    private TextView txtTotalItems;
    private OrderDetailsAdapter adapter;
    private KaninoServices service;
    private UtilKanino utilKanino;
    private FrameLayout fLWithoutConnection;
    private FrameLayout fLProgress;
    private FrameLayout fLSlowConnection;
    private int id;
    private float scale;
    private Animation animScale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        animScale = AnimationUtils.loadAnimation(OrderDetailsActivity.this, R.anim.anim_scale);

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        utilKanino = new UtilKanino();

        Intent intent = getIntent();
        id = intent.getIntExtra("id", 1);

        scale = getResources().getDisplayMetrics().density;
        pLeft = (int) (4 + scale + 0.5f);
        pRight = (int) (4 + scale + 0.5f);

        txtTotalPrice = (TextView) findViewById(R.id.txtTotalPrice);
        txtTotalItems = (TextView) findViewById(R.id.txtTotalItems);
        fLWithoutConnection = (FrameLayout) findViewById(R.id.fLWithoutConnection);
        fLProgress = (FrameLayout) findViewById(R.id.fLProgress);
        fLSlowConnection = (FrameLayout) findViewById(R.id.fLSlowConnection);

        Button btnReloadS = (Button) findViewById(R.id.btnReloadS);
        btnReloadS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                fLProgress.setVisibility(View.VISIBLE);
                fLSlowConnection.setVisibility(View.GONE);
                getProducts();
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.rvOrderDetailsList);
        mRecyclerView.setHasFixedSize(true);

        final LinearLayoutManager Llm = new LinearLayoutManager(this);
        Llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(Llm);

        service = RestService.getInstance().getService();

        if(utilKanino.hasConnected(this)){
            getProducts();
        }
        else{
            loadViewError();
        }

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (Llm.findFirstCompletelyVisibleItemPosition() == 0) {
                    pTop = (int) (4 + scale + 0.5f);
                    mRecyclerView.setPadding(pLeft, pTop, pRight, 0);
                } else {
                    pTop = 0;
                    mRecyclerView.setPadding(pLeft, pTop, pRight, 0);
                }

                if (mList.size() == Llm.findLastCompletelyVisibleItemPosition() + 1) {

                    pBottom = (int) (4 + scale + 0.5f);
                    mRecyclerView.setPadding(pLeft, pTop, pRight, pBottom);
                }
                else{
                    pBottom = 0;
                    mRecyclerView.setPadding(pLeft, pTop, pRight, pBottom);
                }
            }
        });
    }

    private void loadViewError(){
        fLWithoutConnection.setVisibility(View.VISIBLE);
        fLProgress.setVisibility(View.GONE);

        Button btnReload = (Button) findViewById(R.id.btnReload);
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                if (!utilKanino.hasConnected(OrderDetailsActivity.this)) {
                    loadViewError();
                } else {
                    fLWithoutConnection.setVisibility(View.GONE);
                    fLProgress.setVisibility(View.VISIBLE);
                    getProducts();
                }
            }
        });
    }

    public void getProducts(){
        service.getOrderDetails(id).enqueue(new Callback<List<OrderDetailsResponse>>() {
            @Override
            public void onResponse(Response<List<OrderDetailsResponse>> response, Retrofit retrofit) {

                items = response.body();
                mList = items;

                /*for(int i = 0; i < items.size(); i++){
                    Product product = new Product();
                    product.setId(items.get(i).getIdProduto());
                    product.setName(items.get(i).getNomeProduto());
                    product.setPrice(items.get(i).getPrecoVendaItem());
                    product.setTotalPrice(items.get(i).getPrecoVendaItem());
                    product.setAmount(items.get(i).getQtdProduto());
                    //product.setPhoto(items.get(i).getImagem());
                    mList.add(product);
                }*/

                adapter = new OrderDetailsAdapter(OrderDetailsActivity.this, mList, items);

                adapter.setRecyclerViewOnClickListenerHack(OrderDetailsActivity.this);

                mRecyclerView.setAdapter(adapter);
                fLProgress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Throwable t) {

                fLProgress.setVisibility(View.GONE);
                fLSlowConnection.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickListener(View view, int position) {

    }
}
