package br.com.bolodesal.kanino.services;

import com.squareup.okhttp.OkHttpClient;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by thiago.fsilva8 on 22/10/2015.
 */
public class RestService {
    private static RestService ourInstance = new RestService();

    public static RestService getInstance() {
        return ourInstance;
    }

    private KaninoServices service;

    private RestService() {
        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new LoggingInterception());

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://tsitomcat.azurewebsites.net")
                //.baseUrl("http://10.0.3.2:8080")
                .client(client)

                .build();

        service = retrofit.create(KaninoServices.class);

    }

    public KaninoServices getService ( ) {
        return service;
    }
}
