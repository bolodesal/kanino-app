package br.com.bolodesal.kanino.interfaces.validation;

import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.com.bolodesal.kanino.validation.CpfRule;

/**
 * Created by Edgar Maia on 19/11/2015.
 */
@ValidateUsing(CpfRule.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface isCpf {
    int messageResId()   default -1;
    String message()     default "CPF inválido";
    int sequence()       default -1;
}