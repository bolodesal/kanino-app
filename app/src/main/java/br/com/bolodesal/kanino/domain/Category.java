package br.com.bolodesal.kanino.domain;

/**
 * Created by Thiago Ferreira on 12/10/2015.
 */
public class Category {
    private String name;
    private int photo;

    public Category(){}
    public Category(String name, int photo){
        setName(name);
        setPhoto(photo);
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPhoto(int photo){
        this.photo = photo;
    }

    public String getName(){
        return this.name;
    }

    public int getPhoto(){
        return this.photo;
    }
}
