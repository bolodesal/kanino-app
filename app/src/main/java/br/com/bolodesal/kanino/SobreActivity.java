package br.com.bolodesal.kanino;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class SobreActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private float scale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        LinearLayout lblBoloDeSal = (LinearLayout) findViewById(R.id.lblBoloDeSal);
        lblBoloDeSal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = it = new Intent(Intent.ACTION_VIEW);
                it.setData(Uri.parse("http://bolodesal.com.br"));
                startActivity(it);
            }
        });

        scale = getResources().getDisplayMetrics().density;

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle("Sobre");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.textWhite));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bitmap bmAndre = BitmapFactory.decodeResource(getResources(), R.drawable.andre);
        Bitmap bmBruno = BitmapFactory.decodeResource(getResources(), R.drawable.bruno);
        Bitmap bmEdgar = BitmapFactory.decodeResource(getResources(), R.drawable.edgar);
        Bitmap bmJacques = BitmapFactory.decodeResource(getResources(), R.drawable.jacques);
        Bitmap bmThiago = BitmapFactory.decodeResource(getResources(), R.drawable.thiago);

        ImageView ivAndre = (ImageView) findViewById(R.id.ivAndre);
        ivAndre.setImageBitmap(getRoundedCornerBitmap(bmAndre, scale));

        ImageView ivBruno = (ImageView) findViewById(R.id.ivBruno);
        ivBruno.setImageBitmap(getRoundedCornerBitmap(bmBruno, scale));

        ImageView ivEdgar = (ImageView) findViewById(R.id.ivEdgar);
        ivEdgar.setImageBitmap(getRoundedCornerBitmap(bmEdgar, scale));

        ImageView ivJacques = (ImageView) findViewById(R.id.ivJacques);
        ivJacques.setImageBitmap(getRoundedCornerBitmap(bmJacques, scale));

        ImageView ivThiago = (ImageView) findViewById(R.id.ivThiago);
        ivThiago.setImageBitmap(getRoundedCornerBitmap(bmThiago, scale));

    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float scale) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = bitmap.getWidth();

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
