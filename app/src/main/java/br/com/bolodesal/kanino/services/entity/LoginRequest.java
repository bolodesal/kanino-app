
package br.com.bolodesal.kanino.services.entity;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class LoginRequest {

    @SerializedName("emailCliente")
    @Expose
    private String emailCliente;

    @SerializedName("senhaCliente")
    @Expose
    private String senhaCliente;

    /**
     * 
     * @param emailCliente
     *     The emailCliente
     */
    public void setEmailCliente(String emailCliente) {
        this.emailCliente = emailCliente;
    }

    /**
     * 
     * @param senhaCliente
     *     The senhaCliente
     */
    public void setSenhaCliente(String senhaCliente) {
        this.senhaCliente = senhaCliente;
    }

}
