package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Edgar Maia on 21/11/2015.
 */
public class Address {
    @SerializedName("idEndereco")
    @Expose
    private int idEndereco;
    @SerializedName("idCliente")
    @Expose
    private int idCliente;
    @SerializedName("nomeEndereco")
    @Expose
    private String nomeEndereco;
    @SerializedName("logradouroEndereco")
    @Expose
    private String logradouroEndereco;
    @SerializedName("numeroEndereco")
    @Expose
    private String numeroEndereco;
    @SerializedName("cepEndereco")
    @Expose
    private String cepEndereco;
    @SerializedName("complementoEndereco")
    @Expose
    private String complementoEndereco;
    @SerializedName("cidadeEndereco")
    @Expose
    private String cidadeEndereco;
    @SerializedName("paisEndereco")
    @Expose
    private String paisEndereco;
    @SerializedName("ufEndereco")
    @Expose
    private String ufEndereco;
    public int getIdEndereco() {
        return idEndereco;
    }
    public void setIdEndereco(int idEndereco) {
        this.idEndereco = idEndereco;
    }
    public int getIdCliente() {
        return idCliente;
    }
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    public String getNomeEndereco() {
        return nomeEndereco;
    }
    public void setNomeEndereco(String nomeEndereco) {
        this.nomeEndereco = nomeEndereco;
    }
    public String getLogradouroEndereco() {
        return logradouroEndereco;
    }
    public void setLogradouroEndereco(String logradouroEndereco) {
        this.logradouroEndereco = logradouroEndereco;
    }
    public String getNumeroEndereco() {
        return numeroEndereco;
    }
    public void setNumeroEndereco(String numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }
    public String getCepEndereco() {
        return cepEndereco;
    }
    public void setCepEndereco(String cepEndereco) {
        this.cepEndereco = cepEndereco;
    }
    public String getComplementoEndereco() {
        return complementoEndereco;
    }
    public void setComplementoEndereco(String complementoEndereco) {
        this.complementoEndereco = complementoEndereco;
    }
    public String getCidadeEndereco() {
        return cidadeEndereco;
    }
    public void setCidadeEndereco(String cidadeEndereco) {
        this.cidadeEndereco = cidadeEndereco;
    }
    public String getPaisEndereco() {
        return paisEndereco;
    }
    public void setPaisEndereco(String paisEndereco) {
        this.paisEndereco = paisEndereco;
    }
    public String getUfEndereco() {
        return ufEndereco;
    }
    public void setUfEndereco(String ufEndereco) {
        this.ufEndereco = ufEndereco;
    }
}