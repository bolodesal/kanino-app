package br.com.bolodesal.kanino.services.entity;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import br.com.bolodesal.kanino.domain.Category;

public class CategoryResponse {

    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories = new ArrayList<>();

    public ArrayList<Category> getCategories() {
        return categories;
    }
}
