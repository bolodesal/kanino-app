package br.com.bolodesal.kanino;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bolodesal.kanino.adapters.ItemCartAdapter;
import br.com.bolodesal.kanino.domain.Product;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.Cart;
import br.com.bolodesal.kanino.services.entity.ItemCart;
import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class CartActivity extends AppCompatActivity implements RecyclerViewOnClickListenerHack {

    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private List<Product> mList;
    private List<ItemCart> items;
    private float scale;
    private int pLeft, pRight, pTop, pBottom;
    private int controle = 1, aux = 0;
    private int control, controlAux;
    private Toolbar mToolbar;
    private String title = "Carrinho";
    private int size;
    private Cart cart;
    private ProgressBar progressTotalPrice;
    private ProgressBar progressTotalItems;
    private TextView txtTotalPrice;
    private TextView txtTotalItems;
    private double totalPrice = 0;
    private int totalAmount = 0;
    private CardView cVBtnBuy;
    private MenuItem delete;
    private FrameLayout fLEmptyCart;
    private ItemCartAdapter adapter;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        progressTotalPrice = (ProgressBar) findViewById(R.id.progressTotalPrice);
        progressTotalItems = (ProgressBar) findViewById(R.id.progressTotalItems);
        txtTotalPrice = (TextView) findViewById(R.id.txtTotalPrice);
        txtTotalItems = (TextView) findViewById(R.id.txtTotalItems);
        cVBtnBuy = (CardView) findViewById(R.id.cVBtnBuy);
        fLEmptyCart = (FrameLayout) findViewById(R.id.fLEmptyCart);

        final Animation animScale = AnimationUtils.loadAnimation(CartActivity.this, R.anim.anim_scale);

        scale = getResources().getDisplayMetrics().density;
        pLeft = (int) (4 + scale + 0.5f);
        pRight = (int) (4 + scale + 0.5f);

        cart = new Cart(this);

        prefs = PreferenceManager.getDefaultSharedPreferences(CartActivity.this);

        int qtd = cart.getCount();

        if (qtd == 1) {
            title += " - " + qtd + " produto";
        } else if (qtd > 1) {
            title += " - " + qtd + " produtos";
        }

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle(title);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.textWhite));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        items = cart.getAllItems();


        for (int i = 0; i < items.size(); i++) {
            totalPrice += items.get(i).getPrice() * items.get(i).getAmount();
            totalAmount += items.get(i).getAmount();
        }

        DecimalFormat currencyType = new DecimalFormat("0.00");
        String price = currencyType.format(totalPrice);

        txtTotalPrice.setText("R$ " + price);
        txtTotalItems.setText(Integer.toString(totalAmount));

        mList = new ArrayList<Product>();

        if (items.size() >= 10) {
            for (int i = 0; i < 10; i++) {
                Product product = new Product();
                product.setId(items.get(i).getId());
                product.setName(items.get(i).getName());
                product.setPrice(items.get(i).getPrice());
                product.setTotalPrice(items.get(i).getPrice());
                product.setPhoto(items.get(i).getImage());
                product.setAmount(items.get(i).getAmount());
                mList.add(product);
            }
            control = 10;
        } else {
            for (int i = 0; i < items.size(); i++) {
                Product product = new Product();
                product.setId(items.get(i).getId());
                product.setName(items.get(i).getName());
                product.setPrice(items.get(i).getPrice());
                product.setTotalPrice(items.get(i).getPrice());
                product.setPhoto(items.get(i).getImage());
                product.setAmount(items.get(i).getAmount());
                mList.add(product);
            }
        }

        size = mList.size();

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);

        mRecyclerView = (RecyclerView) findViewById(R.id.rvCartList);
        mRecyclerView.setHasFixedSize(true);

        final LinearLayoutManager Llm = new LinearLayoutManager(this);
        Llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(Llm);

        adapter = new ItemCartAdapter(CartActivity.this, mList, items);

        adapter.setRecyclerViewOnClickListenerHack(this);

        final FrameLayout fLProgress = (FrameLayout) findViewById(R.id.fLProgress);

        if (items.size() != 0) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    SystemClock.sleep(1000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mRecyclerView.setAdapter(adapter);
                            fLProgress.setVisibility(View.GONE);
                            cVBtnBuy.setVisibility(View.VISIBLE);
                            delete.setEnabled(true);
                        }
                    });
                }
            }).start();
        } else {
            fLProgress.setVisibility(View.GONE);
            cVBtnBuy.setVisibility(View.GONE);
            fLEmptyCart.setVisibility(View.VISIBLE);
        }

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (Llm.findFirstCompletelyVisibleItemPosition() == 0) {
                    pTop = (int) (4 + scale + 0.5f);
                    mRecyclerView.setPadding(pLeft, pTop, pRight, 0);
                } else {
                    pTop = 0;
                    mRecyclerView.setPadding(pLeft, pTop, pRight, 0);
                }

                if (mList.size() == Llm.findLastCompletelyVisibleItemPosition() + 1 && mList.size() < items.size()) {

                    if (controle != aux) {
                        aux = controle;

                        adapter.addProgressBar();
                        mRecyclerView.scrollToPosition(mList.size() - 1);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                SystemClock.sleep(1000);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        adapter.removeProgressBar();

                                        final List<Product> listAux = null;

                                        if (items.size() - control >= 10) {
                                            controlAux = control + 10;

                                            for (int i = control; i < controlAux; i++) {
                                                Product product = new Product();
                                                product.setId(items.get(i).getId());
                                                product.setName(items.get(i).getName());
                                                product.setPrice(items.get(i).getPrice());
                                                product.setTotalPrice(items.get(i).getPrice());
                                                product.setPhoto(items.get(i).getImage());
                                                product.setAmount(items.get(i).getAmount());
                                                //listAux.add(product);
                                                //adapter.addListItem(listAux.get(i), mList.size());
                                                mList.add(product);
                                                adapter.notifyItemInserted(mList.size());
                                            }
                                            controle++;
                                            control += 10;
                                        } else if (items.size() - control >= 1 && items.size() - control < 10) {
                                            controlAux = items.size() - control;

                                            for (int i = control; i < items.size(); i++) {
                                                Product product = new Product();
                                                product.setId(items.get(i).getId());
                                                product.setName(items.get(i).getName());
                                                product.setPrice(items.get(i).getPrice());
                                                product.setTotalPrice(items.get(i).getPrice());
                                                product.setPhoto(items.get(i).getImage());
                                                product.setAmount(items.get(i).getAmount());
                                                //listAux.add(product);
                                                //adapter.addListItem(listAux.get(i), mList.size());
                                                mList.add(product);
                                                adapter.notifyItemInserted(mList.size());
                                            }
                                        }

                                        size = mList.size();

                                        /*for (int i = 0; i < listAux.size(); i++) {
                                            adapter.addListItem(listAux.get(i), mList.size());
                                        }*/
                                    }
                                });
                            }
                        }).start();
                    }
                } else if (size == Llm.findLastCompletelyVisibleItemPosition() + 1) {
                    pBottom = (int) (4 + scale + 0.5f);
                    mRecyclerView.setPadding(pLeft, pTop, pRight, pBottom);
                }
            }
        });

        Button btnBuy = (Button) findViewById(R.id.btnBuy);
        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                animScale.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Intent intent;

                        if (prefs.getBoolean(SharedPreferencesSettings.KEY_PREFS_USER_LOGGED, false)) {
                            intent = new Intent(CartActivity.this, AddressDeliveryActivity.class);
                        } else {
                            intent = new Intent(CartActivity.this, LoginActivity.class);
                            intent.putExtra("redirectTo", "AddressDeliveryActivity"); //tela de endereços
                        }

                        startActivity(intent);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });
    }

    public void setNewSize(int size) {
        this.size = size;

        if (size > 1) {
            title = "Carrinho - " + size + " produtos";
        } else {
            title = "Carrinho - " + size + " produto";
        }

        mToolbar.setTitle(title);

        items.clear();
        items = cart.getAllItems();
    }

    public int getTotalItems() {
        return totalAmount;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPriceAndAmount(double totalPrice, final int totalAmount) {
        this.totalPrice = totalPrice;
        this.totalAmount = totalAmount;

        txtTotalPrice.setVisibility(View.GONE);
        progressTotalPrice.setVisibility(View.VISIBLE);
        txtTotalItems.setVisibility(View.GONE);
        progressTotalItems.setVisibility(View.VISIBLE);

        DecimalFormat currencyType = new DecimalFormat("0.00");
        final String price = currencyType.format(totalPrice);

        new Thread(new Runnable() {
            @Override
            public void run() {
                SystemClock.sleep(300);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtTotalItems.setText(Integer.toString(totalAmount));
                        txtTotalPrice.setText("R$ " + price);

                        txtTotalPrice.setVisibility(View.VISIBLE);
                        progressTotalPrice.setVisibility(View.GONE);
                        txtTotalItems.setVisibility(View.VISIBLE);
                        progressTotalItems.setVisibility(View.GONE);
                    }
                });
            }
        }).start();
    }

    public void resetActitivyIsEmpytCart(){
        mToolbar.setTitle("Carrinho");
        cVBtnBuy.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        fLEmptyCart.setVisibility(View.VISIBLE);
        Intent i = new Intent(CartActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        delete = menu.getItem(0);
        if(items.size() != 0){
            delete.setEnabled(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {

            if (items.size() != 0) {

                new SweetAlertDialog(CartActivity.this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Deseja continuar?")
                        .setContentText("Confirme se deseja limpar o carrinho.")
                        .setCancelText("Não, cancelar!")
                        .setConfirmText("Sim, limpar!")
                        .showCancelButton(true)
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog
                                        .setTitleText("Pronto!")
                                        .setContentText("Carrinho limpo.")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(null)
                                        .showCancelButton(false)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                for (int i = 0; i < items.size(); i++) {
                                    ItemCart itemCart = new ItemCart();
                                    itemCart.setId(items.get(i).getId());
                                    cart.delete(itemCart);
                                }

                                adapter.notifyDataSetChanged();
                                //resetActitivyIsEmpytCart();
                                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        finish();
                                    }
                                });
                            }
                        }).show();
            } else {
                new SweetAlertDialog(CartActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("Não há produtos para serem removidos.")
                        .show();
            }

            return true;
        }

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickListener(View view, int position) {
        int id = mList.get(position).getId();
        Intent intent = new Intent(CartActivity.this, ProductDetailActivity.class);
        intent.putExtra("id", "k" + Integer.toString(id));
        startActivity(intent);
    }
}
