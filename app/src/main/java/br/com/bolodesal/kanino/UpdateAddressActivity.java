package br.com.bolodesal.kanino;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import br.com.bolodesal.kanino.adapters.AddressAdapter;
import br.com.bolodesal.kanino.adapters.OrderAdapter;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.AddressResponse;
import br.com.bolodesal.kanino.services.models.*;
import br.com.bolodesal.kanino.services.models.Error;
import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.Response;
import retrofit.Retrofit;

public class UpdateAddressActivity extends AppCompatActivity implements RecyclerViewOnClickListenerHack {

    private Toolbar mToolbar;
    private FrameLayout fLEmptyOrders;
    private FrameLayout fLProgress;
    private LinearLayout container;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager Llm;
    private AddressAdapter adapter;
    private List<AddressResponse> mList;
    private List<AddressResponse> items;
    private KaninoServices service;
    private SharedPreferences prefs;
    private int idUser;
    private FrameLayout fLEmptyAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_address);

        final Animation animScale = AnimationUtils.loadAnimation(UpdateAddressActivity.this, R.anim.anim_scale);

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle("Meus endereços");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.textWhite));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fLProgress = (FrameLayout) findViewById(R.id.fLProgress);
        mRecyclerView = (RecyclerView) findViewById(R.id.rvAddressList);
        container = (LinearLayout) findViewById(R.id.container);
        fLEmptyAddress = (FrameLayout) findViewById(R.id.fLEmptyAddress);

        mRecyclerView.setHasFixedSize(true);

        Llm = new LinearLayoutManager(this);
        Llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(Llm);

        service = RestService.getInstance().getService();
        prefs = PreferenceManager.getDefaultSharedPreferences(UpdateAddressActivity.this);
        idUser = prefs.getInt(SharedPreferencesSettings.KEY_PREFS_ID_USER, 0);

        Button btnAddAddress = (Button) findViewById(R.id.btnAddAddress);
        btnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                animScale.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        Intent intent = new Intent(UpdateAddressActivity.this, RegisterAddressActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fLProgress.setVisibility(View.VISIBLE);
        getAddress();
    }

    @Override
    public void onClickListener(View view, int position) {
        int id = mList.get(position).getIdEndereco();

        Intent intent = new Intent(UpdateAddressActivity.this, AddressDetailActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    private void getAddress(){
        service.getAddresses(idUser).enqueue(new Callback<List<AddressResponse>>() {
            @Override
            public void onResponse(Response<List<AddressResponse>> response, Retrofit retrofit) {
                container.setVisibility(View.VISIBLE);
                fLProgress.setVisibility(View.GONE);

                if (response.code() == 200) {
                    mList = response.body();
                    resetActitivyIsNotEmpytAddress();
                    adapter = new AddressAdapter(UpdateAddressActivity.this, mList);
                    adapter.setRecyclerViewOnClickListenerHack(UpdateAddressActivity.this);
                    mRecyclerView.setAdapter(adapter);
                } else {
                    if (response != null && !response.isSuccess() && response.errorBody() != null) {
                        Converter<ResponseBody, GlobalResponse<Error<ArrayList<String>>>> errorConverter = retrofit.responseConverter(new TypeToken<GlobalResponse<Error<ArrayList<String>>>>() {
                        }.getType(), new Annotation[0]);
                        try {
                            GlobalResponse<Error<ArrayList<String>>> r = errorConverter.convert(response.errorBody());

                            String messages = "";

                            for (int i = 0; i < r.getResponse().getErrors().size(); i++) {
                                messages += String.format("%s\n", r.getResponse().getErrors().get(i));
                            }

                            new SweetAlertDialog(UpdateAddressActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText(messages).show();

                            resetActitivyIsEmpytAddress();

                        } catch (IOException e) {

                            new SweetAlertDialog(UpdateAddressActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText("Ops! Houve um problema, estamos trabalhando na solução.").show();
                        }
                    }
                }


            }

            @Override
            public void onFailure(Throwable t) {
                new SweetAlertDialog(UpdateAddressActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText("Não foi possível estabelecer comunicação com nossos serviços.").show();
            }
        });
    }

    public void resetActitivyIsNotEmpytAddress(){
        fLEmptyAddress.setVisibility(View.GONE);
    }

    public void resetActitivyIsEmpytAddress(){
        fLEmptyAddress.setVisibility(View.VISIBLE);
    }
}
