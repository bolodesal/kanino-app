package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerResponse {

    @SerializedName("celularCliente")
    @Expose
    private String celularCliente;
    @SerializedName("cpfCliente")
    @Expose
    private String cpfCliente;
    @SerializedName("dtNascCliente")
    @Expose
    private String dtNascCliente;
    @SerializedName("emailCliente")
    @Expose
    private String emailCliente;
    @SerializedName("idCliente")
    @Expose
    private Integer idCliente;
    @SerializedName("nomeCompletoCliente")
    @Expose
    private String nomeCompletoCliente;
    @SerializedName("recebeNewsletter")
    @Expose
    private Boolean recebeNewsletter;
    @SerializedName("senhaCliente")
    @Expose
    private String senhaCliente;
    @SerializedName("telComercialCliente")
    @Expose
    private String telComercialCliente;
    @SerializedName("telResidencialCliente")
    @Expose
    private String telResidencialCliente;

    /**
     *
     * @return
     * The celularCliente
     */
    public String getCelularCliente() {
        return celularCliente;
    }

    /**
     *
     * @param celularCliente
     * The celularCliente
     */
    public void setCelularCliente(String celularCliente) {
        this.celularCliente = celularCliente;
    }

    /**
     *
     * @return
     * The cpfCliente
     */
    public String getCpfCliente() {
        return cpfCliente;
    }

    /**
     *
     * @param cpfCliente
     * The cpfCliente
     */
    public void setCpfCliente(String cpfCliente) {
        this.cpfCliente = cpfCliente;
    }

    /**
     *
     * @return
     * The dtNascCliente
     */
    public String getDtNascCliente() {
        return dtNascCliente;
    }

    /**
     *
     * @param dtNascCliente
     * The dtNascCliente
     */
    public void setDtNascCliente(String dtNascCliente) {
        this.dtNascCliente = dtNascCliente;
    }

    /**
     *
     * @return
     * The emailCliente
     */
    public String getEmailCliente() {
        return emailCliente;
    }

    /**
     *
     * @param emailCliente
     * The emailCliente
     */
    public void setEmailCliente(String emailCliente) {
        this.emailCliente = emailCliente;
    }

    /**
     *
     * @return
     * The idCliente
     */
    public Integer getIdCliente() {
        return idCliente;
    }

    /**
     *
     * @param idCliente
     * The idCliente
     */
    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    /**
     *
     * @return
     * The nomeCompletoCliente
     */
    public String getNomeCompletoCliente() {
        return nomeCompletoCliente;
    }

    /**
     *
     * @param nomeCompletoCliente
     * The nomeCompletoCliente
     */
    public void setNomeCompletoCliente(String nomeCompletoCliente) {
        this.nomeCompletoCliente = nomeCompletoCliente;
    }

    /**
     *
     * @return
     * The recebeNewsletter
     */
    public Boolean getRecebeNewsletter() {
        return recebeNewsletter;
    }

    /**
     *
     * @param recebeNewsletter
     * The recebeNewsletter
     */
    public void setRecebeNewsletter(Boolean recebeNewsletter) {
        this.recebeNewsletter = recebeNewsletter;
    }

    /**
     *
     * @return
     * The senhaCliente
     */
    public String getSenhaCliente() {
        return senhaCliente;
    }

    /**
     *
     * @param senhaCliente
     * The senhaCliente
     */
    public void setSenhaCliente(String senhaCliente) {
        this.senhaCliente = senhaCliente;
    }

    /**
     *
     * @return
     * The telComercialCliente
     */
    public String getTelComercialCliente() {
        return telComercialCliente;
    }

    /**
     *
     * @param telComercialCliente
     * The telComercialCliente
     */
    public void setTelComercialCliente(String telComercialCliente) {
        this.telComercialCliente = telComercialCliente;
    }

    /**
     *
     * @return
     * The telResidencialCliente
     */
    public String getTelResidencialCliente() {
        return telResidencialCliente;
    }

    /**
     *
     * @param telResidencialCliente
     * The telResidencialCliente
     */
    public void setTelResidencialCliente(String telResidencialCliente) {
        this.telResidencialCliente = telResidencialCliente;
    }

}
