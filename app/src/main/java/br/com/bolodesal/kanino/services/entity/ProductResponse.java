package br.com.bolodesal.kanino.services.entity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.bolodesal.kanino.MainActivity;
import br.com.bolodesal.kanino.extras.ImageHelper;
import br.com.bolodesal.kanino.fragments.ProductFragment;

public class ProductResponse {

    @SerializedName("idProduto")
    @Expose
    private int idProduto;
    @SerializedName("nomeProduto")
    @Expose
    private String nomeProduto;
    @SerializedName("descProduto")
    @Expose
    private String descProduto;
    @SerializedName("imagem")
    @Expose
    private String imagem;
    @SerializedName("precProduto")
    @Expose
    private Double precProduto;
    @SerializedName("descontoPromocao")
    @Expose
    private Double descontoPromocao;
    @SerializedName("idCategoria")
    @Expose
    private Integer idCategoria;
    @SerializedName("ativoProduto")
    @Expose
    private char ativoProduto;
    @SerializedName("idUsuario")
    @Expose
    private Integer idUsuario;
    @SerializedName("qtdMinEstoque")
    @Expose
    private Integer qtdMinEstoque;

    @SerializedName("nomeCategoria")
    @Expose
    private String nomeCategoria;

    private int totalProdutos;

    public void setTotalProdutos(int totalProdutos) {
        this.totalProdutos = totalProdutos;
    }

    public int getTotalProdutos() {
        return totalProdutos;
    }

    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    /**
     *
     * @return
     * The nomeProduto
     */
    public String getNomeProduto() {
        return nomeProduto;
    }

    /**
     *
     * @param nomeProduto
     * The nomeProduto
     */
    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    /**
     *
     * @return
     * The descProduto
     */
    public String getDescProduto() {
        return descProduto;
    }

    /**
     *
     * @param descProduto
     * The descProduto
     */
    public void setDescProduto(String descProduto) {
        this.descProduto = descProduto;
    }

    /**
     *
     * @return
     * The imagem
     */
    public Bitmap getImagem() {

        byte [] encodeByte = Base64.decode(imagem, Base64.DEFAULT);
        Bitmap bmp = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);

        return bmp;
    }

    public byte[] getStringImagem(){
        byte [] encodeByte = Base64.decode(imagem, Base64.DEFAULT);
        return encodeByte;
    }

    /**
     *
     * @param imagem
     * The imagem
     */
    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    /**
     *
     * @return
     * The precProduto
     */
    public Double getPrecProduto() {
        return precProduto;
    }

    /**
     *
     * @param precProduto
     * The precProduto
     */
    public void setPrecProduto(Double precProduto) {
        this.precProduto = precProduto;
    }

    /**
     *
     * @return
     * The descontoPromocao
     */
    public Double getDescontoPromocao() {
        return descontoPromocao;
    }

    /**
     *
     * @param descontoPromocao
     * The descontoPromocao
     */
    public void setDescontoPromocao(Double descontoPromocao) {
        this.descontoPromocao = descontoPromocao;
    }

    /**
     *
     * @return
     * The idCategoria
     */
    public Integer getIdCategoria() {
        return idCategoria;
    }

    /**
     *
     * @param idCategoria
     * The idCategoria
     */
    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    /**
     *
     * @return
     * The ativoProduto
     */
    public char getAtivoProduto() {
        return ativoProduto;
    }

    /**
     *
     * @param ativoProduto
     * The ativoProduto
     */
    public void setAtivoProduto(char ativoProduto) {
        this.ativoProduto = ativoProduto;
    }

    /**
     *
     * @return
     * The idUsuario
     */
    public Integer getIdUsuario() {
        return idUsuario;
    }

    /**
     *
     * @param idUsuario
     * The idUsuario
     */
    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     *
     * @return
     * The qtdMinEstoque
     */
    public Integer getQtdMinEstoque() {
        return qtdMinEstoque;
    }

    /**
     *
     * @param qtdMinEstoque
     * The qtdMinEstoque
     */
    public void setQtdMinEstoque(Integer qtdMinEstoque) {
        this.qtdMinEstoque = qtdMinEstoque;
    }

    public String getNomeCategoria() {
        return nomeCategoria;
    }

    public void setNomeCategoria(String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }
}
