package br.com.bolodesal.kanino.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by Thiago Ferreira on 01/11/2015.
 */
public class UtilKanino {

    public boolean hasConnected(Context classe) {
        ConnectivityManager gerenciador = (ConnectivityManager) classe.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo informacao = gerenciador.getActiveNetworkInfo();
        if ((informacao != null) && (informacao.isConnectedOrConnecting()) && (informacao.isAvailable())) {
            return true;
        }
        return false;
    }

    public static void hideKeyboard(IBinder token, Object getSystemService) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService;
        inputManager.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void showKeyboard(View v,  Object getSystemService) {
        InputMethodManager imm = (InputMethodManager) getSystemService;
        if (v != null)
            imm.showSoftInput(v, 0);
    }

    public static EditText traverseEditTexts(ViewGroup v){
        EditText invalid = null;
        for (int i = 0; i < v.getChildCount(); i++)
        {
            Object child = v.getChildAt(i);
            if (child instanceof EditText)
            {
                EditText e = (EditText)child;
                if(e.getError() != null)
                {
                    return e;
                }
            }
            else if(child instanceof ViewGroup)
            {
                invalid = traverseEditTexts((ViewGroup)child);  // Recursive call.
                if(invalid != null)
                {
                    break;
                }
            }
        }
        return invalid;
    }

}
