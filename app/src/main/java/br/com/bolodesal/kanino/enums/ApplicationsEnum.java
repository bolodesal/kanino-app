package br.com.bolodesal.kanino.enums;

/**
 * Created by Edgar Maia on 25/11/2015.
 */
public enum ApplicationsEnum {

    KANNO_MOBILE(2);

    private int application;

    ApplicationsEnum(int application) {
        this.application = application;
    }

    public int getApplication(){
        return this.application;
    }

}
