package br.com.bolodesal.kanino.domain;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("name")
    @Expose
    private int id;
    private String name;
    private double price;
    private String totalPrice;
    private Bitmap photo;
    private int amount;

    public Product(){}
    public Product(String name, double price, Bitmap photo){
        setName(name);
        setPrice(price);
        setTotalPrice(price);
        setPhoto(photo);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPrice(double price){
        this.price =  price;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = "R$ " + totalPrice;
    }

    public void setPhoto(Bitmap photo){
        this.photo = photo;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public String getName(){
        return this.name;
    }

    public double getPrice(){
        return this.price;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public Bitmap getPhoto(){
        return photo;
    }

    public int getAmount() {
        return amount;
    }
}
