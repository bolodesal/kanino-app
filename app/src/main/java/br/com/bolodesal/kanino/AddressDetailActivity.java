package br.com.bolodesal.kanino;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Optional;
import com.mobsandgeeks.saripaar.annotation.Order;
import com.mobsandgeeks.saripaar.annotation.Pattern;
import com.mobsandgeeks.saripaar.annotation.Select;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import br.com.bolodesal.kanino.interfaces.validation.HasCustomerWithCPF;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.AddressRequest;
import br.com.bolodesal.kanino.services.entity.AddressResponse;
import br.com.bolodesal.kanino.services.models.*;
import br.com.bolodesal.kanino.services.models.Error;
import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;
import br.com.bolodesal.kanino.util.UtilKanino;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.Response;
import retrofit.Retrofit;

public class AddressDetailActivity extends AppCompatActivity implements Validator.ValidationListener {

    private AddressRequest address = new AddressRequest();
    private AddressResponse addressResponse = new AddressResponse();

    @Order(2)
    @Length(sequence = 1, min = 1,  max = 50, message = "Nome inválido")
    private EditText nomeEndereco;

    @Order(1)
    @Pattern(sequence = 1, regex = "^[0-9]{2}[0-9]{3}(-||\\s)[0-9]{3}$", message = "CEP inválido (xxxxx-xxx ou xxxxxxxx)")
    private EditText CEP;

    @Order(3)
    @Length(sequence = 1, min = 1, max = 100, message = "Logradouro inválido")
    private EditText logradouro;

    @Order(4)
    @Length(sequence = 1, min = 1, max = 10, message = "O número do endereço deve conter entre 1 e 10 caracteres")
    private EditText numeroLogradouro;

    @Order(5)
    @Optional
    @Length(sequence = 1, max = 10, message = "O complemento do endereço deve conter entre 1 e 10 caracteres")
    private EditText complemento;

    @Order(6)
    @Length(sequence = 1, min = 1, max = 50, message = "A cidade deve conter entre 1 e 50 caracteres")
    private EditText cidade;

    @Order(7)
    @Select(message = "Selecione um Estado")
    private Spinner spnEstados;

    private ArrayAdapter<String> arrayAdapterEstados;

    private Button btnAlterar;

    private KaninoServices service = RestService.getInstance().getService();

    private FrameLayout flProgressCep;
    private FrameLayout flProgress;

    private Validator validator;

    private ComponentName componentName;

    private ProgressDialog progressDialog;

    private SharedPreferences prefs;

    private Toolbar mToolbar;

    private int addressID;

    private LinearLayout container;

    private FrameLayout fLSlowConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_detail);

        final Animation animScale = AnimationUtils.loadAnimation(AddressDetailActivity.this, R.anim.anim_scale);

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle("Dados de Endereço");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.textWhite));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        componentName = getCallingActivity();

        Intent intent = getIntent();
        addressID = intent.getIntExtra("id", 1);

        /*
        *
        * INITIALIZE COMPONENTS
        *
        * */
        nomeEndereco = (EditText)findViewById(R.id.txtEnderecoNome);
        CEP = (EditText)findViewById(R.id.txtCEP);
        logradouro = (EditText)findViewById(R.id.txtLogradouro);
        numeroLogradouro = (EditText)findViewById(R.id.txtNumeroLogradouro);
        complemento = (EditText)findViewById(R.id.txtComplementoEndereco);
        spnEstados = (Spinner)findViewById(R.id.spnEstados);
        spnEstados.setEnabled(false);
        cidade = (EditText)findViewById(R.id.txtCidade);
        btnAlterar = (Button)findViewById(R.id.btnAlterarEnd);
        service = RestService.getInstance().getService();
        flProgress = (FrameLayout)findViewById(R.id.fLProgress);
        flProgressCep = (FrameLayout)findViewById(R.id.fLProgressCep);

        fLSlowConnection = (FrameLayout) findViewById(R.id.fLSlowConnection);

        Button btnReloadS = (Button) findViewById(R.id.btnReloadS);
        btnReloadS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                flProgress.setVisibility(View.VISIBLE);
                fLSlowConnection.setVisibility(View.GONE);
                getAddressDetail();
            }
        });

        nomeEndereco.clearFocus();
        CEP.clearFocus();
        logradouro.clearFocus();
        numeroLogradouro.clearFocus();
        complemento.clearFocus();
        spnEstados.clearFocus();
        cidade.clearFocus();

        container = (LinearLayout) findViewById(R.id.container);

        prefs = PreferenceManager.getDefaultSharedPreferences(AddressDetailActivity.this);

        spnEstados.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!((TextView) parent.getChildAt(0)).getText().toString().equals(getResources().getStringArray(R.array.arrayEstados)[0].toString())) {
                    if(view.isEnabled())
                    {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.primary_dark));
                    }else
                    {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_text));
                    }
                    if (numeroLogradouro.getText().toString().isEmpty() && !CEP.getText().toString().isEmpty() && !logradouro.getText().toString().isEmpty()) {
                        numeroLogradouro.requestFocus();
                    }
                }
                else
                {
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_text));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String[] items = getResources().getStringArray(R.array.arrayEstados);
        arrayAdapterEstados = new ArrayAdapter<>(AddressDetailActivity.this, R.layout.spinner_item, items);
        arrayAdapterEstados.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnEstados.setAdapter(arrayAdapterEstados);

        cidade.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (cidade.hasFocus()) {
                        UtilKanino.hideKeyboard(cidade.getWindowToken(), getSystemService(Context.INPUT_METHOD_SERVICE));
                        v.clearFocus();
                        spnEstados.requestFocus();
                        spnEstados.performClick();
                    }
                }
                return true;
            }
        });

        progressDialog = new ProgressDialog(AddressDetailActivity.this, R.style.MaterialDrawerBaseTheme_Dialog);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);

        getAddressDetail();

        /*
        *
        * END INITIALIZE COMPONENTS
        *
        * */

        /*
        *
        * VALIDATION
        *
        * */

        validator = new Validator(this);
        validator.registerAnnotation(HasCustomerWithCPF.class);
        validator.setValidationListener(this);

        btnAlterar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                animScale.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (btnAlterar.getText().toString().equals(getResources().getString(R.string.txtBtnAlterarEnd))) {
                            btnAlterar.setText(getResources().getString(R.string.txtBtnSalvar));
                            nomeEndereco.setEnabled(true);
                            CEP.setEnabled(true);
                            logradouro.setEnabled(true);
                            numeroLogradouro.setEnabled(true);
                            complemento.setEnabled(true);
                            spnEstados.setEnabled(true);
                            ((TextView) spnEstados.getChildAt(0)).setTextColor(getResources().getColor(R.color.primary_dark));
                            cidade.setEnabled(true);
                        } else {
                            ((TextView) spnEstados.getChildAt(0)).setTextColor(getResources().getColor(R.color.hint_text));
                            progressDialog.setMessage("Validando dados...");
                            progressDialog.show();
                            validator.validate(true);
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });

        CEP.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View v, boolean hasFocus) {
                TextView txtCEP = (TextView) findViewById(R.id.txtCEP);
                if (!hasFocus && txtCEP.getText().toString().length() > 0) {
                    flProgressCep.setVisibility(View.VISIBLE);
                    PostalCodeSearch buscaCEP = new PostalCodeSearch();
                    buscaCEP.execute(txtCEP.getText().toString());
                }
            }
        });
    }

    public void getAddressDetail(){
        service.getAddressDetail(addressID).enqueue(new Callback<AddressResponse>() {
            @Override
            public void onResponse(Response<AddressResponse> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    addressResponse = response.body();
                    nomeEndereco.setText(addressResponse.getNomeEndereco());
                    CEP.setText(addressResponse.getCepEndereco().trim());
                    logradouro.setText(addressResponse.getLogradouroEndereco());
                    numeroLogradouro.setText(addressResponse.getNumeroEndereco());
                    complemento.setText(addressResponse.getComplementoEndereco());
                    spnEstados.setSelection(arrayAdapterEstados.getPosition(addressResponse.getUfEndereco()));
                    cidade.setText(addressResponse.getCidadeEndereco());
                    container.setVisibility(View.VISIBLE);
                } else {
                    new SweetAlertDialog(AddressDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops!")
                            .setContentText("Não foi possível recuperar suas informações. Agurade um pouco e tente novamente.").show();
                }

                flProgress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Throwable t) {
                fLSlowConnection.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        updateAddress();
        btnAlterar.setText(getResources().getString(R.string.txtBtnAlterarEnd));
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        progressDialog.dismiss();

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

        //Procura todos os EditText e coloca o foco no que estiver com erro
        LinearLayout mainLayoutUpdateAddress = (LinearLayout) findViewById(R.id.mainLayoutUpdateAddress);
        View view = UtilKanino.traverseEditTexts(mainLayoutUpdateAddress);
        if (view != null) {
            view.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }


    /*
    *
    * CEP METHODS
    *
    * */

    public class PostalCodeSearch extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                URL urlCEP = new URL("http://viacep.com.br/ws/" + params[0]  +"/json/");
                HttpURLConnection urlConn = (HttpURLConnection) urlCEP.openConnection();
                InputStream in = urlConn.getInputStream();

                BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                StringBuilder strBuilder = new StringBuilder();
                String response;

                while ((response = reader.readLine()) != null)
                    strBuilder.append(response);

                String result = strBuilder.toString();

                return result;

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                flProgressCep.setVisibility(View.GONE);
                TextView logradouro = (TextView)findViewById(R.id.txtLogradouro);
                //TextView estado = (TextView)findViewById(R.id.txtEstado);
                TextView cidade = (TextView)findViewById(R.id.txtCidade);
                String strLogradouro = "", strUf = "", strLocalidade = "";

                if(s != null)
                {
                    JSONObject json = new JSONObject(s);
                    if(!json.has("erro"))
                    {
                        strLogradouro = json.getString("logradouro") != null ? json.getString("logradouro") : "";
                        strUf = json.getString("uf") != null ? json.getString("uf") : "";
                        strLocalidade = json.getString("localidade") != null ? json.getString("localidade") : "";
                        numeroLogradouro.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(numeroLogradouro, InputMethodManager.SHOW_IMPLICIT);
                    }
                }

                logradouro.setText(strLogradouro);
                //estado.setText(strUf);
                spnEstados.setSelection(arrayAdapterEstados.getPosition(strUf));
                cidade.setText(strLocalidade);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
    *
    * ADDRESS METHODS
    *
    * */

    private void updateAddress()
    {
        progressDialog.setMessage("Atualizando endereço...");

        //Address
        address.setIdCliente(prefs.getInt(SharedPreferencesSettings.KEY_PREFS_ID_USER, 0));
        address.setIdEndereco(addressID);
        address.setNomeEndereco(nomeEndereco.getText().toString());
        address.setCepEndereco(CEP.getText().toString());
        address.setLogradouroEndereco(logradouro.getText().toString());
        address.setNumeroEndereco(numeroLogradouro.getText().toString());
        address.setComplementoEndereco(complemento.getText().toString());
        address.setCidadeEndereco(cidade.getText().toString());
        address.setUfEndereco(spnEstados.getSelectedItem().toString());
        address.setPaisEndereco("Brasil");

        service.updateAddress(address).enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Response<GlobalResponse> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    progressDialog.dismiss();

                    nomeEndereco.setEnabled(false);
                    nomeEndereco.clearFocus();
                    CEP.setEnabled(false);
                    CEP.clearFocus();
                    logradouro.setEnabled(false);
                    logradouro.clearFocus();
                    numeroLogradouro.setEnabled(false);
                    numeroLogradouro.clearFocus();
                    complemento.setEnabled(false);
                    complemento.clearFocus();
                    spnEstados.setEnabled(false);
                    spnEstados.clearFocus();
                    cidade.setEnabled(false);
                    cidade.clearFocus();

                    new SweetAlertDialog(AddressDetailActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Pronto!")
                            .setContentText("Endereço atualizado com sucesso.")
                            .setConfirmText("OK")
                            .setConfirmClickListener(null)
                            .showCancelButton(false)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    finish();
                                }
                            }).show();
                } else {
                    if (response != null && !response.isSuccess() && response.errorBody() != null) {
                        Converter<ResponseBody, GlobalResponse<Error<ArrayList<String>>>> errorConverter = retrofit.responseConverter(new TypeToken<GlobalResponse<Error<ArrayList<String>>>>() {
                        }.getType(), new Annotation[0]);
                        try {
                            GlobalResponse<Error<ArrayList<String>>> r = errorConverter.convert(response.errorBody());

                            String messages = "";

                            for (int i = 0; i < r.getResponse().getErrors().size(); i++) {
                                messages += String.format("%s\n", r.getResponse().getErrors().get(i));
                            }

                            progressDialog.dismiss();
                            new SweetAlertDialog(AddressDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText(messages).show();

                        } catch (IOException e) {
                            progressDialog.dismiss();
                            new SweetAlertDialog(AddressDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText("Ops! Houve um problema, estamos trabalhando na solução.").show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressDialog.dismiss();
                new SweetAlertDialog(AddressDetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText("Não foi possível estabelecer comunicação com nossos serviços.").show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }
}
