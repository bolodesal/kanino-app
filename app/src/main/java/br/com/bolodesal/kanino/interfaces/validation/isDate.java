package br.com.bolodesal.kanino.interfaces.validation;

import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.text.SimpleDateFormat;
import java.util.Date;

import br.com.bolodesal.kanino.validation.DateRule;


/**
 * Created by Edgar Maia on 22/11/2015.
 */
@ValidateUsing(DateRule.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface isDate {
    int messageResId()   default -1;
    String message()     default "Data inválida. Verifique sua data de nascimento";
    int sequence()       default -1;

    String format()      default "dd/MM/yyyy";
    String minDate()     default "";
    String maxDate()     default "";
}
