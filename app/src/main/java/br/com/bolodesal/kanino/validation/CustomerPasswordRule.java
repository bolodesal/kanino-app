package br.com.bolodesal.kanino.validation;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.mobsandgeeks.saripaar.AnnotationRule;

import java.io.IOException;

import br.com.bolodesal.kanino.app.KaninoApplication;
import br.com.bolodesal.kanino.interfaces.validation.CheckPasswordCustomer;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.CustomerResponse;
import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;
import retrofit.Response;

/**
 * Created by Edgar Maia on 29/11/2015.
 */
public class CustomerPasswordRule extends AnnotationRule<CheckPasswordCustomer, String> {

    protected CustomerPasswordRule(CheckPasswordCustomer passwordCustomer) {
        super(passwordCustomer);
    }


    @Override
    public boolean isValid(String passwordCustomer) {
        boolean isValid = false;
        if (passwordCustomer.equals("")) { return true; }

        KaninoServices service = RestService.getInstance().getService();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(KaninoApplication.getAppContext());

        try {
            Response<CustomerResponse> r = service.getCustomer(prefs.getInt(SharedPreferencesSettings.KEY_PREFS_ID_USER, 0)).execute();

            if (r.body().getSenhaCliente().equals(passwordCustomer)) {
                isValid = true;
            }
        } catch (IOException e) {
            return false;
        }catch (Exception e){
            return false;
        }
        return isValid;
    }
}
