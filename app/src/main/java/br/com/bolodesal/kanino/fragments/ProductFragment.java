
package br.com.bolodesal.kanino.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import br.com.bolodesal.kanino.ProductDetailActivity;
import br.com.bolodesal.kanino.R;
import br.com.bolodesal.kanino.adapters.ProductAdapter;
import br.com.bolodesal.kanino.services.entity.ProductResponse;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.models.ProductsPaginated;
import br.com.bolodesal.kanino.util.UtilKanino;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ProductFragment extends Fragment implements RecyclerViewOnClickListenerHack {

    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private List<ProductResponse> mList;
    private ProductAdapter adapter;
    private float scale;
    private int pLeft, pRight, pTop, pBottom;
    private int controle = 1, aux = 0;
    private int page = 1, amount = 6;
    private int totalAmount;
    private View view;
    private KaninoServices service;
    private GridLayoutManager mgridLayoutManager;
    private UtilKanino utilKanino = new UtilKanino();
    FrameLayout fLWithoutConnection;
    FrameLayout fLProgress;
    private FrameLayout fLSlowConnection;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        scale = getResources().getDisplayMetrics().density;
        pLeft = (int) (4 + scale + 0.5f);
        pRight = (int) (4 + scale + 0.5f);

        view = inflater.inflate(R.layout.fragment_product, container, false);

        final Animation animScale = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_scale);

        fLSlowConnection = (FrameLayout) view.findViewById(R.id.fLSlowConnection);

        Button btnReloadS = (Button) view.findViewById(R.id.btnReloadS);
        btnReloadS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fLProgress.setVisibility(View.VISIBLE);
                fLSlowConnection.setVisibility(View.GONE);
                getProducts();
            }
        });

        fLWithoutConnection = (FrameLayout) view.findViewById(R.id.fLWithoutConnection);
        fLProgress = (FrameLayout) view.findViewById(R.id.fLProgress);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rvProductList);
        mRecyclerView.setHasFixedSize(true);

        mgridLayoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);

        mgridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mgridLayoutManager);

        service = RestService.getInstance().getService();

        if(utilKanino.hasConnected(getActivity())){
            getProducts();
        }
        else{
            loadViewError();
        }

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (mgridLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    pTop = (int) (4 + scale + 0.5f);
                    mRecyclerView.setPadding(pLeft, pTop, pRight, 0);
                } else {
                    pTop = 0;
                    mRecyclerView.setPadding(pLeft, pTop, pRight, 0);
                }

                if (mList.size() == mgridLayoutManager.findLastCompletelyVisibleItemPosition() + 1) {

                    pBottom = (int) (4 + scale + 0.5f);
                    mRecyclerView.setPadding(pLeft, pTop, pRight, pBottom);

                    if (controle != aux) {
                        aux = controle;



                        getMoreProducts();
                    }
                }
                else{
                    pBottom = 0;
                    mRecyclerView.setPadding(pLeft, pTop, pRight, pBottom);
                }
            }
        });

        return view;
    }

    private void loadViewError(){
        fLWithoutConnection.setVisibility(View.VISIBLE);
        fLProgress.setVisibility(View.GONE);

        Button btnReload = (Button) view.findViewById(R.id.btnReload);
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!utilKanino.hasConnected(getActivity())) {
                    loadViewError();
                } else {
                    fLWithoutConnection.setVisibility(View.GONE);
                    fLProgress.setVisibility(View.VISIBLE);
                    getProducts();
                }
            }
        });
    }

    public void getProducts(){
        service.getProducts(page, amount).enqueue(new Callback<ProductsPaginated>() {
            @Override
            public void onResponse(Response<ProductsPaginated> response, Retrofit retrofit) {

                if(response.code() == 200){
                    totalAmount = response.body().getTotalProdutos();
                    page++;

                    mList = response.body().getProdutos();
                    adapter = new ProductAdapter(getActivity(), mList);

                    mgridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            switch (adapter.getItemViewType(position)) {
                                case 0:
                                    return 1;
                                case 1:
                                    return 2; //number of columns of the grid
                                default:
                                    return -1;
                            }
                        }
                    });

                    adapter.setRecyclerViewOnClickListenerHack(ProductFragment.this);

                    mRecyclerView.setAdapter(adapter);
                }
                else{
                    fLSlowConnection.setVisibility(View.VISIBLE);
                }

                fLProgress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Throwable t) {
                fLProgress.setVisibility(View.GONE);
                fLSlowConnection.setVisibility(View.VISIBLE);
            }
        });
    }

    public void getMoreProducts(){

        adapter.addProgressBar();
        mRecyclerView.scrollToPosition(mList.size() - 1);

        service.getProducts(page, amount).enqueue(new Callback<ProductsPaginated>() {
            @Override
            public void onResponse(Response<ProductsPaginated> response, Retrofit retrofit) {

                if(response.code() == 200){
                    if (mList.size() <= totalAmount) {
                        List<ProductResponse> listAux = response.body().getProdutos();

                        adapter.removeProgressBar();

                        page++;

                        for (int i = 0; i < listAux.size(); i++) {
                            adapter.addListItem(listAux.get(i), mList.size());
                        }

                        controle++;
                    }
                }
                else{
                    pBottom = (int) (4 + scale + 0.5f);
                    mRecyclerView.setPadding(pLeft, pTop, pRight, pBottom);
                    adapter.removeProgressBar();
                    Toast.makeText(getActivity(), "Todos os produtos já foram carregados.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {

                adapter.removeProgressBar();

                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Falha ao buscar produtos!")
                        .setContentText("Aparentemente sua conexão com a internet está ruim. Deseja tentar novamente?")
                        .setCancelText("Não,cancelar!")
                        .setConfirmText("Sim, Tentar!")
                        .showCancelButton(true)
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog
                                        .setTitleText("Pronto!")
                                        .setContentText("Estamos tentando novamente!")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(null)
                                        .showCancelButton(false)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                                getMoreProducts();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void onClickListener(View view, int position) {

        int id = mList.get(position).getIdProduto();

        Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
        intent.putExtra("id", "k" + Integer.toString(id));
        startActivity(intent);
    }
}
