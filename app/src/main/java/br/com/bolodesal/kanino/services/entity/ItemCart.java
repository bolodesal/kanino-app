package br.com.bolodesal.kanino.services.entity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.sql.Blob;


/**
 * Created by Thiago Ferreira on 02/11/2015.
 */
public class ItemCart {

    private int id;
    private int amount;
    private double price;
    private byte[] image;
    private String name;

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public Bitmap getImage() {
        Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);

        return bmp;
    }

    public byte[] getStringImage(){
        return this.image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
