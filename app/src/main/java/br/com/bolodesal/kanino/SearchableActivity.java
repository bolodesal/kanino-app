package br.com.bolodesal.kanino;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.provider.SearchRecentSuggestions;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import br.com.bolodesal.kanino.adapters.ProductAdapter;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.provider.SearchableProvider;
import br.com.bolodesal.kanino.services.Cart;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.ProductResponse;
import br.com.bolodesal.kanino.services.models.ProductsPaginated;
import br.com.bolodesal.kanino.util.UtilKanino;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class SearchableActivity extends AppCompatActivity implements RecyclerViewOnClickListenerHack {

    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private List<ProductResponse> mList;
    private ProductAdapter adapter;
    private float scale;
    private int pLeft, pRight, pTop, pBottom;
    private int controle = 1, aux = 0;
    private int amount = 10;
    private int totalAmount;
    private KaninoServices service;
    private GridLayoutManager mgridLayoutManager;
    private UtilKanino utilKanino = new UtilKanino();
    FrameLayout fLWithoutConnection;
    FrameLayout fLProgress;
    int pageNumber = 1;
    String query;
    private Toolbar mToolbar;
    private FrameLayout fLSlowConnection;
    private FrameLayout fLNotFound;
    private int hot_number = 0;
    private TextView ui_hot = null;
    private Cart cart;
    private Animation animScale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        animScale = AnimationUtils.loadAnimation(SearchableActivity.this, R.anim.anim_scale);

        cart = new Cart(getBaseContext());

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle("Busca");
        mToolbar.setTitleTextColor(getResources().getColor(R.color.textWhite));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        if( Intent.ACTION_SEARCH.equalsIgnoreCase( intent.getAction() ) ){
            query = intent.getStringExtra(SearchManager.QUERY);

            String palavra = "Busca - " + query.substring(0,1).toUpperCase().concat(query.substring(1));

            mToolbar.setTitle(palavra);

            //hendleSearch( getIntent() );
        }

        scale = getResources().getDisplayMetrics().density;
        pLeft = (int) (4 + scale + 0.5f);
        pRight = (int) (4 + scale + 0.5f);

        fLSlowConnection = (FrameLayout) findViewById(R.id.fLSlowConnection);
        fLNotFound = (FrameLayout) findViewById(R.id.fLNotFound);

        Button btnReloadS = (Button) findViewById(R.id.btnReloadS);
        btnReloadS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                fLProgress.setVisibility(View.VISIBLE);
                fLSlowConnection.setVisibility(View.GONE);
                getProducts();
            }
        });

        fLWithoutConnection = (FrameLayout) findViewById(R.id.fLWithoutConnection);
        fLProgress = (FrameLayout) findViewById(R.id.fLProgress);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);

        mRecyclerView = (RecyclerView) findViewById(R.id.rvProductList);
        mRecyclerView.setHasFixedSize(true);

        mgridLayoutManager = new GridLayoutManager(SearchableActivity.this, 2, GridLayoutManager.VERTICAL, false);

        mgridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mgridLayoutManager);

        service = RestService.getInstance().getService();

        if(utilKanino.hasConnected(SearchableActivity.this)){
            //getProducts();
            mRecyclerView.setVisibility(View.GONE);
            fLProgress.setVisibility(View.VISIBLE);
            hendleSearch( getIntent() );
        }
        else{
            loadViewError();
        }

        mRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (mgridLayoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    pTop = (int) (4 + scale + 0.5f);
                    mRecyclerView.setPadding(pLeft, pTop, pRight, 0);
                } else {
                    pTop = 0;
                    mRecyclerView.setPadding(pLeft, pTop, pRight, 0);
                }

                if (mList.size() == mgridLayoutManager.findLastCompletelyVisibleItemPosition() + 1) {

                    pBottom = (int) (4 + scale + 0.5f);
                    mRecyclerView.setPadding(pLeft, pTop, pRight, pBottom);

                    if (controle != aux) {
                        aux = controle;

                        getMoreProducts();
                    }
                }
                else{
                    pBottom = 0;
                    mRecyclerView.setPadding(pLeft, pTop, pRight, pBottom);
                }
            }
        });
    }

    private void loadViewError(){
        fLWithoutConnection.setVisibility(View.VISIBLE);
        fLProgress.setVisibility(View.GONE);

        Button btnReload = (Button) findViewById(R.id.btnReload);
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                if (!utilKanino.hasConnected(SearchableActivity.this)) {
                    loadViewError();
                } else {
                    fLWithoutConnection.setVisibility(View.GONE);
                    fLProgress.setVisibility(View.VISIBLE);
                    getProducts();
                }
            }
        });
    }

    public void getProducts(){
        service.getProductsBySearch(pageNumber, amount, query).enqueue(new Callback<ProductsPaginated>() {
            @Override
            public void onResponse(Response<ProductsPaginated> response, Retrofit retrofit) {

                ProductsPaginated productsPaginated = response.body();

                if(productsPaginated != null){

                    mList = response.body().getProdutos();

                    totalAmount = response.body().getTotalProdutos();
                    pageNumber++;

                    adapter = new ProductAdapter(SearchableActivity.this, mList);

                    mgridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            switch (adapter.getItemViewType(position)) {
                                case 0:
                                    return 1;
                                case 1:
                                    return 2; //number of columns of the grid
                                default:
                                    return -1;
                            }
                        }
                    });

                    adapter.setRecyclerViewOnClickListenerHack(SearchableActivity.this);

                    FrameLayout fLProgress = (FrameLayout) findViewById(R.id.fLProgress);

                    mRecyclerView.setAdapter(adapter);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    fLProgress.setVisibility(View.GONE);
                }
                else{
                    //Toast.makeText(SearchableActivity.this, "Não foram encontrados produtos compativeis com a busca =(", Toast.LENGTH_SHORT ).show();

                    fLProgress.setVisibility(View.GONE);
                    fLNotFound.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(Throwable t) {

                fLProgress.setVisibility(View.GONE);
                fLSlowConnection.setVisibility(View.VISIBLE);

                /*AlertDialog.Builder informa = new AlertDialog.Builder(ProductsByCategoryActivity.this);
                informa.setTitle("Aviso").setMessage("Falha na conexão com o servidor ao tentar buscar os produtos. Deseja tentar novemente?");
                informa.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                informa.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getProducts();
                    }
                }).show();*/
            }
        });
    }

    public void getMoreProducts(){

        adapter.addProgressBar();
        mRecyclerView.scrollToPosition(mList.size() - 1);

        service.getProductsBySearch(amount, pageNumber, query).enqueue(new Callback<ProductsPaginated>() {
            @Override
            public void onResponse(Response<ProductsPaginated> response, Retrofit retrofit) {

                if (mList.size() < totalAmount) {
                    List<ProductResponse> listAux = response.body().getProdutos();

                    adapter.removeProgressBar();

                    pageNumber++;

                    for (int i = 0; i < listAux.size(); i++) {
                        adapter.addListItem(listAux.get(i), mList.size());
                    }

                    controle++;
                } else {
                    pBottom = (int) (4 + scale + 0.5f);
                    mRecyclerView.setPadding(pLeft, pTop, pRight, pBottom);
                    adapter.removeProgressBar();
                    Toast.makeText(SearchableActivity.this, "Todos os produtos já foram carregados.", Toast.LENGTH_SHORT).show();
                    /*Snackbar.make(view , "Todos os produtos já foram carregados.", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Ok", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();*/
                }
            }

            @Override
            public void onFailure(Throwable t) {

                adapter.removeProgressBar();

                AlertDialog.Builder informa = new AlertDialog.Builder(SearchableActivity.this);
                informa.setTitle("Aviso").setMessage("Falha na conexão com o servidor ao tentar buscar os produtos. Deseja tentar novemente?");
                informa.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                informa.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getMoreProducts();
                    }
                }).show();
            }
        });
    }

    @Override
    public void onClickListener(View view, int position) {

        int id = mList.get(position).getIdProduto();

        Intent intent = new Intent(SearchableActivity.this, ProductDetailActivity.class);
        intent.putExtra("id", "k" + Integer.toString(id));
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        pageNumber = 1;

        mRecyclerView.setVisibility(View.GONE);
        fLNotFound.setVisibility(View.GONE);
        fLProgress.setVisibility(View.VISIBLE);

        hendleSearch(intent);
    }

    public void hendleSearch( Intent intent ){
        if( Intent.ACTION_SEARCH.equalsIgnoreCase( intent.getAction() ) ){
            query = intent.getStringExtra( SearchManager.QUERY );

            String palavra = "Busca - " + query.substring(0,1).toUpperCase().concat(query.substring(1));

            mToolbar.setTitle(palavra);

            getProducts();

            SearchRecentSuggestions searchRecentSuggestions = new SearchRecentSuggestions(this,
                    SearchableProvider.AUTHORITY,
                    SearchableProvider.MODE);
            searchRecentSuggestions.saveRecentQuery( query, null );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView;
        MenuItem item = menu.findItem(R.id.action_searchable_activity);

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ){
            searchView = (SearchView) item.getActionView();
        }
        else{
            searchView = (SearchView) MenuItemCompat.getActionView(item);
        }

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint(getResources().getString(R.string.buscar_produtos));

        final View menu_hotlist = menu.findItem(R.id.action_cart).getActionView();
        ui_hot = (TextView) menu_hotlist.findViewById(R.id.hotlist_hot);
        hot_number = cart.getCount();
        updateHotCount(hot_number);
        new MyMenuItemStuffListener(menu_hotlist, "Carrinho") {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchableActivity.this, CartActivity.class);
                startActivity(intent);
            }
        };
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hot_number = cart.getCount();
        updateHotCount(hot_number);
    }

    public void updateHotCount(final int new_hot_number) {
        hot_number = new_hot_number;

        if (ui_hot == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (new_hot_number == 0)
                    ui_hot.setVisibility(View.INVISIBLE);
                else {
                    ui_hot.setVisibility(View.VISIBLE);
                    ui_hot.setText(Integer.toString(new_hot_number));
                }
            }
        });
    }

    abstract class MyMenuItemStuffListener implements View.OnClickListener, View.OnLongClickListener {
        private String hint;
        private View view;

        MyMenuItemStuffListener(View view, String hint) {
            this.view = view;
            this.hint = hint;
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override abstract public void onClick(View v);

        @Override public boolean onLongClick(View v) {
            final int[] screenPos = new int[2];
            final Rect displayFrame = new Rect();
            view.getLocationOnScreen(screenPos);
            view.getWindowVisibleDisplayFrame(displayFrame);
            final Context context = view.getContext();
            final int width = view.getWidth();
            final int height = view.getHeight();
            final int midy = screenPos[1] + height / 2;
            final int screenWidth = getResources().getDisplayMetrics().widthPixels;
            Toast cheatSheet = Toast.makeText(context, hint, Toast.LENGTH_SHORT);
            if (midy < displayFrame.height()) {
                cheatSheet.setGravity(Gravity.TOP | Gravity.RIGHT,
                        screenWidth - screenPos[0] - width / 2, height);
            } else {
                cheatSheet.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, height);
            }
            cheatSheet.show();
            return true;
        }
    }
}
