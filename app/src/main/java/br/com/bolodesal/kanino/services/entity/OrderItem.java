package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Thiago Ferreira on 22/11/2015.
 */
public class OrderItem {
    @SerializedName("idProduto")
    @Expose
    private int idProduto;

    @SerializedName("precoVendaItem")
    @Expose
    private Double precoVendaItem;

    @SerializedName("qtdProduto")
    @Expose
    private int qtdProduto;

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public void setPrecoVendaItem(Double precoVendaItem) {
        this.precoVendaItem = precoVendaItem;
    }

    public void setQtdProduto(int qtdProduto) {
        this.qtdProduto = qtdProduto;
    }

    public int getIdProduto() {
        return idProduto;
    }

    public Double getPrecoVendaItem() {
        return precoVendaItem;
    }

    public int getQtdProduto() {
        return qtdProduto;
    }
}
