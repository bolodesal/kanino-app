package br.com.bolodesal.kanino;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import br.com.bolodesal.kanino.adapters.AddressAdapter;
import br.com.bolodesal.kanino.adapters.AddressDeliveryAdapter;
import br.com.bolodesal.kanino.interfaces.RecyclerViewOnClickListenerHack;
import br.com.bolodesal.kanino.services.KaninoServices;
import br.com.bolodesal.kanino.services.RestService;
import br.com.bolodesal.kanino.services.entity.AddressResponse;
import br.com.bolodesal.kanino.services.models.*;
import br.com.bolodesal.kanino.services.models.Error;
import br.com.bolodesal.kanino.settings.SharedPreferencesSettings;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.Converter;
import retrofit.Response;
import retrofit.Retrofit;

public class AddressDeliveryActivity extends AppCompatActivity implements RecyclerViewOnClickListenerHack {

    private Toolbar mToolbar;
    private FrameLayout fLEmptyOrders;
    private FrameLayout fLProgress;
    private LinearLayout container;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager Llm;
    private AddressDeliveryAdapter adapter;
    private SweetAlertDialog sweetDialog;
    private int idUser;
    private Button btnAddAddress;
    private List<AddressResponse> mList;
    private List<AddressResponse> items;
    private KaninoServices service;
    private SharedPreferences prefs;
    private int addressId = 1;
    private FrameLayout fLSlowConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_delivery);

        fLSlowConnection = (FrameLayout) findViewById(R.id.fLSlowConnection);

        final Animation animScale = AnimationUtils.loadAnimation(AddressDeliveryActivity.this, R.anim.anim_scale);

        Button btnReloadS = (Button) findViewById(R.id.btnReloadS);
        btnReloadS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                fLProgress.setVisibility(View.VISIBLE);
                fLSlowConnection.setVisibility(View.GONE);
                getAddress();
            }
        });

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fLProgress = (FrameLayout) findViewById(R.id.fLProgress);
        mRecyclerView = (RecyclerView) findViewById(R.id.rvAddressList);
        container = (LinearLayout) findViewById(R.id.container);

        mRecyclerView.setHasFixedSize(true);

        Llm = new LinearLayoutManager(this);
        Llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(Llm);

        service = RestService.getInstance().getService();
        prefs = PreferenceManager.getDefaultSharedPreferences(AddressDeliveryActivity.this);
        idUser = prefs.getInt(SharedPreferencesSettings.KEY_PREFS_ID_USER, 0);

        getAddress();

        btnAddAddress = (Button) findViewById(R.id.btnAddAddress);
        btnAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animScale);
                animScale.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        boolean checked = false;

                        for(int i = 0; i < mList.size(); i++){
                            RadioButton rb = (RadioButton) mRecyclerView.findViewHolderForLayoutPosition(i).itemView.findViewById(R.id.rbAddress);
                            if(rb.isChecked()){
                                checked = true;
                            }
                        }

                        if(checked){
                            Intent intent = new Intent(AddressDeliveryActivity.this, TypePaymentActivity.class);
                            intent.putExtra("idAddress", addressId);
                            startActivity(intent);
                        }
                        else{
                            new SweetAlertDialog(AddressDeliveryActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops...")
                                    .setContentText("Escolha um endereço.")
                                    .show();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                sweetDialog.dismiss();
                getAddress();
            }
        }
    }

    public void resetActitivyIsEmpytAddress()
    {
        btnAddAddress.setVisibility(View.GONE);
    }

    private void getAddress()
    {
        service.getAddresses(idUser).enqueue(new Callback<List<AddressResponse>>() {
            @Override
            public void onResponse(Response<List<AddressResponse>> response, Retrofit retrofit) {

                if(response.code() == 200)
                {
                    mList = response.body();

                    adapter = new AddressDeliveryAdapter(AddressDeliveryActivity.this, mList);
                    adapter.setRecyclerViewOnClickListenerHack(AddressDeliveryActivity.this);
                    mRecyclerView.setAdapter(adapter);
                }
                else
                {
                    if (response != null && !response.isSuccess() && response.errorBody() != null) {
                        Converter<ResponseBody, GlobalResponse<Error<ArrayList<String>>>> errorConverter = retrofit.responseConverter(new TypeToken<GlobalResponse<Error<ArrayList<String>>>>() {
                        }.getType(), new Annotation[0]);
                        try {
                            GlobalResponse<Error<ArrayList<String>>> r = errorConverter.convert(response.errorBody());

                            String messages = "";

                            for (int i = 0; i < r.getResponse().getErrors().size(); i++) {
                                messages += String.format("%s\n", r.getResponse().getErrors().get(i));
                            }

                            new SweetAlertDialog(AddressDeliveryActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText(messages)
                                    .setConfirmText("Adicionar endereço")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetDialog = sweetAlertDialog;
                                            Intent i = new Intent(AddressDeliveryActivity.this, RegisterAddressActivity.class);
                                            startActivityForResult(i, 1);
                                        }
                                    }).show();

                            resetActitivyIsEmpytAddress();

                        } catch (IOException e) {

                            new SweetAlertDialog(AddressDeliveryActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Oops!")
                                    .setContentText("Ops! Houve um problema, estamos trabalhando na solução.").show();
                        }
                    }
                }

                container.setVisibility(View.VISIBLE);
                fLProgress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Throwable t) {
                fLSlowConnection.setVisibility(View.VISIBLE);
            }
        });
    }

    public void changeCheckedItem(int position){
        for(int i = 0; i < mList.size(); i++){
            RadioButton rb = (RadioButton) mRecyclerView.findViewHolderForLayoutPosition(i).itemView.findViewById(R.id.rbAddress);
            rb.setChecked(false);
        }

        RadioButton rb = (RadioButton) mRecyclerView.findViewHolderForLayoutPosition(position).itemView.findViewById(R.id.rbAddress);
        rb.setChecked(true);
        addressId = mList.get(position).getIdEndereco();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickListener(View view, int position) {

    }
}
