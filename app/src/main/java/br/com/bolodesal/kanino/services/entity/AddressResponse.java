package br.com.bolodesal.kanino.services.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by bruno.pregis on 26/11/2015.
 */
public class AddressResponse {

    @SerializedName("cepEndereco")
    @Expose
    private String cepEndereco;
    @SerializedName("cidadeEndereco")
    @Expose
    private String cidadeEndereco;
    @SerializedName("complementoEndereco")
    @Expose
    private String complementoEndereco;
    @SerializedName("idCliente")
    @Expose
    private Integer idCliente;
    @SerializedName("idEndereco")
    @Expose
    private Integer idEndereco;
    @SerializedName("logradouroEndereco")
    @Expose
    private String logradouroEndereco;
    @SerializedName("nomeEndereco")
    @Expose
    private String nomeEndereco;
    @SerializedName("numeroEndereco")
    @Expose
    private String numeroEndereco;
    @SerializedName("paisEndereco")
    @Expose
    private String paisEndereco;
    @SerializedName("ufEndereco")
    @Expose
    private String ufEndereco;

    public String getCepEndereco() {
        return cepEndereco;
    }

    public String getCidadeEndereco() {
        return cidadeEndereco;
    }

    public String getComplementoEndereco() {
        return complementoEndereco;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public Integer getIdEndereco() {
        return idEndereco;
    }

    public String getLogradouroEndereco() {
        return logradouroEndereco;
    }

    public String getNomeEndereco() {
        return nomeEndereco;
    }

    public String getNumeroEndereco() {
        return numeroEndereco;
    }

    public String getPaisEndereco() {
        return paisEndereco;
    }

    public String getUfEndereco() {
        return ufEndereco;
    }

}
